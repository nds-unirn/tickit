package br.edu.unirn.tickit.acesso

class AutenticacaoController {

    def login() { 

    }

    def autenticar(){
    	if(!params?.login || !params?.senha){
    		flash.message = "Para se autenticar no sistema é necessário informar o Login e a Senha."
    		render(view:"login",model:[tipoMensagem:"error"])
    		return
    	}
    	Usuario usuario = Usuario.findByLoginAndSenhaAndAtivo(params?.login,params?.senha?.encodeAsSHA256(),true)
    	if(usuario){
    		session.setAttribute("usuario", usuario)
            redirect(controller:"dashboard")
            return
    	}else{
            flash.message = "O login e senha informados não são válidos."
            render(view:"login",model:[tipoMensagem:"error"])
            return   
        }
    }

    def logout(){
        session.invalidate();
        redirect(controller:"publico")
    }
}
