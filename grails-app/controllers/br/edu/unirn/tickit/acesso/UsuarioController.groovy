package br.edu.unirn.tickit.acesso

import org.codehaus.groovy.grails.commons.GrailsClassUtils
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UsuarioController {

    def usuarioService

    def index(Integer max) {
        
        int offset = params.offset!=null?Integer.parseInt(params.offset):0
        Map variaveis = new HashMap()
        String query = "from Usuario u where u.ativo = true "
        def usuarios
        def totalUsuarios

        variaveis.put("max", 10);
        variaveis.put("offset", offset)
        usuarios = Usuario.executeQuery(query+" order by u.nome asc ", variaveis)

        totalUsuarios = Usuario.executeQuery("select count(u)  "+query,variaveis)

        render view:"index", model:[usuarioInstanceCount: totalUsuarios, usuarioInstanceList:usuarios, params:params,tipoMensagem:params?.tipoMensagem, textoMensagem:params?.textoMensagem ]
        return
    }

    def create(){
        [perfis: Perfil.findAllWhere(ativo:true)]
    }

    @Transactional
    def save(){
        Usuario usuarioInstance = new Usuario();
        usuarioInstance.nome = params?.nome
        usuarioInstance.email = params?.email
        usuarioInstance.login = params?.login
        usuarioInstance.senha = params?.senha
        usuarioInstance.dateCreated = new Date()
        usuarioInstance.usuarioCadastrante = session?.usuario

        //VALIDAÇÃO PARA LOGIN REPETIDO
        if(params.login){

            def usuarios = Usuario.executeQuery("select count(u) from Usuario u where u.ativo = true and u.login = :login", [login:params.login])

            if(usuarios[0] > 0){
                flash.message = "Login já está cadastrado no sistema."
                render(view:"create",model:[usuarioInstance:usuarioInstance,perfis:Perfil.findAllWhere(ativo:true),tipoMensagem:"erro"])
                return
            }
        }

        //VALIDAÇÃO PARA E-MAIL REPETIDO
        if(params.email){

            def usuarios = Usuario.executeQuery("select count(u) from Usuario u where u.ativo = true and u.email = :email", [email:params.email])

            if(usuarios[0] > 0){
                flash.message = "E-mail já está cadastrado no sistema."
                render(view:"create",model:[usuarioInstance:usuarioInstance,perfis:Perfil.findAllWhere(ativo:true),tipoMensagem:"error"])
                return
            }
        }

        if(!params.perfil){
            //TO-DO: internacionalizar
            flash.message = "Por favor selecione um perfil para o usuário"
            render(view:"create",model:[usuarioInstance:usuarioInstance,perfis:Perfil.findAllWhere(ativo:true),tipoMensagem:"error"])
            return;
        }

        if(usuarioInstance?.validate()){


            if(usuarioInstance?.save(flush:true)){

                //adicionando perfil ao usuário cadastrado
                UsuarioPerfil usuarioPerfilInstance = new UsuarioPerfil(usuario:usuarioInstance,perfil:Perfil.get(params?.long('perfil')),usuarioCadastrante:session?.usuario)
                usuarioPerfilInstance?.save flush:true
                   
                flash.message = message(code: 'default.created.message', args: [message(code: 'usuario.label', default: 'Usuário'), usuarioInstance], encodeAs: "None")
                redirect(action:"index",params:[tipoMensagem:"success", textoMensagem:flash.message])
                return
               
            }else{
                //TO-DO: Internacionalizar com codígo de mensagem
                flash.message ="O sistema comportou-se de forma inesperada e não foi possível salvar o usuário. Por favor tente novamente."
                render(view:"create",model:[usuarioInstance:usuarioInstance,tipoMensagem:"error"])
                return
            }
        }else{

            render(view:"create",model:[usuarioInstance:usuarioInstance, tipoMensagem:"error", perfis:Perfil.findAllWhere(ativo:true)])       
            return
        }

    }


    def show(Long id){
        [usuarioInstance: Usuario.get(id),]
    }

    def edit(Long id){
        [usuarioInstance: Usuario.get(id), perfis:Perfil.findAllWhere(ativo:true)]
    }

    @Transactional
    def update(){
        Usuario usuarioInstance = Usuario.get(params?.long('id'));
        usuarioInstance.nome = params?.nome;
        usuarioInstance.email = params?.email;
        usuarioInstance.login = params?.login;

        if(!params.perfil){
            //TO-DO: internacionalizar
            flash.message = "Por favor selecione um perfil para o usuário"
            render(view:"edit",model:[usuarioInstance:usuarioInstance,perfis:Perfil.findAllWhere(ativo:true),tipoMensagem:"error"])
            return;
        }

        if(usuarioInstance?.validate()){
            if(usuarioInstance?.save(flush:true)){
                //atualizado/adicionando perfil ao usuário cadastrado
                if(usuarioInstance?.hasPerfil(Perfil.get(params.long('perfil')))){
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'usuario.label', default: 'Usuário'), usuarioInstance], encodeAs: "None")
                    redirect(action:"index",params:[tipoMensagem:"success"])
                    return
                }else{
                    if(!usuarioInstance?.desativarPerfis()){
                        flash.message = "O sistema comportou-se de forma inesperada e por isso não foi possível alterar o perfil do usuário. Por favor tente novamente"
                        redirect(action:"index",params:[tipoMensagem:"error"])                    
                        return
                    }
                    UsuarioPerfil usuarioPerfilInstance = new UsuarioPerfil(usuarioInstance,Perfil.get(params?.long('perfil')),session?.usuario)
                    if(usuarioPerfilInstance?.save(flush:true)){
                        flash.message = message(code: 'default.updated.message', args: [message(code: 'usuario.label', default: 'Usuário'), usuarioInstance], encodeAs: "None")
                        redirect(action:"index",params:[tipoMensagem:"success"])
                        return
                    }else{
                        flash.message = "O sistema comportou-se de forma inesperada e por isso não foi possível alterar o perfil do usuário. Por favor tente novamente"
                        redirect(action:"index",params:[tipoMensagem:"error"])                    
                        return
                    }    
                }                

            }else{
                //TO-DO: Internacionalizar com codígo de mensagem
                flash.message ="O sistema comportou-se de forma inesperada e não foi possível salvar o usuário. Por favor tente novamente."
                render(view:"edit",model:[usuarioInstance:usuarioInstance,tipoMensagem:"error"])
                return
            }
        }else{
            render(view:"edit",model:[usuarioInstance:usuarioInstance])       
            return
        }
    }
    
    @Transactional
    def delete(Long id){
        def usuarioInstance = Usuario.get(id)
        if(usuarioInstance?.desativar()){
            if(usuarioInstance?.save(flush:true)){
                usuarioService.recriarUsuarioAdminBootstrap()

                flash.message = message(code: 'default.deleted.message', args: [message(code: 'usuario.label', default: 'Usuário'), usuarioInstance], encodeAs: "None")
                redirect(action:"index",params:[tipoMensagem:"success"])
            }else{
                flash.message = "O sistema comportou-se de forma inesperada e não foi possível apagar o usuário. Por favor tente novamente."
                redirect(action:"index",params:[tipoMensagem:"error"])
            }
        }else{
            flash.message = "O sistema comportou-se de forma inesperada e não foi possível apagar os perfis do usuário. Por favor tente novamente."
            redirect(action:"index",params:[tipoMensagem:"error"])
        }
    }

}
