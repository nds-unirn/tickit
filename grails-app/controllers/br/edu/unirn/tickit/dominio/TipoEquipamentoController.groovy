package br.edu.unirn.tickit.dominio


import org.codehaus.groovy.grails.commons.GrailsClassUtils
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TipoEquipamentoController {

    static scaffold = true

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def tipoEquipamentoInstance = new TipoEquipamento()
        def criteriaResult = TipoEquipamento.createCriteria().list(params) {
            if (tipoEquipamentoInstance.properties.containsKey('ativo'))
                eq("ativo", Boolean.TRUE)

            if (params.q) {
                or {
                    GrailsClassUtils?.getStaticPropertyValue(TipoEquipamento, "searchFields")?.each {
                        def property = GrailsClassUtils?.getPropertyType(TipoEquipamento, it)
                        if (property && Number.isAssignableFrom(property) || (property?.isPrimitive() && property != boolean)) {
                            if (property == Integer)
                                eq(it, params.int("q"))
                        } else if (property == String) {
                            ilike(it, params.q + "%")
                        }
                    }
                }
            }
        }
        respond criteriaResult, model: [tipoEquipamentoInstanceCount: criteriaResult.totalCount, params: params]
    }

    @Transactional
    def save(TipoEquipamento tipoEquipamentoInstance) {
        withForm {
            if (tipoEquipamentoInstance == null) {
                notFound()
                return
            }

            tipoEquipamentoInstance.usuarioCadastrante = session?.usuario

            tipoEquipamentoInstance.clearErrors()
            tipoEquipamentoInstance.save(flush: true)

            if (tipoEquipamentoInstance.hasErrors()) {
                respond tipoEquipamentoInstance.errors, view: 'create'
                return
            }

            tipoEquipamentoInstance.save flush: true

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'tipoEquipamento.label', default: 'Tipo de Equipamento'), tipoEquipamentoInstance], encodeAs: "None")
                    redirect action: "index", method: "GET"
                }
                '*' { respond tipoEquipamentoInstance, [status: CREATED] }
            }
        }.invalidToken {
            flash.message = "Requisição inválida. Por favor, tente reiniciar a operação"
            redirect action: 'index'
        }
    }

    def edit(TipoEquipamento tipoEquipamentoInstance) {
        respond tipoEquipamentoInstance
    }

    @Transactional
    def update(TipoEquipamento tipoEquipamentoInstance) {
        withForm {
            if (tipoEquipamentoInstance == null) {
                notFound()
                return
            }

            if (tipoEquipamentoInstance.hasErrors()) {
                respond tipoEquipamentoInstance.errors, view: 'edit'
                return
            }

            tipoEquipamentoInstance.save flush: true

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'TipoEquipamento.label', default: 'Tipo de Equipamento'), tipoEquipamentoInstance], encodeAs: "None")
                    redirect action: "index", method: "GET"
                }
                '*' { respond tipoEquipamentoInstance, [status: OK] }
            }
        }.invalidToken {
            flash.message = "Requisição inválida. Por favor, tente reiniciar a operação"
            redirect action: 'index'
        }
    }

    @Transactional
    def delete(TipoEquipamento tipoEquipamentoInstance) {
        withForm {
            if (tipoEquipamentoInstance == null) {
                notFound()
                return
            }

            if (tipoEquipamentoInstance.properties.containsKey("ativo")) {
                tipoEquipamentoInstance.ativo = false
                tipoEquipamentoInstance.save flush: true
            } else {
                tipoEquipamentoInstance.delete flush: true
            }

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'TipoEquipamento.label', default: 'Tipo de Equipamento'), tipoEquipamentoInstance], encodeAs: "None")
                    redirect action: "index", method: "GET"
                }
                '*' { render status: NO_CONTENT }
            }
        }.invalidToken {
            flash.message = "Requisição inválida. Por favor, tente reiniciar a operação"
            redirect action: 'index'
        }
    }
}
