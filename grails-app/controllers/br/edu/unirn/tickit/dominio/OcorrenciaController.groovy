package br.edu.unirn.tickit.dominio



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import java.text.SimpleDateFormat
import java.text.DateFormat
import java.util.*
import java.sql.Timestamp


@Transactional(readOnly = true)
class OcorrenciaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {

        flash.message = null
        int offset = params.offset!=null?Integer.parseInt(params.offset):0
        Map variaveis = new HashMap()
        String query = "from Ocorrencia o where o.ativo = true "
        def ocorrencias
        def totalOcorrencias
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyy HH:mm")


        if(params?.tipoOcorrencia != null && !params?.tipoOcorrencia.trim().equals("")){

            query += " and o.tipoOcorrencia = :tipoOcorrencia "
            def tipoOcorrencia = TipoOcorrencia.get(params.long('tipoOcorrencia'))

            variaveis.put("tipoOcorrencia", tipoOcorrencia)

        }

        //Se a Data incial estiver preenchida, entra aqui
        if(params?.dataInicioIntervalo != null && !params?.dataInicioIntervalo.trim().equals("")){

            //Se as duas datas estiverem preenchidas e o dateTime inicial for menor ou igual ao dateTime final, entra aqui

            if(params?.dataFinalIntervalo != null && !params?.dataFinalIntervalo.trim().equals("") && sdf.parse(params?.dataInicioIntervalo) <= sdf.parse(params?.dataFinalIntervalo)){

               query += " and o.dataOcorrencia between :dataInicioIntervalo and :dataFinalIntervalo "
               variaveis.put("dataInicioIntervalo", sdf.parse(params?.dataInicioIntervalo))
               variaveis.put("dataFinalIntervalo", sdf.parse(params?.dataFinalIntervalo))
            }
            else {


                //Se apenas a data inicial estiver preenchida, entra aqui
                flash.message = "Os dois campos de data precisam ser preenchidos e a data inicial deve ser menor que a data final."
                variaveis.put("max", 10);
                variaveis.put("offset", offset)
                ocorrencias = Ocorrencia.executeQuery(query+" order by o.dataOcorrencia desc ", variaveis)

                totalOcorrencias = Ocorrencia.executeQuery("select count(o)  "+query,variaveis)
                [ocorrencias:ocorrencias, ocorrenciaInstanceCount: totalOcorrencias[0], params:params]
                

            }

            //Se apenas a data final estiver preenchida, entra aqui

        }else if(params?.dataFinalIntervalo != null && !params?.dataFinalIntervalo.trim().equals("")){

                flash.message = "Os dois campos de data precisam ser preenchidos."
                variaveis.put("max", 10);
                variaveis.put("offset", offset)
                ocorrencias = Ocorrencia.executeQuery(query+" order by o.dataOcorrencia desc ", variaveis)

                totalOcorrencias = Ocorrencia.executeQuery("select count(o)  "+query,variaveis)
                [ocorrencias:ocorrencias, ocorrenciaInstanceCount: totalOcorrencias[0], params:params]

        }



        variaveis.put("max", 10);
        variaveis.put("offset", offset)
        ocorrencias = Ocorrencia.executeQuery(query+" order by o.dataOcorrencia desc ", variaveis)

        totalOcorrencias = Ocorrencia.executeQuery("select count(o)  "+query,variaveis)

        if(params?.imprimir != null){

            render view:"imprimir", model:[ocorrencias:ocorrencias, ocorrenciaInstanceCount: totalOcorrencias[0], params:params, contador:1, contadorPagina:1]
            return
        }

        /*
        params.max = Math.min(max ?: 10, 100)
        params.sort = "dataOcorrencia"
        params.order = "desc"*/
        [ocorrencias:ocorrencias, ocorrenciaInstanceCount: totalOcorrencias[0], params:params]
    }

    def show(Ocorrencia ocorrenciaInstance) {
        respond ocorrenciaInstance
    }

    def create() {
        respond new Ocorrencia(params)
    }

    @Transactional
    def save(Ocorrencia ocorrenciaInstance) {
        if (ocorrenciaInstance == null) {
            notFound()
            return
        }

       DateFormat sdf = new SimpleDateFormat("dd/MM/yyy HH:mm")

       ocorrenciaInstance.dataOcorrencia = sdf.parse(params.dataOcorrencia)

        //Verifica se a data e a hora escolhidas é maior que a data e hora atuais.
         if(ocorrenciaInstance.dataOcorrencia > new Date()){
            respond ocorrenciaInstance.errors, view:'create'
            return
        }
        
        ocorrenciaInstance.usuarioCadastrante = session?.usuario

        ocorrenciaInstance.clearErrors()
        ocorrenciaInstance.save(flush: true)

        if (ocorrenciaInstance.hasErrors()) {
            respond ocorrenciaInstance.errors, view:'create'
            return
        }

        
        ocorrenciaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'ocorrencia.label', default: 'Ocorrencia'), ocorrenciaInstance.id])
                redirect ocorrenciaInstance
            }
            '*' { respond ocorrenciaInstance, [status: CREATED] }
        }
    }

    def edit(Ocorrencia ocorrenciaInstance) {

        //Converte Timestamp em Date
        Date date = new Date(ocorrenciaInstance.dataOcorrencia.getTime());

        //Para converter a data que vem do banco neste formato abaixo
        DateFormat dateFormat  = new SimpleDateFormat("dd/MM/yyyy HH:mm")
 

      [ocorrenciaInstance:ocorrenciaInstance, dataOcorrencia: dateFormat.format(ocorrenciaInstance.dataOcorrencia)]
    }

    @Transactional
    def update(Ocorrencia ocorrenciaInstance) {
         if (ocorrenciaInstance == null) {
            notFound()
            return
        }

       DateFormat sdf = new SimpleDateFormat("dd/MM/yyy HH:mm")


       //Verifica se a data e a hora escolhidas é maior que a data e hora atuais.
        if(sdf.parse(params.dataOcorrencia) > new Date()){
            respond ocorrenciaInstance, view:'edit'
            return
        }
        
        ocorrenciaInstance.dataOcorrencia = sdf.parse(params.dataOcorrencia)

        ocorrenciaInstance.usuarioCadastrante = session?.usuario

        ocorrenciaInstance.clearErrors()
        ocorrenciaInstance.save(flush: true)

        if (ocorrenciaInstance.hasErrors()) {
            respond ocorrenciaInstance.errors, view:'edit'
            return
        }

        
        ocorrenciaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Ocorrencia.label', default: 'Ocorrencia'), ocorrenciaInstance.id])
                redirect ocorrenciaInstance
            }
            '*'{ respond ocorrenciaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Ocorrencia ocorrenciaInstance) {

        if (ocorrenciaInstance == null) {
            notFound()
            return
        }

        ocorrenciaInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Ocorrencia.label', default: 'Ocorrencia'), ocorrenciaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'ocorrencia.label', default: 'Ocorrencia'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
