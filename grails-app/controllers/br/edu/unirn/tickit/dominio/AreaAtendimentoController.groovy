package br.edu.unirn.tickit.dominio


import org.codehaus.groovy.grails.commons.GrailsClassUtils
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AreaAtendimentoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond AreaAtendimento.list(params), model:[areaAtendimentoInstanceCount: AreaAtendimento.count()]
    }

    def show(AreaAtendimento areaAtendimentoInstance) {
        respond areaAtendimentoInstance
    }

    def create() {
        respond new AreaAtendimento(params)
    }

    @Transactional
    def save(AreaAtendimento areaAtendimentoInstance) {
         withForm {
            if (areaAtendimentoInstance == null) {
                notFound()
                return
            }


            areaAtendimentoInstance.usuarioCadastrante = session?.usuario
            areaAtendimentoInstance.email = params?.email
            areaAtendimentoInstance.clearErrors()
            areaAtendimentoInstance.save (flush:true)


             if (areaAtendimentoInstance.hasErrors()) {
                respond areaAtendimentoInstance.errors, view:'create'
                return
            }

            areaAtendimentoInstance.save flush:true

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'areaAtendimento.label', default: 'Área de Atendimento'), areaAtendimentoInstance], encodeAs: "None")
                    redirect action: "index", method: "GET"
                }
                '*' { respond areaAtendimentoInstance, [status: CREATED] }
            }
        }.invalidToken {
            flash.message = "Requisição inválida. Por favor, tente reiniciar a operação"
            redirect action: 'index'
        }
    }

    def edit(AreaAtendimento areaAtendimentoInstance) {
        respond areaAtendimentoInstance
    }

    @Transactional
    def update(AreaAtendimento areaAtendimentoInstance) {
        if (areaAtendimentoInstance == null) {
            notFound()
            return
        }

        if (areaAtendimentoInstance.hasErrors()) {
            respond areaAtendimentoInstance.errors, view:'edit'
            return
        }

        areaAtendimentoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'AreaAtendimento.label', default: 'AreaAtendimento'), areaAtendimentoInstance.id])
                redirect areaAtendimentoInstance
            }
            '*'{ respond areaAtendimentoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(AreaAtendimento areaAtendimentoInstance) {

        if (areaAtendimentoInstance == null) {
            notFound()
            return
        }

        areaAtendimentoInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'AreaAtendimento.label', default: 'AreaAtendimento'), areaAtendimentoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'areaAtendimento.label', default: 'AreaAtendimento'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
