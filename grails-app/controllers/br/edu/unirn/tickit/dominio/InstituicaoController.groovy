package br.edu.unirn.tickit.dominio


import org.codehaus.groovy.grails.commons.GrailsClassUtils
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class InstituicaoController {

    static scaffold = true

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def instituicaoInstance = new Instituicao()
        def criteriaResult = Instituicao.createCriteria().list(params) {
            if(instituicaoInstance.properties.containsKey('ativo'))
                eq("ativo",Boolean.TRUE)

            if (params.q) {
                or {
                    GrailsClassUtils?.getStaticPropertyValue(Instituicao, "searchFields")?.each {
                        def property = GrailsClassUtils?.getPropertyType(Instituicao, it)
                        if (property && Number.isAssignableFrom(property) || (property?.isPrimitive() && property != boolean)) {
                            if (property == Integer)
                                eq(it, params.int("q"))
                        } else if (property == String) {
                            ilike(it, params.q + "%")
                        }
                    }
                }
            }
        }
        respond criteriaResult, model:[instituicaoInstanceCount: criteriaResult.totalCount, params:params]
    }

    @Transactional
    def save(Instituicao instituicaoInstance) {
        withForm{
            if (instituicaoInstance == null) {
                notFound()
                return
            }

            instituicaoInstance.usuarioCadastrante = session?.usuario

            instituicaoInstance.clearErrors()
            instituicaoInstance.save(flush: true)

            if (instituicaoInstance.hasErrors()) {
                respond instituicaoInstance.errors, view:'create'
                return
            }

            instituicaoInstance.save flush:true

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'instituicao.label', default: 'Instituição'),instituicaoInstance], encodeAs: "None")
                    redirect action: "index", method: "GET"
                }
                '*' { respond instituicaoInstance, [status: CREATED] }
            }
        }.invalidToken{
            flash.message = "Requisição inválida. Por favor, tente reiniciar a operação"
            redirect action: 'index'
        }
    }

    @Transactional
    def update(Instituicao instituicaoInstance) {
        withForm{
            if (instituicaoInstance == null) {
                notFound()
                return
            }

            if (instituicaoInstance.hasErrors()) {
                respond instituicaoInstance.errors, view:'edit'
                return
            }

            instituicaoInstance.save flush:true

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'Instituicao.label', default: 'Instituição'),instituicaoInstance], encodeAs: "None")
                    redirect action: "index", method: "GET"
                }
                '*'{ respond instituicaoInstance, [status: OK] }
            }
        }.invalidToken{
            flash.message = "Requisição inválida. Por favor, tente reiniciar a operação"
            redirect action: 'index'
        }
    }

    @Transactional
    def delete(Instituicao instituicaoInstance) {
        withForm{
            if (instituicaoInstance == null) {
                notFound()
                return
            }

            if(instituicaoInstance.properties.containsKey("ativo")){
                instituicaoInstance.ativo = false
                instituicaoInstance.save flush:true
            }else{
                instituicaoInstance.delete flush:true
            }

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'Instituicao.label', default: 'Instituição'),instituicaoInstance], encodeAs: "None")
                    redirect action:"index", method:"GET"
                }
                '*'{ render status: NO_CONTENT }
            }
        }.invalidToken{
            flash.message = "Requisição inválida. Por favor, tente reiniciar a operação"
            redirect action: 'index'
        }
    }
}
