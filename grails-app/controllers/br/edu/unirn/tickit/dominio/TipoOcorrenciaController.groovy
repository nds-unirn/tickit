package br.edu.unirn.tickit.dominio



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TipoOcorrenciaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond TipoOcorrencia.list(params), model:[tipoOcorrenciaInstanceCount: TipoOcorrencia.count()]
    }

    def show(TipoOcorrencia tipoOcorrenciaInstance) {
        respond tipoOcorrenciaInstance
    }

    def create() {
        respond new TipoOcorrencia(params)
    }

    @Transactional
    def save(TipoOcorrencia tipoOcorrenciaInstance) {
        if (tipoOcorrenciaInstance == null) {
            notFound()
            return
        }

          tipoOcorrenciaInstance.usuarioCadastrante = session?.usuario

        tipoOcorrenciaInstance.clearErrors()
        tipoOcorrenciaInstance.save(flush: true)

        if (tipoOcorrenciaInstance.hasErrors()) {
            respond tipoOcorrenciaInstance.errors, view:'create'
            return
        }

        tipoOcorrenciaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tipoOcorrencia.label', default: 'TipoOcorrencia'), tipoOcorrenciaInstance.id])
                redirect tipoOcorrenciaInstance
            }
            '*' { respond tipoOcorrenciaInstance, [status: CREATED] }
        }
    }

    def edit(TipoOcorrencia tipoOcorrenciaInstance) {
        respond tipoOcorrenciaInstance
    }

    @Transactional
    def update(TipoOcorrencia tipoOcorrenciaInstance) {
        if (tipoOcorrenciaInstance == null) {
            notFound()
            return
        }

        if (tipoOcorrenciaInstance.hasErrors()) {
            respond tipoOcorrenciaInstance.errors, view:'edit'
            return
        }

        tipoOcorrenciaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TipoOcorrencia.label', default: 'TipoOcorrencia'), tipoOcorrenciaInstance.id])
                redirect tipoOcorrenciaInstance
            }
            '*'{ respond tipoOcorrenciaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(TipoOcorrencia tipoOcorrenciaInstance) {

        if (tipoOcorrenciaInstance == null) {
            notFound()
            return
        }

        tipoOcorrenciaInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TipoOcorrencia.label', default: 'TipoOcorrencia'), tipoOcorrenciaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipoOcorrencia.label', default: 'TipoOcorrencia'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
