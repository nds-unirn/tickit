package br.edu.unirn.tickit.dominio


import org.codehaus.groovy.grails.commons.GrailsClassUtils
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class StatusController {

    static scaffold = true

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def statusInstance = new Status()
        def criteriaResult = Status.createCriteria().list(params) {
            if (statusInstance.properties.containsKey('ativo'))
                eq("ativo", Boolean.TRUE)

            if (params.q) {
                or {
                    GrailsClassUtils?.getStaticPropertyValue(Status, "searchFields")?.each {
                        def property = GrailsClassUtils?.getPropertyType(Status, it)
                        if (property && Number.isAssignableFrom(property) || (property?.isPrimitive() && property != boolean)) {
                            if (property == Integer)
                                eq(it, params.int("q"))
                        } else if (property == String) {
                            ilike(it, params.q + "%")
                        }
                    }
                }
            }
        }
        respond criteriaResult, model: [statusInstanceCount: criteriaResult.totalCount, params: params]
    }

    @Transactional
    def save(Status statusInstance) {
        withForm {
            if (statusInstance == null) {
                notFound()
                return
            }

            statusInstance.usuarioCadastrante = session?.usuario
            statusInstance.clearErrors()
            statusInstance.save(flush: true)

            if (statusInstance.hasErrors()) {
                respond statusInstance.errors, view: 'create'
                return
            }

            statusInstance.save flush: true

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'status.label', default: 'Status'), statusInstance], encodeAs: "None")
                    redirect action: "index", method: "GET"
                }
                '*' { respond statusInstance, [status: CREATED] }
            }
        }.invalidToken {
            flash.message = "Requisição inválida. Por favor, tente reiniciar a operação"
            redirect action: 'index'
        }
    }

    @Transactional
    def update(Status statusInstance) {
        withForm {
            if (statusInstance == null) {
                notFound()
                return
            }

            if (statusInstance.hasErrors()) {
                respond statusInstance.errors, view: 'edit'
                return
            }

            statusInstance.save flush: true

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'Status.label', default: 'Status'), statusInstance], encodeAs: "None")
                    redirect action: "index", method: "GET"
                }
                '*' { respond statusInstance, [status: OK] }
            }
        }.invalidToken {
            flash.message = "Requisição inválida. Por favor, tente reiniciar a operação"
            redirect action: 'index'
        }
    }

    @Transactional
    def delete(Status statusInstance) {
        withForm {
            if (statusInstance == null) {
                notFound()
                return
            }

            if (statusInstance.properties.containsKey("ativo")) {
                statusInstance.ativo = false
                statusInstance.save flush: true
            } else {
                statusInstance.delete flush: true
            }

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'Status.label', default: 'Status'), statusInstance], encodeAs: "None")
                    redirect action: "index", method: "GET"
                }
                '*' { render status: NO_CONTENT }
            }
        }.invalidToken {
            flash.message = "Requisição inválida. Por favor, tente reiniciar a operação"
            redirect action: 'index'
        }
    }
}
