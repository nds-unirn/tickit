package br.edu.unirn.tickit.dominio


import br.edu.unirn.tickit.acesso.Usuario
import br.edu.unirn.tickit.acesso.Perfil
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import java.util.regex.Pattern
import java.util.regex.Matcher
import java.text.SimpleDateFormat

@Transactional(readOnly = true)
class ChamadoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Chamado.list(params), model:[chamadoInstanceCount: Chamado.count()]
    }

    @Transactional
    def show(Chamado chamadoInstance) {




         /**
        *Condiçao onde um comentário é adicionado do chamado
        */
        if(params.comentario != null && !params.comentario.trim().equals("")){

            ComentarioChamado cc = new ComentarioChamado()

            cc.comentario = params.comentario
            cc.usuarioCadastrante = session.usuario
            cc.chamado = Chamado.get(chamadoInstance.id)
            cc.save flush:true

            render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                    responsaveis:Usuario.findAllWhere(ativo:true),
                    equipamentos:Equipamento.findAllWhere(ativo:true),
                    statusChamado:Status.findAllWhere(ativo:true),
                    historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                    status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                    equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
                    comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                    responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                    anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                    mensagemSucesso:"Comentário adicionado com sucesso."])
                    return

        }



        /**
        *Condiçao onde um responsável é removido do chamado
        */
        if(params.usuarioId != null){


            if(session?.usuario?.hasPerfil(Perfil.findByNome("Padrão"))){
                UsuarioChamado uc = new UsuarioChamado();
                if(!uc.isUsuarioChamado(chamadoInstance, session?.usuario)){
                     render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                    responsaveis:Usuario.findAllWhere(ativo:true),
                    equipamentos:Equipamento.findAllWhere(ativo:true),
                    statusChamado:Status.findAllWhere(ativo:true),
                    historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                    status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                    equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
                    comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                    responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                    anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                    mensagemErro:"Apenas responsaveis pelo chamado poderão remover responsaveis do chamado."])
                    return
                }
            }


            UsuarioChamado us = UsuarioChamado.get(params.long('usuarioId'))

            us.ativo = false

            us.save flush:true

            render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                    responsaveis:Usuario.findAllWhere(ativo:true),
                    equipamentos:Equipamento.findAllWhere(ativo:true),
                    statusChamado:Status.findAllWhere(ativo:true),
                    historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                    status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                    equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
                    comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                    responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                    anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                    mensagemSucesso:"Responsável removido com sucesso."])
                    return

        }

         /**
        *Condiçao onde um responsável é adicionado ao chamado
        */
        if(params.addResponsavel != null){

            if(session?.usuario?.hasPerfil(Perfil.findByNome("Padrão"))){
                UsuarioChamado uc = new UsuarioChamado();
                if(!uc.isUsuarioChamado(chamadoInstance, session?.usuario)){
                     render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                    responsaveis:Usuario.findAllWhere(ativo:true),
                    equipamentos:Equipamento.findAllWhere(ativo:true),
                    statusChamado:Status.findAllWhere(ativo:true),
                    historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                    status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                    equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
                    comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                    responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                    anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                    mensagemErro:"Apenas responsaveis pelo chamado poderão adicionar responsável ao chamado."])
                    return
                }
            }


            def responsavelDoChamado = UsuarioChamado.executeQuery("select us from UsuarioChamado us where us.chamado = :chamado and us.usuario = :usuario and us.ativo = true", [chamado:chamadoInstance, usuario:Usuario.get(params.long('addResponsavel'))])

             /**
            *Verifica se um responsável já está vinculado ao chamado
            */
            if(!responsavelDoChamado.isEmpty()){


                render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                responsaveis:Usuario.findAllWhere(ativo:true),
                equipamentos:Equipamento.findAllWhere(ativo:true),
                statusChamado:Status.findAllWhere(ativo:true),
                historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
                comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                mensagemErro:"Responsável já está vinculado ao chamado."])
                return
            }
                    
            else{

                UsuarioChamado uc = new UsuarioChamado()
                uc.usuario = Usuario.get(params.long('addResponsavel'))
                uc.chamado = Chamado.get(chamadoInstance.id)
                uc.usuarioCadastrante = session.usuario

                uc.save flush:true

                render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                responsaveis:Usuario.findAllWhere(ativo:true),
                equipamentos:Equipamento.findAllWhere(ativo:true),
                statusChamado:Status.findAllWhere(ativo:true),
                historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
                comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                mensagemSucesso:"Responsável adicionado com sucesso."])
                return

            }
        }



         /**
        *Condiçao onde um anexo é removido do chamado
        */
        if(params.anexoId != null){


            if(session?.usuario?.hasPerfil(Perfil.findByNome("Padrão"))){
                UsuarioChamado uc = new UsuarioChamado();
                if(!uc.isUsuarioChamado(chamadoInstance, session?.usuario)){
                     render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                    responsaveis:Usuario.findAllWhere(ativo:true),
                    equipamentos:Equipamento.findAllWhere(ativo:true),
                    statusChamado:Status.findAllWhere(ativo:true),
                    historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                    status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                    equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
                    comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                    responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                    anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                    mensagemErro:"Apenas responsaveis pelo chamado poderão remover anexos do chamado."])
                    return
                }
            }

            AnexoChamado ac = AnexoChamado.get(params.long('anexoId'))

            ac.ativo = false

            ac.save flush:true


            render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
            responsaveis:Usuario.findAllWhere(ativo:true),
            equipamentos:Equipamento.findAllWhere(ativo:true),
            statusChamado:Status.findAllWhere(ativo:true),
            historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
            status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
            comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
            responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
            equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
            anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
            mensagemSucesso:"Anexo removido com sucesso."])
            return
        }


         /**
        *Condiçao onde um equipamento é removido do chamado
        */
        if(params.equipamentoId != null){

            if(session?.usuario?.hasPerfil(Perfil.findByNome("Padrão"))){
                UsuarioChamado uc = new UsuarioChamado();
                if(!uc.isUsuarioChamado(chamadoInstance, session?.usuario)){
                     render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                    responsaveis:Usuario.findAllWhere(ativo:true),
                    equipamentos:Equipamento.findAllWhere(ativo:true),
                    statusChamado:Status.findAllWhere(ativo:true),
                    historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                    status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                    equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
                    comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                    responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                    anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                    mensagemErro:"Apenas responsaveis pelo chamado poderão remover equipamentos do chamado."])
                    return
                }
            }

            EquipamentoChamado ec = EquipamentoChamado.get(params.long('equipamentoId'))

            ec.ativo = false

            ec.save flush:true


            render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
            responsaveis:Usuario.findAllWhere(ativo:true),
            equipamentos:Equipamento.findAllWhere(ativo:true),
            statusChamado:Status.findAllWhere(ativo:true),
            historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
            status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
            comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
            responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
            equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
            anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
            mensagemSucesso:"Equipamento removido com sucesso."])
            return
        }

        /**
        *Condiçao onde um equipamento é adicionado ao chamado
        */
        if(params.addEquipamento != null){

            if(session?.usuario?.hasPerfil(Perfil.findByNome("Padrão"))){
                UsuarioChamado uc = new UsuarioChamado();
                if(!uc.isUsuarioChamado(chamadoInstance, session?.usuario)){
                     render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                    responsaveis:Usuario.findAllWhere(ativo:true),
                    equipamentos:Equipamento.findAllWhere(ativo:true),
                    statusChamado:Status.findAllWhere(ativo:true),
                    historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                    status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                    equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
                    comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                    responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                    anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                    mensagemErro:"Apenas responsaveis pelo chamado poderão adicionar equipamentos ao chamado."])
                    return
                }
            }


            def equipamentoDoChamado = EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.equipamento = :equipamento and ec.ativo = true", [chamado:chamadoInstance, equipamento:Equipamento.get(params.long('addEquipamento'))])

             /**
            *Verifica se um equipamento já está vinculado ao chamado
            */
            if(!equipamentoDoChamado.isEmpty()){


                render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                responsaveis:Usuario.findAllWhere(ativo:true),
                equipamentos:Equipamento.findAllWhere(ativo:true),
                statusChamado:Status.findAllWhere(ativo:true),
                historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
                anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                mensagemErro:"Equipamento já está vinculado ao chamado."])
                return
            }
                    
            else{

                EquipamentoChamado ec = new EquipamentoChamado()
                ec.equipamento = Equipamento.get(params.long('addEquipamento'))
                ec.chamado = Chamado.get(chamadoInstance.id)
                ec.usuarioCadastrante = session.usuario

                ec.save flush:true

                render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                responsaveis:Usuario.findAllWhere(ativo:true),
                equipamentos:Equipamento.findAllWhere(ativo:true),
                statusChamado:Status.findAllWhere(ativo:true),
                historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
                anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                mensagemSucesso:"Equipamento adicionado com sucesso."])
                return

            }
        }


        //Alterar status do chamado

        if(params.status != null){




            if(session?.usuario?.hasPerfil(Perfil.findByNome("Padrão"))){
                UsuarioChamado uc = new UsuarioChamado();
                if(!uc.isUsuarioChamado(chamadoInstance, session?.usuario)){
                     render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                    responsaveis:Usuario.findAllWhere(ativo:true),
                    equipamentos:Equipamento.findAllWhere(ativo:true),
                    statusChamado:Status.findAllWhere(ativo:true),
                    historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                    status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                    equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
                    comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                    responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                    anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                    mensagemErro:"Apenas responsaveis pelo chamado poderão alterar o status do chamado."])
                    return
                }
            }


            //Buscando o Anterior
            def statusAnterior = StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance])

            

            //Desativando o status anterior
            for(StatusChamado st: statusAnterior){
                
                st.ativo = false
                st.save flush:true  
            }


            //Criando um novo
            StatusChamado novoStatus = new StatusChamado()
            novoStatus.usuarioCadastrante = session?.usuario
            novoStatus.chamado = Chamado.get(chamadoInstance.id)
            novoStatus.status = Status.get(params.long('status'))

            novoStatus.save flush:true

            render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
            responsaveis:Usuario.findAllWhere(ativo:true),
            equipamentos:Equipamento.findAllWhere(ativo:true),
            historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
            statusChamado:Status.findAllWhere(ativo:true),
            status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
            comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
            responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
            equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
            anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
            mensagemSucesso:"Status alterado com sucesso."])
            return

        }

        [chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
        responsaveis:Usuario.findAllWhere(ativo:true),
        equipamentos:Equipamento.findAllWhere(ativo:true),
        statusChamado:Status.findAllWhere(ativo:true),
        historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC ", [chamado:chamadoInstance]),
        status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
        equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
        responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
        anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
        comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
        mensagemErro:params?.mensagemErro]
    }

    def create() {
        respond new Chamado(params)
    }

    @Transactional
    def save(Chamado chamadoInstance) {
        if (chamadoInstance == null) {
            notFound()
            return
        }

        if (chamadoInstance.hasErrors()) {
            respond chamadoInstance.errors, view:'create'
            return
        }


        chamadoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'chamado.label', default: 'Chamado'), chamadoInstance.id])
                redirect chamadoInstance
            }
            '*' { respond chamadoInstance, [status: CREATED] }
        }
    }

    def edit(Chamado chamadoInstance) {
        respond chamadoInstance
    }

    @Transactional
    def update(Chamado chamadoInstance) {

        if(session?.usuario?.hasPerfil(Perfil.findByNome("Padrão"))){
            UsuarioChamado uc = new UsuarioChamado();
            if(!uc.isUsuarioChamado(chamadoInstance, session?.usuario)){
                long idChamado = chamadoInstance?.id
                chamadoInstance?.discard()
                redirect action:'show', id:idChamado, params:[mensagemErro:"Apenas responsaveis pelo chamado poderão alterar a área de atendimento."]
                return false
            }
        }
        
        if (chamadoInstance == null) {
            notFound()
            return
        }

        if (chamadoInstance.hasErrors()) {
            respond chamadoInstance.errors, view:'edit'
            return
        }

        chamadoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Chamado.label', default: 'Chamado'), chamadoInstance.id])
                redirect chamadoInstance
            }
            '*'{ respond chamadoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Chamado chamadoInstance) {

        if (chamadoInstance == null) {
            notFound()
            return
        }

        chamadoInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Chamado.label', default: 'Chamado'), chamadoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'chamado.label', default: 'Chamado'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Transactional
    def upload(Chamado chamadoInstance){
        def f = request.getFile('anexo')


        if(session?.usuario?.hasPerfil(Perfil.findByNome("Padrão"))){
                UsuarioChamado uc = new UsuarioChamado();
                if(!uc.isUsuarioChamado(chamadoInstance, session?.usuario)){
                     render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                    responsaveis:Usuario.findAllWhere(ativo:true),
                    equipamentos:Equipamento.findAllWhere(ativo:true),
                    statusChamado:Status.findAllWhere(ativo:true),
                    historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                    status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                    equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
                    comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                    responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                    anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                    mensagemErro:"Apenas responsaveis pelo chamado poderão fazer upload de anexos."])
                    return
                }
            }


        if(!f.empty) {


              if ( !( f.getContentType().equalsIgnoreCase( "image/png") || f.getContentType().equalsIgnoreCase( "application/msword")
              || f.getContentType().equalsIgnoreCase( "application/pdf") 
              || f.getContentType().equalsIgnoreCase( "image/jpeg") 
              || f.getContentType().equalsIgnoreCase( "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
              || f.getContentType().equalsIgnoreCase( "application/vnd.ms-excel")
              || f.getContentType().equalsIgnoreCase( "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
              || f.getContentType().equalsIgnoreCase( "application/vnd.ms-powerpoint")
              || f.getContentType().equalsIgnoreCase( "application/vnd.openxmlformats-officedocument.presentationml.presentation")
              ) )
             {
               render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                responsaveis:Usuario.findAllWhere(ativo:true),
                equipamentos:Equipamento.findAllWhere(ativo:true),
                historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                statusChamado:Status.findAllWhere(ativo:true),
                status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
                anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                mensagemErro:"Formato de arquivo inválido inválido!"
                ])
                return 
             }


             //Condição que verifica se o nome tem algum caractere especial
          
            Pattern p = Pattern.compile("[^a-zA-Z0-9\\.]", Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(f.getOriginalFilename());
            boolean b = m.find();

            if (b){
               render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                responsaveis:Usuario.findAllWhere(ativo:true),
                equipamentos:Equipamento.findAllWhere(ativo:true),
                historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                statusChamado:Status.findAllWhere(ativo:true),
                status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
                anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                mensagemErro:"O nome do arquivo nao pode conter caracteres especiais."
                ])
                return  
            }
            

        }    
        else {
           render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                responsaveis:Usuario.findAllWhere(ativo:true),
                equipamentos:Equipamento.findAllWhere(ativo:true),
                historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                statusChamado:Status.findAllWhere(ativo:true),
                status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance]),
                anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                mensagemErro:"Nao pode ser vazio!"
                ])
                return 
        }

            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyHHmmss")

            String novoNome = "${sdf.format(new Date())}"+"_"+f.getOriginalFilename()

        //now transfer file
            def webrootDir = servletContext.getRealPath("/") //app directory
            File fileDest = new File(webrootDir,"images/${novoNome}")
            f.transferTo(fileDest)

            AnexoChamado ac = new AnexoChamado()
            ac.nomeArquivo = novoNome
            ac.chamado = Chamado.get(chamadoInstance.id)
            ac.usuarioCadastrante = session?.usuario

            ac.save flush:true
           

            render(view:"show", model:[chamadoInstance:chamadoInstance, areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),
                responsaveis:Usuario.findAllWhere(ativo:true),
                equipamentos:Equipamento.findAllWhere(ativo:true),
                historicoStatus:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado ORDER BY sc.dateCreated DESC", [chamado:chamadoInstance]),
                statusChamado:Status.findAllWhere(ativo:true),
                mensagemSucesso:"Arquivo submetido com sucesso.",
                anexosDoChamado:AnexoChamado.executeQuery("select ac from AnexoChamado ac where ac.chamado = :chamado and ac.ativo = true ", [chamado:chamadoInstance]),
                status:StatusChamado.executeQuery("select sc from StatusChamado sc where sc.chamado = :chamado and sc.ativo = true ", [chamado:chamadoInstance]),
                comentariosChamado:ComentarioChamado.executeQuery("select cc from ComentarioChamado cc where cc.chamado = :chamado and cc.ativo = true ORDER BY cc.dateCreated ASC", [chamado:chamadoInstance]),
                responsaveisChamado:UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.chamado = :chamado and uc.ativo = true ORDER BY uc.dateCreated DESC", [chamado:chamadoInstance]),
                equipamentosChamado:EquipamentoChamado.executeQuery("select ec from EquipamentoChamado ec where ec.chamado = :chamado and ec.ativo = true ORDER BY ec.dateCreated DESC", [chamado:chamadoInstance])
                ])
                return

    }
}
