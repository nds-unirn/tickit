package br.edu.unirn.tickit.dominio


import org.codehaus.groovy.grails.commons.GrailsClassUtils
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import java.text.SimpleDateFormat
import java.text.DateFormat

@Transactional(readOnly = true)
class EquipamentoController {

    static scaffold = true


      def create() {

        def caracteristicas = Caracteristica.executeQuery("from Caracteristica c where c.ativo = true order by c.nome asc")

        [caracteristicas:caracteristicas]
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def equipamentoInstance = new Equipamento()
        def criteriaResult = Equipamento.createCriteria().list(params) {
            if (equipamentoInstance.properties.containsKey('ativo'))
                eq("ativo", Boolean.TRUE)

            if(params?.nome)
                ilike("nome","%"+params?.nome+"%")

            if(params?.tipoEquipamento?.id){
                createAlias("tipoEquipamento","tipoEquipamento")
                eq("tipoEquipamento.id", params?.tipoEquipamento?.id?.toLong())
            }

            order "nome","asc"
        }
        respond criteriaResult, model: [equipamentoInstanceCount: criteriaResult.totalCount, params: params]
    }

    @Transactional
    def save(Equipamento equipamentoInstance) {
        withForm {
            if (equipamentoInstance == null) {
                notFound()
                return
            }

            DateFormat sdf = new SimpleDateFormat("dd/MM/yyy HH:mm")

            equipamentoInstance.dataCompra = sdf.parse(params.dataCompra)

            equipamentoInstance.usuarioCadastrante = session?.usuario

            equipamentoInstance.clearErrors()
            equipamentoInstance.save(flush: true)

            if (equipamentoInstance.hasErrors()) {
                respond equipamentoInstance.errors, view: 'create'
                return
            }

            equipamentoInstance.save flush: true

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'equipamento.label', default: 'Equipamento'), equipamentoInstance], encodeAs: "None")
                    redirect action: "index", method: "GET"
                }
                '*' { respond equipamentoInstance, [status: CREATED] }
            }
        }.invalidToken {
            flash.message = "Requisição inválida. Por favor, tente reiniciar a operação"
            redirect action: 'index'
        }
    }

    @Transactional
    def update(Equipamento equipamentoInstance) {
        withForm {
            if (equipamentoInstance == null) {
                notFound()
                return
            }

            if (equipamentoInstance.hasErrors()) {
                respond equipamentoInstance.errors, view: 'edit'
                return
            }

            equipamentoInstance.save flush: true

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'Equipamento.label', default: 'Equipamento'), equipamentoInstance], encodeAs: "None")
                    redirect action: "index", method: "GET"
                }
                '*' { respond equipamentoInstance, [status: OK] }
            }
        }.invalidToken {
            flash.message = "Requisição inválida. Por favor, tente reiniciar a operação"
            redirect action: 'index'
        }
    }

    @Transactional
    def delete(Equipamento equipamentoInstance) {
        withForm {
            if (equipamentoInstance == null) {
                notFound()
                return
            }

            if (equipamentoInstance.properties.containsKey("ativo")) {
                equipamentoInstance.ativo = false
                equipamentoInstance.save flush: true
            } else {
                equipamentoInstance.delete flush: true
            }

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'Equipamento.label', default: 'Equipamento'), equipamentoInstance], encodeAs: "None")
                    redirect action: "index", method: "GET"
                }
                '*' { render status: NO_CONTENT }
            }
        }.invalidToken {
            flash.message = "Requisição inválida. Por favor, tente reiniciar a operação"
            redirect action: 'index'
        }
    }


    @Transactional
    def show(Equipamento equipamentoInstance){

        def setores = Setor.findAllWhere(ativo:true)


        if(params.setor != null){

            def setor = Setor.get(params.long('setor'))

            def vinculosAnteriores = EquipamentoSetor.executeQuery("select es from EquipamentoSetor es where es.equipamento = :equipamento ", [equipamento:equipamentoInstance])

            if(!vinculosAnteriores.isEmpty()){


                //Verifica se o setor escolhido é o mesmo que o último ativo
                for(EquipamentoSetor eqSetor: vinculosAnteriores){

                    if(eqSetor.setor == setor && eqSetor.ativo ){
                        render(view:"show", model:[setores:setores, mensagemErro:"Equipamento já está vinculado a este Setor", equipamentoInstance:equipamentoInstance, 
                        vinculos:EquipamentoSetor.executeQuery("select es from EquipamentoSetor es where es.equipamento = :equipamento ORDER BY es.dateCreated DESC", [equipamento:equipamentoInstance])])
                        return
                    }
                    

                }
                

                //Desativar todos anteriores
                for(EquipamentoSetor eqSetor: vinculosAnteriores){

                    eqSetor.ativo = false
                    eqSetor.save flush:true

                }


                //Após desativar os anteriores, cria um novo
                EquipamentoSetor es = new EquipamentoSetor()
                es.setor = setor
                es.equipamento = Equipamento.get(equipamentoInstance.id)
                es.usuarioCadastrante = session?.usuario
               
                es.save flush: true
            }
            else{

                //Primeiro vinculo
                EquipamentoSetor es = new EquipamentoSetor()
                es.setor = setor
                es.equipamento = Equipamento.get(equipamentoInstance.id)
                es.usuarioCadastrante = session?.usuario
               
                es.save flush: true
            }

             render(view:"show", model:[setores:setores, mensagemSucesso:"Equipamento vinculado!" , equipamentoInstance:equipamentoInstance, 
            vinculos:EquipamentoSetor.executeQuery("select es from EquipamentoSetor es where es.equipamento = :equipamento ORDER BY es.dateCreated DESC", [equipamento:equipamentoInstance])])
            return

        }else{

            render(view:"show", model:[setores:setores, equipamentoInstance:equipamentoInstance, 
            vinculos:EquipamentoSetor.executeQuery("select es from EquipamentoSetor es where es.equipamento = :equipamento ORDER BY es.dateCreated DESC", [equipamento:equipamentoInstance])])
            return
        }

     }
}
