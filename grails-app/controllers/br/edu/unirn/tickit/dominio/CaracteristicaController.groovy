package br.edu.unirn.tickit.dominio



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CaracteristicaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
       
        int offset = params.offset!=null?Integer.parseInt(params.offset):0
        Map variaveis = new HashMap()
        String query = "from Caracteristica c where c.ativo = true "
        def caracteristicas
        def totalCaracteristicas

        variaveis.put("max", 10);
        variaveis.put("offset", offset)
        caracteristicas = Caracteristica.executeQuery(query+" order by c.nome asc ", variaveis)

        totalCaracteristicas = Caracteristica.executeQuery("select count(c)  "+query,variaveis)

        render view:"index", model:[caracteristicaInstanceCount: totalCaracteristicas[0], caracteristicaInstanceList:caracteristicas, params:params,tipoMensagem:params?.tipoMensagem, textoMensagem:params?.textoMensagem ]
        return
    }

    def show(Caracteristica caracteristicaInstance) {
        respond caracteristicaInstance
    }

    def create() {
        respond new Caracteristica(params)
    }

    @Transactional
    def save(Caracteristica caracteristicaInstance) {
        if (caracteristicaInstance == null) {
            notFound()
            return
        }


        caracteristicaInstance.usuarioCadastrante = session?.usuario

        caracteristicaInstance.clearErrors()
        caracteristicaInstance.save (flush:true)


        if (caracteristicaInstance.hasErrors()) {
            respond caracteristicaInstance.errors, view:'create'
            return
        }

        caracteristicaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = "Característica '${caracteristicaInstance?.nome}' criada."
                redirect caracteristicaInstance
            }
            '*' { respond caracteristicaInstance, [status: CREATED] }
        }
    }

    def edit(Caracteristica caracteristicaInstance) {
        respond caracteristicaInstance
    }

    @Transactional
    def update(Caracteristica caracteristicaInstance) {
        if (caracteristicaInstance == null) {
            notFound()
            return
        }

        if (caracteristicaInstance.hasErrors()) {
            respond caracteristicaInstance.errors, view:'edit'
            return
        }

        caracteristicaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = "Característica atualizada!"
                redirect caracteristicaInstance
            }
            '*'{ respond caracteristicaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Caracteristica caracteristicaInstance) {

        if (caracteristicaInstance == null) {
            notFound()
            return
        }

        caracteristicaInstance.ativo = false
        caracteristicaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = "Característica '${caracteristicaInstance?.nome}' removida."
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'caracteristica.label', default: 'Caracteristica'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
