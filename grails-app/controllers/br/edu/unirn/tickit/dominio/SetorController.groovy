package br.edu.unirn.tickit.dominio


import org.codehaus.groovy.grails.commons.GrailsClassUtils
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class SetorController {

    static scaffold = true

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def setorInstance = new Setor()
        def criteriaResult = Setor.createCriteria().list(params) {
            if (setorInstance.properties.containsKey('ativo')){
                createAlias("instituicao","instituicao")
                eq("instituicao.ativo",Boolean.TRUE)
                eq("ativo", Boolean.TRUE)
            }

            if (params.q) {
                or {
                    GrailsClassUtils?.getStaticPropertyValue(Setor, "searchFields")?.each {
                        def property = GrailsClassUtils?.getPropertyType(Setor, it)
                        if (property && Number.isAssignableFrom(property) || (property?.isPrimitive() && property != boolean)) {
                            if (property == Integer)
                                eq(it, params.int("q"))
                        } else if (property == String) {
                            ilike(it, params.q + "%")
                        }
                    }
                }
            }
        }
        respond criteriaResult, model: [setorInstanceCount: criteriaResult.totalCount, params: params]
    }

    @Transactional
    def save(Setor setorInstance) {
        withForm {
            if (setorInstance == null) {
                notFound()
                return
            }

            setorInstance.usuarioCadastrante = session?.usuario

            setorInstance.clearErrors()
            setorInstance.save(flush: true)

            if (setorInstance.hasErrors()) {
                respond setorInstance.errors, view: 'create'
                return
            }

            setorInstance.save flush: true

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'setor.label', default: 'Setor'), setorInstance], encodeAs: "None")
                    redirect action: "index", method: "GET"
                }
                '*' { respond setorInstance, [status: CREATED] }
            }
        }.invalidToken {
            flash.message = "Requisição inválida. Por favor, tente reiniciar a operação"
            redirect action: 'index'
        }
    }

    @Transactional
    def update(Setor setorInstance) {
        withForm {
            if (setorInstance == null) {
                notFound()
                return
            }

            if (setorInstance.hasErrors()) {
                respond setorInstance.errors, view: 'edit'
                return
            }

            setorInstance.save flush: true

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'Setor.label', default: 'Setor'), setorInstance], encodeAs: "None")
                    redirect action: "index", method: "GET"
                }
                '*' { respond setorInstance, [status: OK] }
            }
        }.invalidToken {
            flash.message = "Requisição inválida. Por favor, tente reiniciar a operação"
            redirect action: 'index'
        }
    }

    @Transactional
    def delete(Setor setorInstance) {
        withForm {
            if (setorInstance == null) {
                notFound()
                return
            }

            if (setorInstance.properties.containsKey("ativo")) {
                setorInstance.ativo = false
                setorInstance.save flush: true
            } else {
                setorInstance.delete flush: true
            }

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'Setor.label', default: 'Setor'), setorInstance], encodeAs: "None")
                    redirect action: "index", method: "GET"
                }
                '*' { render status: NO_CONTENT }
            }
        }.invalidToken {
            flash.message = "Requisição inválida. Por favor, tente reiniciar a operação"
            redirect action: 'index'
        }
    }
}
