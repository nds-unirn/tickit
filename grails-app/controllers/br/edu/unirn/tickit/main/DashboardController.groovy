package br.edu.unirn.tickit.main

class DashboardController {

    def dashboardService

    def index() {
    	

    	if(params.administrador == null){
    		flash.message = null
    	}

        def result = dashboardService.listaChamados(params);

        [result:result.retorno, params:params]
    }
}
