package br.edu.unirn.tickit.main

import br.edu.unirn.tickit.dominio.Chamado
import br.edu.unirn.tickit.dominio.Equipamento
import br.edu.unirn.tickit.dominio.TipoEquipamento
import br.edu.unirn.tickit.dominio.command.RelatorioChamadoCommand
import br.edu.unirn.tickit.dominio.command.RelatorioEquipamentoCommand
import java.text.SimpleDateFormat
import java.text.DateFormat

class RelatoriosController {

    def relatoriosService

    def index(){
        forward action: 'equipamentos', params: params
    }

    def equipamentos(RelatorioEquipamentoCommand relatorioEquipamentoCommand){


        int offset = params.offset!=null?Integer.parseInt(params.offset):0
        Map variaveis = new HashMap()
        String query = "from Equipamento e where e.ativo = true "
        def equipamentos
        def totalEquipamentos
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
        flash.message = null

   
         if(params?.nome != null && !params?.nome.trim().equals("")){

            query += " and e.nome like '%"+ params?.nome +"%' "           

        }

        if(params?.tipoEquipamento != null && !params?.tipoEquipamento.trim().equals("")){

            query += " and e.tipoEquipamento = :tipoEquipamento "
            def tipoEquipamento = TipoEquipamento.get(params.long('tipoEquipamento'))

            variaveis.put("tipoEquipamento", tipoEquipamento)

        }

        //Se a Data incial estiver preenchida, entra aqui
        if(params?.dataInicioCompra != null && !params?.dataInicioCompra.trim().equals("")){

            //Se as duas datas estiverem preenchidas e o dateTime inicial for menor ou igual ao dateTime final, entra aqui

            if(params?.dataFinalCompra != null && !params?.dataFinalCompra.trim().equals("") && sdf.parse(params?.dataInicioCompra) <= sdf.parse(params?.dataFinalCompra)){

               query += " and e.dataCompra between :dataInicioCompra and :dataFinalCompra "
               variaveis.put("dataInicioCompra", sdf.parse(params?.dataInicioCompra))
               variaveis.put("dataFinalCompra", sdf.parse(params?.dataFinalCompra))
            }
            else {

                //Se apenas a data inicial estiver preenchida, entra aqui
                flash.message = "Os dois campos de data precisam ser preenchidos e a data inicial deve ser menor que a data final."
                variaveis.put("max", 10);
                variaveis.put("offset", offset)
                equipamentos = Equipamento.executeQuery(query+" order by e.dataCompra desc ", variaveis)

                totalEquipamentos = Equipamento.executeQuery("select count(e)  "+query,variaveis)
                [equipamentoInstanceList : equipamentos, equipamentoInstanceCount:totalEquipamentos[0], params:params]
                

            }

            //Se apenas a data final estiver preenchida, entra aqui

        }else if(params?.dataFinalCompra != null && !params?.dataFinalCompra.trim().equals("")){

                flash.message = "Os dois campos de data precisam ser preenchidos."
                variaveis.put("max", 10);
                variaveis.put("offset", offset)
                equipamentos = Equipamento.executeQuery(query+" order by e.dataCompra desc ", variaveis)

                totalEquipamentos = Equipamento.executeQuery("select count(e)  "+query,variaveis)
                [equipamentoInstanceList : equipamentos, equipamentoInstanceCount:totalEquipamentos[0], params:params]

        }

              

        variaveis.put("max", 10);
        variaveis.put("offset", offset)
        equipamentos = Equipamento.executeQuery(query+" order by e.dataCompra desc ", variaveis)

        totalEquipamentos = Equipamento.executeQuery("select count(e)  "+query,variaveis)

        [equipamentoInstanceList : equipamentos, equipamentoInstanceCount:totalEquipamentos[0], params:params]
    }

    def chamados(RelatorioChamadoCommand relatorioChamadoCommand){
        setParamsPadrao()
        params.each{ println it }
        def results = relatoriosService?.relatorioChamados(relatorioChamadoCommand, params)

        [chamadoInstanceList: results]
    }

}
