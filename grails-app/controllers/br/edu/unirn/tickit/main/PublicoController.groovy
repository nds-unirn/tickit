package br.edu.unirn.tickit.main
import br.edu.unirn.tickit.dominio.AreaAtendimento
import br.edu.unirn.tickit.dominio.Setor
import br.edu.unirn.tickit.dominio.Chamado
import br.edu.unirn.tickit.dominio.Status
import br.edu.unirn.tickit.dominio.StatusChamado
import br.edu.unirn.tickit.acesso.Usuario
import br.edu.unirn.tickit.acesso.UsuarioPerfil
import br.edu.unirn.tickit.acesso.Perfil

class PublicoController {
    def mailService

    def index() {
		[areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),setores:Setor.findAllWhere(ativo:true)]
    }

    def abrirChamado(){
    	Chamado chamadoInstance = new Chamado()
    	chamadoInstance?.areaAtendimento = AreaAtendimento.get(params?.long('areaAtendimento'))
    	chamadoInstance?.titulo = params?.titulo
    	chamadoInstance?.emailSolicitante = params?.emailSolicitante
    	chamadoInstance?.setorSolicitante = Setor.get(params?.long('setorSolicitante'))
    	chamadoInstance?.descricao = params?.descricao
    	if(chamadoInstance?.validate()){
            if(chamadoInstance?.save(flush:true)){
                StatusChamado statusChamado = new StatusChamado(status:Status.findByNome("Aberto"),chamado:chamadoInstance)
                statusChamado?.save(flush:true)

                //enviar email de notificação para o usuário solicitante
                mailService?.sendMail {
                    to chamadoInstance?.emailSolicitante
                    from "tickit@unirn.edu.br"
                    subject "Novo chamado - TickIT"
                    html """Olá, <br/> O chamado <strong>${chamadoInstance?.titulo}</strong> foi cadastrado no sistema, em breve a nossa
                    equipe irá atendê-lo. Abaixo encontra-se um resumo do mesmo: <br/><br/>
                    
                    <strong>#CHAMADO ${chamadoInstance?.id}</strong> <hr/><br/>
                    Data de cadastro: ${chamadoInstance?.getDateCreatedAsString()} <br/>
                    Área de atendimento: ${chamadoInstance?.areaAtendimento?.nome} <br/>
                    Título: ${chamadoInstance?.titulo} <br/>
                    Setor solicitante: ${chamadoInstance?.setorSolicitante?.nome} <br/>
                    Descrição: ${chamadoInstance?.descricao} <br/><br/><br/>
                    <hr/>
                    <div style='width:100%;text-align:center;'>Esta mensagem foi enviada automaticamente. Novas menasgem serão enviadas conforme surgirem novidades. <br/>
                    <strong>Tickit | Software desenvolvido pelo Núcleo de Desenvolvimento do UNI-RN </strong> </div>
                    """
                }

                //email para os administradores
               // def usuarios = UsuarioPerfil.executeQuery("select distinct up.usuario from UsuarioPerfil up where up.perfil = :perfil and up.ativo = true",[perfil:Perfil.findByNome('Administrador')]);
                //for(Usuario usuario : usuarios){
                //}

                //email para a área de atendimento
                mailService?.sendMail {
                    to chamadoInstance?.areaAtendimento?.email
                    from "tickit@unirn.edu.br"
                    subject "Novo chamado - TickIT"
                    html """Olá, <br/> Um novo chamado foi cadastrado no sistema. Abaixo encontra-se um resumo do mesmo: <br/><br/>
                    
                    <strong>#CHAMADO ${chamadoInstance?.id}</strong> <hr/><br/>
                    Data de cadastro: ${chamadoInstance?.getDateCreatedAsString()} <br/>
                    Área de atendimento: ${chamadoInstance?.areaAtendimento?.nome} <br/>
                    Título: ${chamadoInstance?.titulo} <br/>
                    Setor solicitante: ${chamadoInstance?.setorSolicitante?.nome} <br/>
                    Descrição: ${chamadoInstance?.descricao} <br/><br/><br/>
                    <hr/>
                    <div style='width:100%;text-align:center;'>Esta mensagem foi enviada automaticamente. Novas menasgem serão enviadas conforme surgirem novidades. <br/>
                    <strong>Tickit | Software desenvolvido pelo Núcleo de Desenvolvimento do UNI-RN </strong> </div>
                    """
                }

                flash.message = "O chamado foi aberto corretamente."
                render(view:"index",model:[areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),setores:Setor.findAllWhere(ativo:true),tipoMensagem:"success"])
                return       
            }else{
                flash.message = "Ocorreu um erro no sistema e o chamado não pode ser aberto, por favor tente novamente."
                render(view:"index",model:[chamadoInstance:chamadoInstance,areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),setores:Setor.findAllWhere(ativo:true),tipoMensagem:"error"])
                return       
            }
		}else{
			render(view:"index",model:[chamadoInstance:chamadoInstance,areasAtendimento:AreaAtendimento.findAllWhere(ativo:true),setores:Setor.findAllWhere(ativo:true)])
            return		
		}
    }

}
