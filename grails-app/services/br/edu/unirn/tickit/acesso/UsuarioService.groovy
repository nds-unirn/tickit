package br.edu.unirn.tickit.acesso

import grails.transaction.Transactional

@Transactional
class UsuarioService {

    /**
     * Verifica se a tabela de usuarios possui algum usuario com perfil 'Administrador
     * cadastrado e, se N�O houver, recria o usuario 'admin' criado no bootstrap
     */
    def recriarUsuarioAdminBootstrap() {
        if( isUsuariosAdminsEmpty() ) {
            def usuarioAdmin = Usuario.findByLogin("admin") ?: new Usuario(senha: "admin", login: 'admin', nome: 'administrador',email:'tickit@unirn.edu.br')
            usuarioAdmin.ativo = true
            usuarioAdmin.save(flush: true)
        }
    }


    /**
     * Verifica se a tabela de usuarios possui algum usuario com perfil 'Administrador'
     * cadastrado e ATIVO; se houver, remove o usuario 'admin' criado no bootstrap
     */
    def removerUsuarioAdminBootstrap(){
        def usuarioAdmin = Usuario.findByLogin("admin")
        usuarioAdmin.ativo = false
        usuarioAdmin?.save(flush: true)
    }

    /**
     * verifica se existe mais de 1 usuarioPerfil com perfil Administrador e usuario nao nulo,
     * pois 1 deles deve ser o usuario admin
     * @return
     */
    Boolean isUsuariosAdminsEmpty(){
        def perfilAdmin = Perfil.findByNomeAndAtivo(Perfil.ADMINISTRADOR, true)
        UsuarioPerfil.countByPerfilAndAtivoAndUsuarioIsNotNull(perfilAdmin, true) > 1
    }
}
