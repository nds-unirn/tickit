package br.edu.unirn.tickit.dashboard

import grails.transaction.Transactional

@Transactional
class DashboardService {

    def groovySql

    def listaChamados(params){

        def retorno

        StringBuilder sql = new StringBuilder()

        sql.append("""SELECT ch.data_de_cadastro AS 'data', ch.titulo AS 'titulo', aa.nome AS 'area_de_atendimento',
                        se.nome AS 'setor', st.nome AS 'status', ch.id AS 'id'
                        FROM chamado AS ch LEFT JOIN setor AS se ON ch.setor_solicitante_id = se.id
                        LEFT JOIN status_chamado AS stch ON stch.chamado_id = ch.id
                        LEFT JOIN status AS st ON stch.status_id = st.id
                        LEFT JOIN area_atendimento AS aa ON aa.id = ch.area_atendimento_id
                        LEFT JOIN usuario_chamado AS uch ON uch.chamado_id = ch.id""")

        if(params?.setor?.id || params?.status?.id || params?.areaAtendimento?.id || params?.responsavel?.id)
            sql.append(" WHERE")

        if (params?.setor?.id)
            sql.append(" se.id = ${params?.setor?.id}")

        if(params?.status?.id){
           if(params?.setor?.id){
               sql.append(" AND st.id = ${params?.status?.id}")
           }else {
               sql.append(" st.id = ${params?.status?.id}")
           }
        }

        if(params?.areaAtendimento?.id){
            if(params?.status?.id){
                sql.append(" AND aa.id = ${params?.areaAtendimento?.id}")
            }else {
                sql.append(" aa.id = ${params?.areaAtendimento?.id}")
            }
        }

        if(params?.responsavel?.id){
            if(params?.areaAtendimento?.id){
                sql.append(" AND uch.usuario_id = ${params?.responsavel?.id}")
            }else {
                sql.append(" uch.usuario_id = ${params?.responsavel?.id}")
            }
        }

        sql.append(" ORDER BY ch.data_de_cadastro ASC, st.nome ASC")
        retorno = groovySql.rows(sql.toString())

        [retorno:retorno]
    }


}
