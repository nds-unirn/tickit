package br.edu.unirn.tickit.main

import br.edu.unirn.tickit.dominio.command.RelatorioChamadoCommand
import br.edu.unirn.tickit.dominio.command.RelatorioEquipamentoCommand
import grails.transaction.Transactional

@Transactional
class RelatoriosService {

    def groovySql

    def relatorioEquipamentos(RelatorioEquipamentoCommand relatorioEquipamentoCommand, Map params){
        def query = makeQueryEquipamentos(relatorioEquipamentoCommand, params)
        def results = executeQuery(query) ?: []
        results
    }

    def relatorioChamados(RelatorioChamadoCommand relatorioChamadoCommand, Map params){
        def query = makeQueryChamados(relatorioChamadoCommand, params)
        def results = executeQuery(query) ?: []
        results
    }

    def executeQuery(String query){
        getResults(query)
    }

    def getResults(String query){
        groovySql.rows(query)
    }

    /**
     * monta query SQL a ser executada para consulta do relat�rio.
     * recebe mapa com um conjunto de par�metros da consulta
     * @param parametros
     */
    def makeQueryEquipamentos(RelatorioEquipamentoCommand relatorioEquipamentoCommand, Map params){
        // filtros: instituicao , setor , tipoEquipamento , locado , dataCompra
        def strQuery = """
                select
                    e.id as equipamento_id, e.data_compra, e.data_de_cadastro, e.locado, e.nome as equipamento, e.tombamento,
                    s.id as setor_id, s.nome as setor,
                    te.id as tipo_equipamento_id, te.nome as tipo_equipamento,
                    i.id as instituicao_id, i.nome as instituicao
                from
                    tickit_develop.equipamento e
                    left join tickit_develop.tipo_equipamento te on (e.tipo_equipamento_id = te.id)
                    left join tickit_develop.equipamento_setor es on (e.id = es.equipamento_id)
                    left join tickit_develop.setor s on (es.setor_id = s.id)
                    left join tickit_develop.instituicao i on (s.instituicao_id = i.id)
                where
                    e.id is not null
                    ${relatorioEquipamentoCommand?.instituicao?.id ? " and i.id = ${relatorioEquipamentoCommand?.instituicao?.id}" : ""}
                    ${relatorioEquipamentoCommand.setor ? " and s.id = ${relatorioEquipamentoCommand.setor.id}" : ""}
                    ${relatorioEquipamentoCommand.tipoEquipamento ? " and te.id = ${relatorioEquipamentoCommand.tipoEquipamento.id}" : ""}
                    ${relatorioEquipamentoCommand.locado ? " and e.locado = ${relatorioEquipamentoCommand.locado}" : ""}
                    ${params?.dataCompraInicio && params?.dataCompraFim? " and eq.data_compra between '${params?.date("dataInicio", "dd/MM/yyyy")}' and '${params?.date("dataInicio", "dd/MM/yyyy")}'" : ""}
                order by e.nome asc
            """
    }


    def makeQueryChamados(RelatorioChamadoCommand relatorioChamadoCommand, Map params){
        // filtros: instituicao , setor , tipoEquipamento , locado , dataCompra
        def strQuery = """
                select
                    c.id as chamado_id, c.titulo, c.ativo, c.data_de_cadastro, c.email_solicitante,
                    s.id as setor_id, s.nome as setor,
                    aa.id as area_atendimento_id, aa.nome as area_atendimento
                from tickit_develop.chamado c
                    left join tickit_develop.setor s on (c.setor_solicitante_id = s.id)
                    left join tickit_develop.area_atendimento aa on (aa.id = c.area_atendimento_id)
                where
                    c.id is not null
                    ${relatorioChamadoCommand.ativo ? " and c.ativo = ${relatorioChamadoCommand.ativo}" : ""}
                    ${relatorioChamadoCommand?.titulo ? " and c.titulo like '%${relatorioChamadoCommand?.titulo}%'" : ""}
                    ${relatorioChamadoCommand.emailSolicitante ? " and c.email_solicitante like '%${relatorioChamadoCommand.emailSolicitante}%'" : ""}
                    ${relatorioChamadoCommand.usuarioCadastrante ? " and c.usuario_cadastrante_id = ${relatorioChamadoCommand.usuarioCadastrante?.id}" : ""}
                    ${relatorioChamadoCommand.setorSolicitante ? " and c.setor_solicitante_id = ${relatorioChamadoCommand.setorSolicitante?.id}" : ""}
                    ${relatorioChamadoCommand.areaAtendimento ? " and c.area_atendimento_id = ${relatorioChamadoCommand.areaAtendimento?.id}" : ""}
                    ${params?.dateCreatedInicio && params?.dateCreatedFim? " and c.data_de_cadastro between '${params?.date("dateCreatedInicio", "dd/MM/yyyy")}' and '${params?.date("dateCreatedFim", "dd/MM/yyyy")}'" : ""}
                order by c.titulo asc
            """
    }

}
