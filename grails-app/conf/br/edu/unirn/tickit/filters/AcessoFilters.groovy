package br.edu.unirn.tickit.filters
import br.edu.unirn.tickit.acesso.Usuario
import br.edu.unirn.tickit.acesso.Perfil

class AcessoFilters {

    def filters = {
        all(controller:'*', action:'*', controllerExclude:'publico|autenticacao') {
            before = {
                if(session.usuario == null){
                    redirect(controller:'autenticacao',action: 'login')
                    return false
                }
            }
        }

        administrador(controller:'usuario|instituicao|setor|areaAtendimento|status|tipoOcorrencia|relatorios', action:'*') {
            before = {
                flash.message = null
                if(!session?.usuario?.hasPerfil(Perfil.findByNome("Administrador"))){
                    flash.message = "Usuário não tem permissão para acesso a esta página."
                    redirect(controller:'dashboard',action: 'index',params:[administrador:true])
                    return false
                }
            }
        }

        supervisor(controller:'equipamento|tipoEquipamento', action:'*') {
            before = {
                flash.message = null
                if(!session?.usuario?.hasPerfil(Perfil.findByNome("Administrador")) && !session?.usuario?.hasPerfil(Perfil.findByNome("Supervisor"))){
                    flash.message = "Usuário não tem permissão para acesso a esta página."
                    redirect(controller:'dashboard',action: 'index',params:[supervisor:true])
                    return false
                }
            }
        }        

    }
}
