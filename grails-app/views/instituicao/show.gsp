
<%@ page import="br.edu.unirn.tickit.dominio.Instituicao" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="administrativo">
	<g:set var="entityName" value="${message(code: 'instituicao.label', default: 'Instituição')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<section class="content-header">
	<ol class="breadcrumb">
		<li class="active">Listagem</li>
		<li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><g:message code="default.show.label" args="[entityName]" /></h3>
		</div>

		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<table class="table table-view table-condensed">
						<thead>
							<tr>
								<th colspan="2">
									Dados do ${entityName}
								</th>
							</tr>
						</thead>
						<tbody>
							
							<g:if test="${instituicaoInstance?.nome}">
								<tr>
									<td><strong><g:message code="instituicao.nome.label" default="Nome" />:</strong></td>
									
									<td><g:fieldValue bean="${instituicaoInstance}" field="nome"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${instituicaoInstance?.ativo}">
								<tr>
									<td><strong><g:message code="instituicao.ativo.label" default="Ativo" />:</strong></td>
									
									<td><g:formatBoolean boolean="${instituicaoInstance?.ativo}" /></td>
									
								</tr>
							</g:if>
							
							<g:if test="${instituicaoInstance?.dateCreated}">
								<tr>
									<td><strong><g:message code="instituicao.dateCreated.label" default="Data de Cadastro" />:</strong></td>
									
									<td><g:formatDate date="${instituicaoInstance?.dateCreated}" format="dd/MM/yyyy HH:mm"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${instituicaoInstance?.usuarioCadastrante}">
								<tr>
									<td><strong><g:message code="instituicao.usuarioCadastrante.label" default="Usuário Cadastrante" />:</strong></td>
									
									<td><g:link controller="usuario" action="show" id="${instituicaoInstance?.usuarioCadastrante?.id}">${instituicaoInstance?.usuarioCadastrante?.encodeAsHTML()}</g:link></td>
									
								</tr>
							</g:if>
							
						</tbody>
					</table>
					<div class="box-footer clearfix">
						<g:link action="index" class="btn btn-primary pull-right"><i class="fa fa-reply"></i>Voltar</g:link>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<g:javascript>
        $(document).ready(function(){
            TICKIT.selectMenu("instituicaoOpt");
        });
</g:javascript>
</body>
</html>
