
<%@ page import="br.edu.unirn.tickit.dominio.Instituicao" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="administrativo">
	<g:set var="entityName" value="${message(code: 'instituicao.label', default: 'Instituição')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
	<ol class="breadcrumb">
		<li class="active">Listagem</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><g:message code="default.list.label" args="[entityName]" /></h3>
			<div class="box-tools pull-right">
				<g:link class="btn btn-default btn-box-tool" action="create"><i class="fa fa-plus"></i> <g:message code="default.new.label" args="[entityName]" /></g:link>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<g:form action="index">
						<div class="input-group margin-bottom-10">
							<g:textField name="q" class="form-control input-sm  pull-right" placeholder="Pesquisar" style="width: 150px;" value="${query}"/>
							<div class="input-group-btn">
								<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
							</div>
						</div>
					</g:form>
				</div>
				<div class="col-xs-12">
					<g:if test="${flash.message}">
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
							<div class="message" role="status">${flash.message}</div>
						</div>
					</g:if>
					<table class="table table-bordered table-condensed">
						<thead>
						<tr>
							
							<g:sortableColumn params="[q: "${query?:''}"]" property="nome" title="${message(code: 'instituicao.nome.label', default: 'Nome')}" />
							


							<th width="9%" colspan="3">&nbsp;</th>
						</tr>
						</thead>
						<tbody>
						<g:each in="${instituicaoInstanceList}" status="i" var="instituicaoInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
								
								
								<td>${fieldValue(bean: instituicaoInstance, field: "nome")}</td>
								

								<td class="text-center">
									<g:link class="btn btn-xs btn-default" action="show" id="${instituicaoInstance.id}" data-toggle="tooltip" data-placement="bottom" title="Visualizar"><i class="fa fa-eye"></i></g:link>
									<g:link class="btn btn-xs btn-default" action="edit" id="${instituicaoInstance.id}" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="fa fa-pencil"></i></g:link>
									<g:form useToken="true" url="[resource:instituicaoInstance, action:'delete']" method="DELETE" style="display: inline-block;">
										<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="bottom" title="Remover" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i class="glyphicon glyphicon-remove"></i></button>
									</g:form>
								</td>
							</tr>
						</g:each>
						</tbody>
					</table>
					<div class="pagination">
						<g:paginate params="[q: "${query?:''}"]" class="pagination-sm" total="${instituicaoInstanceCount ?: 0}" />
					</div>
				</div>
			</div>
		</div><!-- /.box-body -->
	</div><!-- /.box -->

</section>
<g:javascript>
        $(document).ready(function(){
            TICKIT.selectMenu("instituicaoOpt");
        });
</g:javascript>
</body>
</html>
