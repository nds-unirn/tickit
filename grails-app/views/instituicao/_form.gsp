<%@ page import="br.edu.unirn.tickit.dominio.Instituicao" %>



<div class="form-group ${hasErrors(bean: instituicaoInstance, field: 'nome', 'error')} required">
	<label for="nome" class="col-sm-2 control-label">
		<g:message code="instituicao.nome.label" default="Nome" />:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5">
		<g:textField name="nome" required="" maxlength="255" value="${instituicaoInstance?.nome}"/>

		<g:hasErrors bean="${instituicaoInstance}" field="nome">
			<span class="help-block error"><g:renderErrors bean="${instituicaoInstance}" field="nome" as="list" /></span>
		</g:hasErrors>
	</div>
</div>

