
<%@ page import="br.edu.unirn.tickit.dominio.Equipamento" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="administrativo">
	<g:set var="entityName" value="${message(code: 'equipamento.label', default: 'Equipamento')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>

	

</head>
<body>

	


<section class="content-header">
	<ol class="breadcrumb">
		<li class="active">Listagem</li>
		<li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
	</ol>
</section>

<g:if test="${mensagemErro}">
  <div class="alert alert-error alert-dismissable">
		<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
		<div class="message" role="status">${mensagemErro}</div>
	</div>
</g:if>
<g:if test="${mensagemSucesso}">
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
		<div class="message" role="status">${mensagemSucesso}</div>
		</div>
</g:if>

<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><g:message code="default.show.label" args="[entityName]" /></h3>
		</div>

		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<table class="table table-view table-condensed">
						<thead>
							<tr>
								<th colspan="2">
									Dados do ${entityName}
								</th>
							</tr>
						</thead>
						<tbody>
							
							<g:if test="${equipamentoInstance?.nome}">
								<tr>
									<td><strong><g:message code="equipamento.nome.label" default="Nome" />:</strong></td>
									
									<td><g:fieldValue bean="${equipamentoInstance}" field="nome"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${equipamentoInstance?.tipoEquipamento}">
								<tr>
									<td><strong><g:message code="equipamento.tipoEquipamento.label" default="Tipo de Equipamento" />:</strong></td>
									
									<td><g:link controller="tipoEquipamento" action="show" id="${equipamentoInstance?.tipoEquipamento?.id}">${equipamentoInstance?.tipoEquipamento?.encodeAsHTML()}</g:link></td>
									
								</tr>
							</g:if>
							
							<g:if test="${equipamentoInstance?.dataCompra}">
								<tr>
									<td><strong><g:message code="equipamento.dataCompra.label" default="Data de Compra" />:</strong></td>
									
									<td><g:formatDate date="${equipamentoInstance?.dataCompra}" format="dd/MM/yyyy HH:mm"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${equipamentoInstance?.locado}">
								<tr>
									<td><strong><g:message code="equipamento.locado.label" default="Locado" />:</strong></td>
									
									<td><g:formatBoolean boolean="${equipamentoInstance?.locado}" /></td>
									
								</tr>
							</g:if>
							
							<g:if test="${equipamentoInstance?.ativo}">
								<tr>
									<td><strong><g:message code="equipamento.ativo.label" default="Ativo" />:</strong></td>
									
									<td><g:formatBoolean boolean="${equipamentoInstance?.ativo}" /></td>
									
								</tr>
							</g:if>
							
							<g:if test="${equipamentoInstance?.tombamento}">
								<tr>
									<td><strong><g:message code="equipamento.tombamento.label" default="Tombamento" />:</strong></td>
									
									<td><g:fieldValue bean="${equipamentoInstance}" field="tombamento"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${equipamentoInstance?.dateCreated}">
								<tr>
									<td><strong><g:message code="equipamento.dateCreated.label" default="Data de Cadastro" />:</strong></td>
									
									<td><g:formatDate date="${equipamentoInstance?.dateCreated}" format="dd/MM/yyyy HH:mm"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${equipamentoInstance?.usuarioCadastrante}">
								<tr>
									<td><strong><g:message code="equipamento.usuarioCadastrante.label" default="Usuário Cadastrante" />:</strong></td>
									
									<td><g:link controller="usuario" action="show" id="${equipamentoInstance?.usuarioCadastrante?.id}">${equipamentoInstance?.usuarioCadastrante?.encodeAsHTML()}</g:link></td>
									
								</tr>
							</g:if>

							<g:if test="${equipamentoInstance?.observacoes}">
								<tr>
									<td><strong><g:message code="equipamento.observacoes.label" default="Observações" />:</strong></td>

									<td><g:fieldValue bean="${equipamentoInstance}" field="observacoes"/></td>

								</tr>
							</g:if>
							
						</tbody>
					</table>
					<div class="box-footer clearfix">
						<g:link action="index" class="btn btn-primary pull-right"><i class="fa fa-reply"></i>Voltar</g:link>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<!-- Dados do Computador-->
<g:if test="${equipamentoInstance?.tipoEquipamento?.nome.equals("Computador")}">
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Dados do computador</h3>
		</div>

		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">

					<table class="table table-view table-condensed">
						<thead>
							<tr>
								<th>
									S. O.
								</th>
								<th>
									Processador
								</th>
								<th>
									Memória
								</th>
								<th>
									Unidade Óptica
								</th>
								<th>
									HD
								</th>
								<th>
									Fabricante
								</th>
							</tr>
						</thead>
						<tbody>							
							
							<tr>
								<td>

								${equipamentoInstance?.sistemaOperacional}

								</td>
								
								<td>${equipamentoInstance?.processador}</td>
								<td>
									${equipamentoInstance?.memoria}
								</td>
								<td>

								${equipamentoInstance?.unidadeOptica}

								</td>
								
								<td>${equipamentoInstance?.hd}</td>
								<td>
									${equipamentoInstance?.fabricante}
								</td>
								
							</tr>							
							
						</tbody>
					</table>
					
					<div class="box-footer clearfix">
						
					</div>
				</div>
			</div>



		</div>


	</div>



</section>
</g:if>


<!-- Dados da Impressora-->
<g:if test="${equipamentoInstance?.tipoEquipamento?.nome.equals("Impressora")}">
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Dados da impressora</h3>
		</div>

		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">

					<table class="table table-view table-condensed">
						<thead>
							<tr>
								<th>
									MAC
								</th>
								<th>
									IP
								</th>
								
							</tr>
						</thead>
						<tbody>							
							
							<tr>
								<td>

								${equipamentoInstance?.mac}

								</td>
								
								<td>${equipamentoInstance?.ip}</td>
								
							</tr>							
							
						</tbody>
					</table>
					
					<div class="box-footer clearfix">
						
					</div>
				</div>
			</div>



		</div>


	</div>



</section>
</g:if>

<!-- Tabela de setores vinculados-->

<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Tabela de Setores Vinculados</h3>

			<div class="box-tools pull-right">
				
				<a href="#openModal">Vincular a Setor</a>

			</div>

		</div>


		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">

					<g:if test="${!vinculos.isEmpty()}">
					<table class="table table-view table-condensed">
						<thead>
							<tr>
								<th>
									Data
								</th>
								<th>
									Setor
								</th>
								<th>
									Ativo
								</th>
							</tr>
						</thead>
						<tbody>
							
							<g:each in="${vinculos}">
								<tr>
									<td>

									<g:formatDate date="${it?.dateCreated}" type="datetime"
									 style="MEDIUM"/>

									</td>
									
									<td>${it?.setor?.nome}</td>
									<td>
										<g:if test="${it?.ativo == true}" >										
											Sim
										</g:if>
										<g:else>
											Nao
										</g:else>

									</td>
									
								</tr>
						
							</g:each>
							
						</tbody>
					</table>
					</g:if>
					<g:else>
						Este equipamento nao possui setor vinculado
					</g:else>
					<div class="box-footer clearfix">
						
					</div>
				</div>
			</div>



		</div>


	</div>



</section>

<div id="openModal" class="modalDialog">
    <div>	

    	<a href="#close" title="Close" class="close">X</a>

        	<h2>Escolha o Setor</h2>

        	<div class="content">

	        	<g:form url="[resource:equipamentoInstance, action:'show']" name="modalEquipamento">
	        		<g:hiddenField name="equipamentoId" value="${equipamentoInstance?.id}" />

	        		<g:select class="form-control" name="setor" from="${setores}" noSelection="${['null':'Selecione']}" required="true"
	        					optionKey="id" optionValue="nome" />

	        		<button type="submit" form="modalEquipamento" class="btn btn-success pull-right" style="margin-top:100px; "><i class="fa fa-check"></i>Vincular</button>


	        	</g:form>
        	</div>
    </div>
</div>
	
<g:javascript>

        $(document).ready(function(){
            TICKIT.selectMenu("equipamentoOpt");
        });

</g:javascript>




</body>


</html>
