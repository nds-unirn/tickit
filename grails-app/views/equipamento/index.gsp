
<%@ page import="br.edu.unirn.tickit.dominio.Equipamento; br.edu.unirn.tickit.dominio.TipoEquipamento" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="administrativo">
	<g:set var="entityName" value="${message(code: 'equipamento.label', default: 'Equipamento')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
	<ol class="breadcrumb">
		<li class="active">Listagem</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Lista de equipamentos (${equipamentoInstanceCount})</h3>
			<div class="box-tools pull-right">
				<g:link class="btn btn-default btn-box-tool" action="create"><i class="fa fa-plus"></i> Novo equipamento</g:link>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<g:form action="index">
						<a href="#pesquisa" data-toggle="collapse" class="pull-right help-block collapse-btn">Pesquisar</a>
						<div class="col-xs-12 margin-bottom-10 collapse" id="pesquisa">
							<div class="row">
								<div class="col-sm-5">
									<div class="form-group">
										<label class="control-label" for="nome">Nome</label>
										<g:textField name="nome" class="form-control input-sm" value="${params?.nome}"/>
									</div>
								</div>

								<div class="col-sm-5">
									<div class="form-group">
										<label class="control-label" for="tipoEquipamento">
											Tipo de Equipamento
										</label>
										<g:select id="tipoEquipamento" name="tipoEquipamento.id" from="${TipoEquipamento.findAllByAtivo(true, [sort:'nome', order:"asc"])}" optionKey="id" value="${params?.tipoEquipamento?.id}" class="form-control" noSelection="['':'- Selecione -']"/>
									</div>
								</div>

								<div class="col-sm-2 padding-top-20">
									<button type="submit" class="btn btn-block btn-primary"><i class="fa fa-search"></i>Pesquisar</button>
								</div>
							</div>
						</div>
					</g:form>
				</div>
				<div class="col-xs-12">
					<g:if test="${flash.message}">
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
							<div class="message" role="status">${flash.message}</div>
						</div>
					</g:if>
					<table class="table table-bordered table-condensed">
						<thead>
						<tr>
							
							<g:sortableColumn params="[q: "${query?:''}"]" property="nome" title="${message(code: 'equipamento.nome.label', default: 'Nome')}" />
							
							<th><g:message code="equipamento.tipoEquipamento.label" default="Tipo Equipamento" /></th>
							
							<g:sortableColumn params="[q: "${query?:''}"]" property="dataCompra" title="${message(code: 'equipamento.dataCompra.label', default: 'Data de Compra')}" />
							
							<g:sortableColumn params="[q: "${query?:''}"]" property="locado" title="${message(code: 'equipamento.locado.label', default: 'Locado')}" />

							<g:sortableColumn params="[q: "${query?:''}"]" property="tombamento" title="${message(code: 'equipamento.tombamento.label', default: 'Tombamento')}" />
							


							<th width="9%" colspan="3">&nbsp;</th>
						</tr>
						</thead>
						<tbody>
						<g:each in="${equipamentoInstanceList}" status="i" var="equipamentoInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
								
								
								<td>${fieldValue(bean: equipamentoInstance, field: "nome")}</td>
								
								
								<td>${fieldValue(bean: equipamentoInstance, field: "tipoEquipamento")}</td>
								
								
								<td><g:formatDate date="${equipamentoInstance.dataCompra}" format="dd/MM/yyyy HH:mm"/></td>
								
								
								<td><g:formatBoolean boolean="${equipamentoInstance.locado}" /></td>
								
								
								<td>${fieldValue(bean: equipamentoInstance, field: "tombamento")}</td>
								

								<td class="text-center">
									<g:link class="btn btn-xs btn-default" action="show" id="${equipamentoInstance.id}" data-toggle="tooltip" data-placement="bottom" title="Visualizar"><i class="fa fa-eye"></i></g:link>
									<g:link class="btn btn-xs btn-default" action="edit" id="${equipamentoInstance.id}" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="fa fa-pencil"></i></g:link>
									<g:form useToken="true" url="[resource:equipamentoInstance, action:'delete']" method="DELETE" style="display: inline-block;">
										<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="bottom" title="Remover" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i class="glyphicon glyphicon-remove"></i></button>
									</g:form>
								</td>
							</tr>
						</g:each>
						</tbody>
					</table>
					<div class="pagination">
						<g:paginate params="[q: "${query?:''}"]" class="pagination-sm" total="${equipamentoInstanceCount ?: 0}" />
					</div>
				</div>
			</div>
		</div><!-- /.box-body -->
	</div><!-- /.box -->

</section>
<g:javascript>
        $(document).ready(function(){
            TICKIT.selectMenu("equipamentoOpt");
        });
</g:javascript>
</body>
</html>
