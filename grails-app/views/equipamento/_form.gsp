<%@ page import="br.edu.unirn.tickit.dominio.Equipamento; br.edu.unirn.tickit.dominio.TipoEquipamento"; import="br.edu.unirn.tickit.dominio.Caracteristica; %>

<script>
  $(function() {
  	$('#datepicker').datetimepicker();
   
  });
  </script>

<div class="content">
	<h3>Dados do equipamento</h3>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">

				<div class="form-group ${hasErrors(bean: equipamentoInstance, field: 'nome', 'error')} required">
					<div class="row">
						<label for="nome" class="control-label">
							<g:message code="equipamento.nome.label" default="Nome" />:
							<span class="required-indicator">*</span>
						</label>
					</div>
					<div class="row" style="width: 80%;">
						<g:textField name="nome" class="form-control" required="" maxlength="255" value="${equipamentoInstance?.nome}"/>

						<g:hasErrors bean="${equipamentoInstance}" field="nome">
							<span class="help-block error"><g:renderErrors bean="${equipamentoInstance}" field="nome" as="list" /></span>
						</g:hasErrors>
					</div>
				</div>

			</div>
			<div class="col-md-6">

				<div class="form-group ${hasErrors(bean: equipamentoInstance, field: 'tipoEquipamento', 'error')} required">
					<div class="row">
						<label for="tipoEquipamento" class="control-label">
							<g:message code="equipamento.tipoEquipamento.label" default="Tipo de Equipamento" />:
							<span class="required-indicator">*</span>
						</label>
					</div>
					<div class="row" style="width: 80%;">
						<g:select id="tipoEquipamento" name="tipoEquipamento.id" from="${TipoEquipamento.findAllByAtivo(true, [sort:'nome', order:"asc"])}" optionKey="id" required="" value="${equipamentoInstance?.tipoEquipamento?.id}" class="form-control"/>

						<g:hasErrors bean="${equipamentoInstance}" field="tipoEquipamento">
							<span class="help-block error"><g:renderErrors bean="${equipamentoInstance}" field="tipoEquipamento" as="list" /></span>
						</g:hasErrors>
					</div>
				</div>
			</div>
		</div>
	</div>

<div class="row">
	<div class="col-md-12">
	<div class="col-md-6">
		<div class="form-group ${hasErrors(bean: equipamentoInstance, field: 'dataCompra', 'error')} required">
			<div class="row">
				<label for="dataCompra" class="control-label">
					<g:message code="equipamento.dataCompra.label" default="Data de Compra/Locação" />:
					<span class="required-indicator">*</span>
				</label>
			</div>
			<div class="row" style="width: 46%;">
				<g:textField name="dataCompra" id="datepicker" class="form-control" value="${equipamentoInstance?.dataCompra}"  />

				<g:hasErrors bean="${equipamentoInstance}" field="dataCompra">
					<span class="help-block error"><g:renderErrors bean="${equipamentoInstance}" field="dataCompra" as="list" /></span>
				</g:hasErrors>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group ${hasErrors(bean: equipamentoInstance, field: 'locado', 'error')} ">
			<div class="row">
			<label for="locado" class="control-label">
				<g:message code="equipamento.locado.label" default="Locado" />:
				
			</label>
			</div>
			<div class="row">
				<g:checkBox name="locado" value="${equipamentoInstance?.locado}" />

				<g:hasErrors bean="${equipamentoInstance}" field="locado">
					<span class="help-block error"><g:renderErrors bean="${equipamentoInstance}" field="locado" as="list" /></span>
				</g:hasErrors>
			</div>
		</div>
	</div>
	</div>
</div>


<div class="row">
<div class="col-md-12">
	<div class="col-md-6">

		<div class="form-group ${hasErrors(bean: equipamentoInstance, field: 'tombamento', 'error')} ">
			<div class="row">
			<label for="tombamento" class="control-label">
				<g:message code="equipamento.tombamento.label" default="Tombamento" />:
				
			</label>
			</div>
			<div class="row" style="width: 46%;" >
				<g:textField name="tombamento" maxlength="255" class="form-control" value="${equipamentoInstance?.tombamento}"/>

				<g:hasErrors bean="${equipamentoInstance}" field="tombamento">
					<span class="help-block error"><g:renderErrors bean="${equipamentoInstance}" field="tombamento" as="list" /></span>
				</g:hasErrors>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group ${hasErrors(bean: equipamentoInstance, field: 'observacoes', 'error')} ">
			<div class="row">
				<label for="observacoes" class="control-label">
					<g:message code="equipamento.observacoes.label" default="Observações" />:

				</label>
			</div>
			<div class="row" style="width: 80%;">
				<g:textArea name="observacoes" maxlength="5000" class="form-control" value="${equipamentoInstance?.observacoes}" cols="40" rows="3"/>

				<g:hasErrors bean="${equipamentoInstance}" field="observacoes">
					<span class="help-block error"><g:renderErrors bean="${equipamentoInstance}" field="observacoes" as="list" /></span>
				</g:hasErrors>
			</div>
		</div>
	</div>
</div>
</div>


<div class="row">
<div class="col-md-12">
	<div class="col-md-6">

		<div class="form-group">
			<div class="row">
			<label for="caracteristica" class="control-label">
				Características:
				
			</label>
			</div>
			<div class="row form-group" style="width: 46%;" >
			
			<g:select  id="caracteristica" name="caracteristica.id" from="${caracteristicas}"
			optionValue="nome" optionKey="id" required="" value="${it?.id}" class="form-control"/>
			<g:textField name="descricaoCaracteristica" maxlength="255" class="form-control" value=""/>
			<button class="btn btn-success" style="margin-top: 1px; margin-left:140px; " name="add" type="submit">Adicionar <i class="fa fa-plus" aria-hidden="true"></i></button>	
			    
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="row">
			<label for="caracteristicas" class="control-label">
				Tabela de características:
				
			</label>
			</div>
	</div>
</div>
</div>

</div><!-- FIM CONTENT-->
