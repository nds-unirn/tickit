
<%@ page import="br.edu.unirn.tickit.dominio.TipoOcorrencia" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="administrativo">
	<g:set var="entityName" value="${message(code: 'tipoOcorrencia.label', default: 'Tipo de Ocorrência')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<section class="content-header">
	<ol class="breadcrumb">
		<li class="active">Listagem</li>
		<li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><g:message code="default.show.label" args="[entityName]" /></h3>
		</div>

		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<table class="table table-view table-condensed">
						<thead>
							<tr>
								<th colspan="2">
									Dados do ${entityName}
								</th>
							</tr>
						</thead>
						<tbody>
							
							<g:if test="${tipoOcorrenciaInstance?.nome}">
								<tr>
									<td><strong><g:message code="tipoOcorrencia.nome.label" default="Nome" />:</strong></td>
									
									<td><g:fieldValue bean="${tipoOcorrenciaInstance}" field="nome"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${tipoOcorrenciaInstance?.ativo}">
								<tr>
									<td><strong><g:message code="tipoOcorrencia.ativo.label" default="Ativo" />:</strong></td>
									
									<td><g:formatBoolean boolean="${tipoOcorrenciaInstance?.ativo}" /></td>
									
								</tr>
							</g:if>
							
							<g:if test="${tipoOcorrenciaInstance?.dateCreated}">
								<tr>
									<td><strong><g:message code="tipoOcorrencia.dateCreated.label" default="Data de Cadastro" />:</strong></td>
									
									<td><g:formatDate date="${tipoOcorrenciaInstance?.dateCreated}" format="dd/MM/yyyy HH:mm"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${tipoOcorrenciaInstance?.usuarioCadastrante}">
								<tr>
									<td><strong><g:message code="tipoOcorrencia.usuarioCadastrante.label" default="Usuário Cadastrante" />:</strong></td>
									
									<td><g:link controller="usuario" action="show" id="${tipoOcorrenciaInstance?.usuarioCadastrante?.id}">${tipoOcorrenciaInstance?.usuarioCadastrante?.encodeAsHTML()}</g:link></td>
									
								</tr>
							</g:if>
							
						</tbody>
					</table>
					<div class="box-footer clearfix">
						<g:link action="index" class="btn btn-primary pull-right"><i class="fa fa-reply"></i>Voltar</g:link>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<g:javascript>
        $(document).ready(function(){
            TICKIT.selectMenu("tipoOcorrenciaOpt");
        });
</g:javascript>
</body>
</html>
