<%@ page import="br.edu.unirn.tickit.dominio.TipoOcorrencia" %>



<div class="form-group ${hasErrors(bean: tipoOcorrenciaInstance, field: 'nome', 'error')} required">
	<label for="nome" class="col-sm-2 control-label">
		<g:message code="tipoOcorrencia.nome.label" default="Nome" />:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5">
		<g:textField name="nome" required="" maxlength="255" value="${tipoOcorrenciaInstance?.nome}"/>

		<g:hasErrors bean="${tipoOcorrenciaInstance}" field="nome">
			<span class="help-block error"><g:renderErrors bean="${tipoOcorrenciaInstance}" field="nome" as="list" /></span>
		</g:hasErrors>
	</div>
</div>
