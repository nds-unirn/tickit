
<%@ page import="br.edu.unirn.tickit.dominio.Caracteristica" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="administrativo">
	<g:set var="entityName" value="${message(code: 'caracteristica.label', default: 'Característica')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<section class="content-header">
	<ol class="breadcrumb">
		<li class="active">Listagem</li>
		<li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
		<g:if test="${flash.message}">
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
				<div class="message" role="status">${flash.message}</div>
			</div>
			<h3 class="box-title"><g:message code="default.show.label" args="[entityName]" /></h3>
		</g:if>
		</div>

		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<table class="table table-view table-condensed">
						<thead>
							<tr>
								<th colspan="2">
									Dados da ${entityName}
								</th>
							</tr>
						</thead>
						<tbody>
							
							<g:if test="${caracteristicaInstance?.nome}">
								<tr>
									<td><strong><g:message code="caracteristica.nome.label" default="Nome" />:</strong></td>
									
									<td><g:fieldValue bean="${caracteristicaInstance}" field="nome"/></td>
									
								</tr>
							</g:if>

							<g:if test="${caracteristicaInstance?.descricao}">
								<tr>
									<td><strong><g:message code="caracteristica.descricao.label" default="Descrição" />:</strong></td>
									
									<td><g:fieldValue bean="${caracteristicaInstance}" field="descricao"/></td>
									
								</tr>
							</g:if>
							
						
							<g:if test="${caracteristicaInstance?.dateCreated}">
								<tr>
									<td><strong><g:message code="caracteristica.dateCreated.label" default="Data de Cadastro" />:</strong></td>
									
									<td><g:formatDate date="${caracteristicaInstance?.dateCreated}" format="dd/MM/yyyy HH:mm"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${caracteristicaInstance?.usuarioCadastrante}">
								<tr>
									<td><strong><g:message code="caracteristica.usuarioCadastrante.label" default="Usuário Cadastrante" />:</strong></td>
									
									<td><g:link controller="usuario" action="show" id="${caracteristicaInstance?.usuarioCadastrante?.id}">${caracteristicaInstance?.usuarioCadastrante?.encodeAsHTML()}</g:link></td>
									
								</tr>
							</g:if>
							
						</tbody>
					</table>
					<div class="box-footer clearfix">
						<g:link action="index" class="btn btn-primary pull-right"><i class="fa fa-reply"></i>Voltar</g:link>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<g:javascript>
        $(document).ready(function(){
            TICKIT.selectMenu("caracteristicaOpt");
        });
</g:javascript>
</body>
</html>
