<%@ page import="br.edu.unirn.tickit.dominio.Caracteristica" %>



<div class="form-group ${hasErrors(bean: caracteristicaInstance, field: 'nome', 'error')} required">
	<label for="nome" class="col-sm-2 control-label">
		<g:message code="caracteristica.nome.label" default="Nome" />:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5">
		<g:textField class="form-control" name="nome" required="" maxlength="255" value="${caracteristicaInstance?.nome}"/>

		<g:hasErrors bean="${caracteristicaInstance}" field="nome">
			<span class="help-block error"><g:renderErrors bean="${caracteristicaInstance}" field="nome" as="list" /></span>
		</g:hasErrors>
	</div>
</div>

<div class="form-group ${hasErrors(bean: caracteristicaInstance, field: 'descricao', 'error')} required">
	<label for="descricao" class="col-sm-2 control-label">
		<g:message code="caracteristica.descricao.label" default="Descrição" />:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5">
		<g:textArea class="form-control" name="descricao" required="" maxlength="5000" value="${caracteristicaInstance?.descricao}" cols="10" rows="3"/>

		<g:hasErrors bean="${caracteristicaInstance}" field="descricao">
			<span class="help-block error"><g:renderErrors bean="${caracteristicaInstance}" field="descricao" as="list" /></span>
		</g:hasErrors>
	</div>
</div>