<%@ page import="br.edu.unirn.tickit.dominio.Ocorrencia; br.edu.unirn.tickit.dominio.TipoOcorrencia" %>


  <script>
  $(function() {
  	$('#datepicker').datetimepicker();
   
  });
  </script>

<div class="form-group ${hasErrors(bean: ocorrenciaInstance, field: 'descricao', 'error')} required">
	<label for="descricao" class="col-sm-2 control-label">
		<g:message code="ocorrencia.descricao.label" default="Descrição" />:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5">
		<g:textArea name="descricao" required="" rows="5" cols="80" maxlength="255" value="${ocorrenciaInstance?.descricao}"/>

		<g:hasErrors bean="${ocorrenciaInstance}" field="descricao">
			<span class="help-block error"><g:renderErrors bean="${ocorrenciaInstance}" field="descricao" as="list" /></span>
		</g:hasErrors>
	</div>
</div>

<div class="form-group ${hasErrors(bean: ocorrenciaInstance, field: 'dataOcorrencia', 'error')} required">
	<label for="dataOcorrencia" class="col-sm-2 control-label">
		<g:message code="ocorrencia.dataOcorrencia.label" default="Data da Ocorrência" />:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5">


		
  		<g:textField name="dataOcorrencia" required="" 
  		value="${dataOcorrencia}" id="datepicker" /><br/>
  		<span >* Deve ser menor que a data e hora atuais.</span>
		<g:hasErrors bean="${ocorrenciaInstance}" field="dataOcorrencia">
			<span class="help-block error"><g:renderErrors bean="${ocorrenciaInstance}" field="dataOcorrencia" as="list" /></span>
		</g:hasErrors>

	</div>
</div>

<div class="form-group ${hasErrors(bean: ocorrenciaInstance, field: 'tipoOcorrencia', 'error')} required">
	<label for="tipoOcorrencia" class="col-sm-2 control-label">
		<g:message code="ocorrencia.tipoOcorrencia.label" default="Tipo de Ocorrência" />:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5">
		<g:select id="tipoOcorrencia" name="tipoOcorrencia.id" from="${TipoOcorrencia.findAllByAtivo(true, [sort:'nome', order:"asc"])}" optionKey="id" required="" value="${ocorrenciaInstance?.tipoOcorrencia?.id}" class="many-to-one"/>

		<g:hasErrors bean="${ocorrenciaInstance}" field="tipoOcorrencia">
			<span class="help-block error"><g:renderErrors bean="${ocorrenciaInstance}" field="tipoOcorrencia" as="list" /></span>
		</g:hasErrors>
	</div>
</div>

