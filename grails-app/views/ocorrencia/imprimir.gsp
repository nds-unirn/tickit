<html>
<head>


<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<style type="text/css">
body {
    margin: 0;
    padding: 0;
    background-color: #FAFAFA;
    font: 10pt "Arial";
    text-align:justify;
    line-height: 1;
}
* {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
}
.page {
    width: 21cm;
    min-height: 29.7cm;
    padding: 1.5cm;
    margin: 1cm auto;
    border: 1px #D3D3D3 solid;
    border-radius: 5px;
    background: white;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}
@page {
    size: A4;
    margin: 0;
}
@media print {
    .page {
        margin: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: always;
    }
}
</style>

</head>

<body>
	<div class="book">
		<g:if test="${ocorrencias != null}">

			<g:each in="${ocorrencias}">
			<g:if test="${contadorPagina == 1}">
	        <div class="page">
		    	<div style="font-weight:bold; text-align:center; font:normal 20px 'Arial';">
		    		<strong>TickIT - NDS-UNIRN</strong> 
			    	 <br/>
			    	<strong>Lista de ocorrências</strong> <br/>		    	
			    	
			    	<br/>
			    </div>
			        <br><br>
		    	<div class="widget-content">
						<table class="table table-striped table-bordered table-highlight">
							<thead>
								<tr>
									<th width="25%">Data da ocorrência</th>
									<th width="15%">Tipo </th>
									<th width="40%">Descrição</th>
									<th width="20%">Usuário Cadastrante</th>
								</tr>
							</thead>
							<tbody>
			</g:if>
									<tr>
								
										<td>
											<g:formatDate format="dd/MM/yyyy" date="${it?.dataOcorrencia}"/>
											- <g:formatDate format="HH:mm" date="${it?.dataOcorrencia}"/>
										</td>
										<td>
											${it?.tipoOcorrencia?.nome }
										</td>
										<td>
											${it?.descricao}
										</td>
										<td>
											${it?.usuarioCadastrante?.nome}
											
										</td>
									</tr>

									<% contador++ %>
									<% contadorPagina++ %>
									<g:if test="${contadorPagina == 10}">
										<% contadorPagina = 1 %>
			</g:if>

			<g:if test="${contadorPagina == 1}">
									</tbody>
								</table>
							</div>
					</div>
				<div style="page-break-before:always;"> </div>
				</g:if>
				</g:each>
			</g:if>
			<g:else>
				<br>
				Não há nenhuma ocorrência que atenda aos filtros selecionados.
			</g:else>
	</div>
</body>

</html>