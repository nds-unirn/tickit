
<%@ page import="br.edu.unirn.tickit.dominio.Ocorrencia; br.edu.unirn.tickit.dominio.TipoOcorrencia" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="administrativo">
	<g:set var="entityName" value="${message(code: 'ocorrencia.label', default: 'Ocorrência')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>


  <script>
  $(function() {
  	$('#datepicker').datetimepicker();
   
  });
  </script>
   <script>
  $(function() {
  	$('#datepicker2').datetimepicker();
   
  });
  </script>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
	<ol class="breadcrumb">
		<li class="active">Listagem</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Lista de ocorrências (${ocorrenciaInstanceCount})</h3>
			<div class="box-tools pull-right">
				<g:link class="btn btn-default btn-box-tool" action="create"><i class="fa fa-plus"></i> Nova ocorrência</g:link>
			</div>
		</div>
		<div class="box-body">
		<g:if test="${flash.message}">
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					${flash.message}
				</div>
			</g:if>
			<div class="row">
				<div class="col-xs-12">
					<g:form action="index">
						<a href="#pesquisa" data-toggle="collapse" class="pull-right help-block collapse-btn">Pesquisar</a>
						<div class="col-xs-12 margin-bottom-10 collapse" id="pesquisa">
							<div class="row">
								<div class="col-sm-2">
									<div class="form-group">
										<label class="control-label" for="dataInicioIntervalo">Data início</label>
										<g:textField name="dataInicioIntervalo" class="form-control"
  											value="" id="datepicker" /><br/>
									</div>
								</div>

								<div class="col-sm-2">
									<div class="form-group">
										<label class="control-label" for="dataFinalIntervalo">Data final</label>
										<g:textField name="dataFinalIntervalo" class="form-control"
  											value="" id="datepicker2" /><br/>
									</div>
								</div>

								<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label" for="tipoOcorrencia">
											Tipo de Ocorrência
										</label>
										<g:select id="tipoOcorrencia" name="tipoOcorrencia" from="${TipoOcorrencia.findAllByAtivo(true, [sort:'nome', order:"asc"])}" optionKey="id" value="${tipoOcorrencia?.id}" class="form-control" noSelection="['':'- Selecione -']"/>
									</div>
								</div>

								<div class="col-sm-2">
									<div class="form-group">
									<button type="submit" class="btn btn-block btn-primary" style="margin-top:25px; "><i class="fa fa-search"></i>Pesquisar</button>
									</div>
								</div>
							</div>
						</div>
					</g:form>
				</div>
				<div class="col-xs-12">
					
					<table class="table table-bordered table-condensed">
						<thead>
						<tr>
							<th><g:message code="ocorrencia.dataOcorrencia.label" default="Data da Ocorrência" /></th>
							
							
							<th><g:message code="ocorrencia.tipoOcorrencia.label" default="Tipo Ocorrência" /></th>
							
							<th><g:message code="ocorrencia.descricao.label" default="Descriçao" /></th>

						
							<th width="9%" colspan="3">&nbsp;</th>
						</tr>
						</thead>
						<tbody>
						<g:each in="${ocorrencias}" status="i" var="ocorrenciaInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
								
								
								<td>
								<g:formatDate date="${ocorrenciaInstance?.dataOcorrencia}" format="dd/MM/yyyy HH:mm"/>
								</td>
								
								
								<td>${fieldValue(bean: ocorrenciaInstance, field: "tipoOcorrencia")}</td>
								
								
								<td>${fieldValue(bean: ocorrenciaInstance, field: "descricao")}</td>

								<td class="text-center">
									<g:link class="btn btn-xs btn-default" action="show" id="${ocorrenciaInstance.id}" data-toggle="tooltip" data-placement="bottom" title="Visualizar"><i class="fa fa-eye"></i></g:link>
									<g:link class="btn btn-xs btn-default" action="edit" id="${ocorrenciaInstance.id}" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="fa fa-pencil"></i></g:link>
									<g:form useToken="true" url="[resource:ocorrenciaInstance, action:'delete']" method="DELETE" style="display: inline-block;">
										<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="bottom" title="Remover" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i class="glyphicon glyphicon-remove"></i></button>
									</g:form>
								</td>
							</tr>
						</g:each>
						</tbody>
					</table>
					<div class="pagination">
						<g:paginate params="[q: "${query?:''}"]" class="pagination-sm" total="${ocorrenciaInstanceCount ?: 0}" />
					</div>
				</div>

				<div>
					<div class="box-tools pull-right">

						<g:if test="${ocorrenciaInstanceCount > 0}">

							<g:link class="btn btn-default btn-box-tool" name="imprimir" action="index" target="_blank" params="[tipoOcorrencia:params.tipoOcorrencia, imprimir:true, dataInicioIntervalo:params.dataInicioIntervalo, dataFinalIntervalo:params.dataFinalIntervalo]"><i class="fa fa-print"></i> Imprimir lista</g:link>

						</g:if>
					</div>
				</div>

			</div>

		</div><!-- /.box-body -->
	</div><!-- /.box -->

</section>
<g:javascript>
        $(document).ready(function(){
            TICKIT.selectMenu("ocorrenciaOpt");
        });
</g:javascript>
</body>
</html>
