
<%@ page import="br.edu.unirn.tickit.dominio.Ocorrencia" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="administrativo">
	<g:set var="entityName" value="${message(code: 'ocorrencia.label', default: 'Ocorrência')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>

	

</head>
<body>

	


<section class="content-header">
	<ol class="breadcrumb">
		<li class="active">Listagem</li>
		<li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
	</ol>
</section>

<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><g:message code="default.show.label" args="[entityName]" /></h3>
		</div>

		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<table class="table table-view table-condensed">
						<thead>
							<tr>
								<th colspan="2">
									Dados da ${entityName}
								</th>
							</tr>
						</thead>
						<tbody>
							
							<g:if test="${ocorrenciaInstance?.descricao}">
								<tr>
									<td><strong><g:message code="ocorrencia.descricao.label" default="Descrição" />:</strong></td>
									
									<td><g:fieldValue bean="${ocorrenciaInstance}" field="descricao"/></td>
									
								</tr>
							</g:if>

							<g:if test="${ocorrenciaInstance?.dataOcorrencia}">
								<tr>
									<td><strong><g:message code="ocorrencia.dataOcorrencia.label" default="Data da ocorrência" />:</strong></td>
									
									<td>
									<g:formatDate date="${ocorrenciaInstance?.dataOcorrencia}" format="dd/MM/yyyy HH:mm"/>
									</td>
									
								</tr>
							</g:if>
							
							<g:if test="${ocorrenciaInstance?.tipoOcorrencia}">
								<tr>
									<td><strong><g:message code="ocorrencia.tipoOcorrencia.label" default="Tipo de Ocorrência" />:</strong></td>
									
									<td><g:link controller="tipoOcorrencia" action="show" id="${ocorrenciaInstance?.tipoOcorrencia?.id}">${ocorrenciaInstance?.tipoOcorrencia?.encodeAsHTML()}</g:link></td>
									
								</tr>
							</g:if>
							
							
							
						
							<g:if test="${ocorrenciaInstance?.ativo}">
								<tr>
									<td><strong><g:message code="ocorrencia.ativo.label" default="Ativo" />:</strong></td>
									
									<td><g:formatBoolean boolean="${ocorrenciaInstance?.ativo}" /></td>
									
								</tr>
							</g:if>
							
						
							
							<g:if test="${ocorrenciaInstance?.dateCreated}">
								<tr>
									<td><strong><g:message code="ocorrencia.dateCreated.label" default="Data de Cadastro" />:</strong></td>
									
									<td><g:formatDate date="${ocorrenciaInstance?.dateCreated}" format="dd/MM/yyyy HH:mm"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${ocorrenciaInstance?.usuarioCadastrante}">
								<tr>
									<td><strong><g:message code="ocorrencia.usuarioCadastrante.label" default="Usuário Cadastrante" />:</strong></td>
									
									<td><g:link controller="usuario" action="show" id="${ocorrenciaInstance?.usuarioCadastrante?.id}">${ocorrenciaInstance?.usuarioCadastrante?.encodeAsHTML()}</g:link></td>
									
								</tr>
							</g:if>

			
						</tbody>
					</table>
					<div class="box-footer clearfix">
						<g:link action="index" class="btn btn-primary pull-right"><i class="fa fa-reply"></i>Voltar</g:link>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

	
<g:javascript>

        $(document).ready(function(){
            TICKIT.selectMenu("ocorrenciaOpt");
        });

</g:javascript>




</body>


</html>
