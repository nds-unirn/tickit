<%@ page import="br.edu.unirn.tickit.dominio.TipoEquipamento" %>



<div class="form-group ${hasErrors(bean: tipoEquipamentoInstance, field: 'nome', 'error')} required">
	<label for="nome" class="col-sm-2 control-label">
		<g:message code="tipoEquipamento.nome.label" default="Nome" />:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5">
		<g:textField name="nome" required="" maxlength="255" value="${tipoEquipamentoInstance?.nome}"/>

		<g:hasErrors bean="${tipoEquipamentoInstance}" field="nome">
			<span class="help-block error"><g:renderErrors bean="${tipoEquipamentoInstance}" field="nome" as="list" /></span>
		</g:hasErrors>
	</div>
</div>
