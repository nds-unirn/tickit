<%@ page import="br.edu.unirn.tickit.acesso.Usuario" %>



<div class="form-group ${hasErrors(bean: usuarioInstance, field: 'nome', 'error')} required">
	<label for="nome" class="col-sm-2 control-label">
		<g:message code="usuario.nome.label" default="Nome" />:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5">
		<g:textField name="nome" required="" value="${usuarioInstance?.nome}"/>

		<g:hasErrors bean="${usuarioInstance}" field="nome">
			<span class="help-block error"><g:renderErrors bean="${usuarioInstance}" field="nome" as="list" /></span>
		</g:hasErrors>
	</div>
</div>

<div class="form-group ${hasErrors(bean: usuarioInstance, field: 'email', 'error')} required">
	<label for="email" class="col-sm-2 control-label">
		<g:message code="usuario.email.label" default="Email" />:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5">
		<g:textField name="email" required="" value="${usuarioInstance?.email}"/>

		<g:hasErrors bean="${usuarioInstance}" field="email">
			<span class="help-block error"><g:renderErrors bean="${usuarioInstance}" field="email" as="list" /></span>
		</g:hasErrors>
	</div>
</div>

<div class="form-group ${hasErrors(bean: usuarioInstance, field: 'login', 'error')} required">
	<label for="login" class="col-sm-2 control-label">
		<g:message code="usuario.login.label" default="Login" />:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5">
		<g:textField name="login" required="" value="${usuarioInstance?.login}"/>

		<g:hasErrors bean="${usuarioInstance}" field="login">
			<span class="help-block error"><g:renderErrors bean="${usuarioInstance}" field="login" as="list" /></span>
		</g:hasErrors>
	</div>
</div>

<g:if test="${usuarioInstance?.id}">
<g:hiddenField name="id" value="${usuarioInstance?.id}"/>
</g:if>
<g:else>
	<div class="form-group ${hasErrors(bean: usuarioInstance, field: 'senha', 'error')} required">
		<label for="senha" class="col-sm-2 control-label">
			<g:message code="usuario.senha.label" default="Senha" />:
			<span class="required-indicator">*</span>
		</label>
		<div class="col-sm-5">
			<g:passwordField type="password" name="senha" required="" value=""/>

			<g:hasErrors bean="${usuarioInstance}" field="senha">
				<span class="help-block error"><g:renderErrors bean="${usuarioInstance}" field="senha" as="list" /></span>
			</g:hasErrors>
		</div>
	</div>
</g:else>

<div class="form-group ${hasErrors(bean: usuarioInstance, field: '', 'error')} required">
	<label for="perfil" class="col-sm-2 control-label">
		Perfil:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-2">
		<g:select class="form-control" name="perfil" from="${perfis}" optionKey="id" optionValue="nome" value=""/>
	</div>
</div>