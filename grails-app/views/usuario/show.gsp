
<%@ page import="br.edu.unirn.tickit.acesso.Usuario" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="administrativo">
	<g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuário')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<section class="content-header">
	<ol class="breadcrumb">
		<li class="active">Listagem</li>
		<li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><g:message code="default.show.label" args="[entityName]" /></h3>
		</div>

		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<table class="table table-view table-condensed">
						<thead>
							<tr>
								<th colspan="2">
									Dados do ${entityName}
								</th>
							</tr>
						</thead>
						<tbody>
							
							<g:if test="${usuarioInstance?.login}">
								<tr>
									<td><strong><g:message code="usuario.login.label" default="Login" />:</strong></td>
									
									<td><g:fieldValue bean="${usuarioInstance}" field="login"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${usuarioInstance?.nome}">
								<tr>
									<td><strong><g:message code="usuario.nome.label" default="Nome" />:</strong></td>
									
									<td><g:fieldValue bean="${usuarioInstance}" field="nome"/></td>
									
								</tr>
							</g:if>

							<g:if test="${usuarioInstance?.email}">
								<tr>
									<td><strong><g:message code="usuario.email.label" default="Email" />:</strong></td>
									
									<td><g:fieldValue bean="${usuarioInstance}" field="email"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${usuarioInstance?.ativo}">
								<tr>
									<td><strong><g:message code="usuario.ativo.label" default="Ativo" />:</strong></td>
									
									<td><g:formatBoolean boolean="${usuarioInstance?.ativo}" /></td>
									
								</tr>
							</g:if>
							
							<g:if test="${usuarioInstance?.dateCreated}">
								<tr>
									<td><strong><g:message code="usuario.dateCreated.label" default="Data de Cadastro" />:</strong></td>
									
									<td><g:formatDate date="${usuarioInstance?.dateCreated}" format="dd/MM/yyyy HH:mm"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${usuarioInstance?.usuarioCadastrante}">
								<tr>
									<td><strong><g:message code="usuario.usuarioCadastrante.label" default="Usuário Cadastrante" />:</strong></td>
									
									<td><g:link controller="usuario" action="show" id="${usuarioInstance?.usuarioCadastrante?.id}">${usuarioInstance?.usuarioCadastrante?.encodeAsHTML()}</g:link></td>
									
								</tr>
							</g:if>

							<g:if test="${usuarioInstance?.getPerfil()?.id}">
								<tr>
									<td><strong><g:message code="usuario.usuarioCadastrante.label" default="Perfil" />:</strong></td>

									<td>${usuarioInstance?.getPerfil()}</td>

								</tr>
							</g:if>
							
						</tbody>
					</table>
					<div class="box-footer clearfix">
						<g:link action="index" class="btn btn-primary pull-right"><i class="fa fa-reply"></i>Voltar</g:link>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<g:javascript>
        $(document).ready(function(){
            TICKIT.selectMenu("usuarioOpt");
        });
</g:javascript>
</body>
</html>
