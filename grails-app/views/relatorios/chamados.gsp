
<%@ page import="br.edu.unirn.tickit.dominio.AreaAtendimento; br.edu.unirn.tickit.dominio.Setor; br.edu.unirn.tickit.acesso.Usuario" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="administrativo">
    <g:set var="entityName" value="${message(code: 'chamado.label', default: 'Chamado')}" />
    <title><g:message code="default.report.label" args="[entityName]" default="Relatório de Chamados" /></title>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
    <ol class="breadcrumb">
        <li class="active">Relatório</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><g:message code="default.report.label" args="[entityName]" /></h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <g:form action="chamados">

                        <div class="form-group col-sm-4">
                            <label for="ativo" class="control-label">
                                <g:message code="equipamento.ativo.label" default="Ativo" />:
                            </label>
                            <g:select noSelection="['':'--- Selecione ---']" from="${[true : "Sim", false : "Não"]}" optionKey="key" optionValue="value" name="ativo" class="form-control" value="${params?."ativo"}"/>
                        </div>

                        <div class="form-group col-sm-4">
                            <label for="titulo" class="control-label">
                                <g:message code="chamado.titulo.label" default="Título" />:
                            </label>
                            <g:textField name="titulo" class="form-control" maxlength="255" value="${params?.titulo}"/>
                        </div>

                        <div class="form-group col-sm-4">
                            <label for="emailSolicitante" class="control-label">
                                <g:message code="chamado.emailSolicitante.label" default="E-mail Solicitante" />:
                            </label>
                            <g:textField name="emailSolicitante" class="form-control" maxlength="255" value="${params?.emailSolicitante}"/>
                        </div>

                        <div class="form-group col-sm-4">
                            <label for="usuarioCadastrante" class="control-label">
                                <g:message code="equipamento.usuarioCadastrante.label" default="Usuário Cadastrante" />:
                            </label>
                            <g:select noSelection="['':'--- Selecione ---']" from="${Usuario.findAllByAtivo(true, [sort: "nome", order: "asc"] )}" optionKey="id" name="usuarioCadastrante.id" class="form-control" value="${params?."usuarioCadastrante.id"}"/>
                        </div>

                        <div class="form-group col-sm-4">
                            <label for="setorSolicitante" class="control-label">
                                <g:message code="equipamento.setorSolicitante.label" default="Setor Solicitante" />:
                            </label>
                            <g:select noSelection="['':'--- Selecione ---']" from="${Setor.findAllByAtivo(true, [sort: "nome", order: "asc"] )}" optionKey="id" name="setorSolicitante.id" class="form-control" value="${params?."setorSolicitante.id"}"/>
                        </div>

                        <div class="form-group col-sm-4">
                            <label for="areaAtendimento" class="control-label">
                                <g:message code="equipamento.areaAtendimento.label" default="Área de Atendimento" />:
                            </label>
                            <g:select noSelection="['':'--- Selecione ---']" from="${AreaAtendimento.findAllByAtivo(true, [sort: "nome", order: "asc"] )}" optionKey="id" name="areaAtendimento.id" class="form-control" value="${params?."areaAtendimento.id"}"/>
                        </div>

                        <div class="form-group col-sm-4">
                            <label for="dateCreated" class="control-label">
                                <g:message code="equipamento.dateCreated.label" default="Data de Cadastro" />:
                            </label>
                            <div class="input-group">
                                <span class="input-group-addon">De</span>
                                <input type="text" class="form-control datepicker" name="dateCreatedInicio" value="${params?.dateCreatedInicio}"/>
                                <span class="input-group-addon" id="basic-addon1">Até</span>
                                <input type="text" class="form-control datepicker" name="dateCreatedFim" value="${params?.dateCreatedFim}"/>
                            </div>
                        </div>

                        <div class="form-group col-lg-offset-4 col-sm-4">
                            <label>&nbsp;</label>
                            <div>
                                <g:submitButton name="Filtrar" class="btn btn-primary pull-right"/>
                            </div>
                        </div>

                    </g:form>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <g:if test="${flash.message}">
                        <div class="alert alert-${tipoMensagem} alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                            <div class="message" role="status">${flash.message}</div>
                        </div>
                    </g:if>
                    <table class="table table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th> Ativo </th>
                            <th> ID </th>
                            <th> Área de Atendimento </th>
                            <th> Título </th>
                            <th> E-mail Solicitante </th>
                            <th> Setor </th>
                            <th> Data de Cadastro </th>
                            <th width="9%" colspan="3">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${chamadoInstanceList}" status="i" var="row">
                            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                                <td>${row?."ativo"}</td>
                                <td>${row?."chamado_id"}</td>
                                <td>${row?."area_atendimento"}</td>
                                <td>${row?."titulo"}</td>
                                <td>${row?."email_solicitante"}</td>
                                <td>${row?."setor"}</td>
                                <td>${row?."data_de_cadastro"}</td>

                                <td class="text-center">
                                    <g:link class="btn btn-xs btn-default" action="show" id="${row?."chamado_id"}" data-toggle="tooltip" data-placement="bottom" title="Visualizar"><i class="fa fa-eye"></i></g:link>
                                    <g:link class="btn btn-xs btn-default" action="edit" id="${row?."chamado_id"}" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="fa fa-pencil"></i></g:link>
                                    <g:form useToken="true" url="[controller: 'chamado', action:'delete']" method="DELETE" style="display: inline-block;">
                                        <g:hiddenField name="id" value="${row?."chamado_id"}"/>
                                        <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="bottom" title="Remover" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i class="glyphicon glyphicon-remove"></i></button>
                                    </g:form>
                                </td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>

                    <div class="pagination">
                        <g:paginate params="[q: "${query?:''}"]" class="pagination-sm" total="${chamadoInstanceList.size() ?: 0}" />
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

</section>
<g:javascript>
    $(document).ready(function(){
        TICKIT.selectMenu("relatorioChamadosOpt");
    });
</g:javascript>
</body>
</html>
