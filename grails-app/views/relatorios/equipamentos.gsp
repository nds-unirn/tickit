
<%@ page import="br.edu.unirn.tickit.acesso.Usuario" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="administrativo">
    <g:set var="entityName" value="${message(code: 'equipamento.label', default: 'Equipamentos')}" />
    <title><g:message code="default.report.label" args="[entityName]" default="Relatório de Equipamentos" /></title>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
    <ol class="breadcrumb">
        <li class="active">Relatório</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><g:message code="default.report.label" args="[entityName]" /></h3>
        </div>
        <div class="box-body">
        <g:if test="${flash.message}">
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ${flash.message}
                </div>
            </g:if>
            <div class="row">
                <div class="col-xs-12">
                    <g:form action="equipamentos">

                        <div class="form-group col-sm-3">
                            <label for="nome" class="control-label">
                                <g:message code="equipamento.nome.label" default="Nome" />:
                            </label>
                            <g:textField name="nome" class="form-control" value="${params?.nome}"/>
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="tipoEquipamento" class="control-label">
                                <g:message code="equipamento.tipoEquipamento.label" default="Tipo de Equipamento" />:
                            </label>
                            <g:select noSelection="['':'--- Selecione ---']" from="${br.edu.unirn.tickit.dominio.TipoEquipamento.list(sort: "nome", order: "asc")}" optionKey="id" name="tipoEquipamento" class="form-control" value="${tipoEquipamento?.id}"/>
                        </div>


                        <div class="form-group col-sm-4">
                            <label for="dataCompra" class="control-label">
                                <g:message code="equipamento.dataCompra.label" default="Data de Compra" />:
                            </label>
                            <div class="input-group">
                                <span class="input-group-addon">De</span>
                                <input type="text" class="form-control datepicker" name="dataInicioCompra" value=""/>
                                <span class="input-group-addon" id="basic-addon1">Até</span>
                                <input type="text" class="form-control datepicker" name="dataFinalCompra" value=""/>
                            </div>
                        </div>

                        <div class="form-group col-sm-2">
                            <label>&nbsp;</label>
                            <div>
                                <g:submitButton name="Filtrar" class="btn btn-primary pull-right"/>
                            </div>
                        </div>

                    </g:form>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <g:if test="${flash.message}">
                        <div class="alert alert-${tipoMensagem} alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                            <div class="message" role="status">${flash.message}</div>
                        </div>
                    </g:if>
                    <table class="table table-bordered table-condensed">
                        <thead>
                        <tr>
                            
                            
                            <th> Equipamento </th>
                            <th> Tipo de Equipamento </th>
                            <th> Tombamento </th>
                            <th> Locado </th>
                            <th> Data de Compra </th>
                            <th width="9%" colspan="3">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${equipamentoInstanceList}" status="i" var="equipamentoInstance">
                           
                                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                                
                               
                                <td>${equipamentoInstance?.nome}</td>
                                <td>${equipamentoInstance?.tipoEquipamento?.nome}</td>
                                 <td>${equipamentoInstance?.tombamento}</td>
                                <td>
                                    <g:if test="${equipamentoInstance?.locado == true}">
                                        Sim
                                    </g:if>
                                    <g:else>                                    
                                       Não
                                    </g:else>
                                </td>
                                <td><g:formatDate date="${equipamentoInstance?.dataCompra}" format="dd/MM/yyyy"/></td>

                                <td class="text-center">
                                    <g:link class="btn btn-xs btn-default" controller="equipamento" action="show" id="${equipamentoInstance?.id}" data-toggle="tooltip" data-placement="bottom" title="Visualizar"><i class="fa fa-eye"></i></g:link>
                                    <g:link class="btn btn-xs btn-default" action="edit" id="${equipamentoInstance?.id}" controller="equipamento" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="fa fa-pencil"></i></g:link>
                                    <g:form useToken="true" url="[controller: 'equipamento', action:'delete']" method="DELETE" style="display: inline-block;">
                                        <g:hiddenField name="id" value="${equipamentoInstance?.id}"/>
                                        <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="bottom" title="Remover" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i class="glyphicon glyphicon-remove"></i></button>
                                    </g:form>
                                </td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                    <div class="pagination">
                        <g:paginate params="[q: "${query?:''}"]" class="pagination-sm" total="${equipamentoInstanceCount ?: 0}" />
                    </div>
                    
                </div>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

</section>
<g:javascript>
    $(document).ready(function(){
        TICKIT.selectMenu("relatorioEquipamentosOpt");
    });
</g:javascript>
</body>
</html>
