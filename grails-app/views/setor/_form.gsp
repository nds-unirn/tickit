<%@ page import="br.edu.unirn.tickit.dominio.Setor; br.edu.unirn.tickit.dominio.Instituicao" %>



<div class="form-group ${hasErrors(bean: setorInstance, field: 'nome', 'error')} required">
	<label for="nome" class="col-sm-2 control-label">
		<g:message code="setor.nome.label" default="Nome" />:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5">
		<g:textField name="nome" required="" maxlength="255" value="${setorInstance?.nome}"/>

		<g:hasErrors bean="${setorInstance}" field="nome">
			<span class="help-block error"><g:renderErrors bean="${setorInstance}" field="nome" as="list" /></span>
		</g:hasErrors>
	</div>
</div>

<div class="form-group ${hasErrors(bean: setorInstance, field: 'instituicao', 'error')} required">
	<label for="instituicao" class="col-sm-2 control-label">
		<g:message code="setor.instituicao.label" default="Instituição" />:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-4">
		<g:select id="instituicao" name="instituicao.id" from="${Instituicao.findAllByAtivo(true,[sort:"nome", order:"asc"])}" optionKey="id" required="" value="${setorInstance?.instituicao?.id}" class="form-control"/>

		<g:hasErrors bean="${setorInstance}" field="instituicao">
			<span class="help-block error"><g:renderErrors bean="${setorInstance}" field="instituicao" as="list" /></span>
		</g:hasErrors>
	</div>
</div>

