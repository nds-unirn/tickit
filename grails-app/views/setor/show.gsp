
<%@ page import="br.edu.unirn.tickit.dominio.Setor" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="administrativo">
	<g:set var="entityName" value="${message(code: 'setor.label', default: 'Setor')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<section class="content-header">
	<ol class="breadcrumb">
		<li class="active">Listagem</li>
		<li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><g:message code="default.show.label" args="[entityName]" /></h3>
		</div>

		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<table class="table table-view table-condensed">
						<thead>
							<tr>
								<th colspan="2">
									Dados do ${entityName}
								</th>
							</tr>
						</thead>
						<tbody>
							
							<g:if test="${setorInstance?.nome}">
								<tr>
									<td><strong><g:message code="setor.nome.label" default="Nome" />:</strong></td>
									
									<td><g:fieldValue bean="${setorInstance}" field="nome"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${setorInstance?.instituicao}">
								<tr>
									<td><strong><g:message code="setor.instituicao.label" default="Instituição" />:</strong></td>
									
									<td><g:link controller="instituicao" action="show" id="${setorInstance?.instituicao?.id}">${setorInstance?.instituicao?.encodeAsHTML()}</g:link></td>
									
								</tr>
							</g:if>
							
							<g:if test="${setorInstance?.ativo}">
								<tr>
									<td><strong><g:message code="setor.ativo.label" default="Ativo" />:</strong></td>
									
									<td><g:formatBoolean boolean="${setorInstance?.ativo}" /></td>
									
								</tr>
							</g:if>
							
							<g:if test="${setorInstance?.dateCreated}">
								<tr>
									<td><strong><g:message code="setor.dateCreated.label" default="Data de Cadastro" />:</strong></td>
									
									<td><g:formatDate date="${setorInstance?.dateCreated}" format="dd/MM/yyyy HH:mm"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${setorInstance?.usuarioCadastrante}">
								<tr>
									<td><strong><g:message code="setor.usuarioCadastrante.label" default="Usuário Cadastrante" />:</strong></td>
									
									<td><g:link controller="usuario" action="show" id="${setorInstance?.usuarioCadastrante?.id}">${setorInstance?.usuarioCadastrante?.encodeAsHTML()}</g:link></td>
									
								</tr>
							</g:if>
							
						</tbody>
					</table>
					<div class="box-footer clearfix">
						<g:link action="index" class="btn btn-primary pull-right"><i class="fa fa-reply"></i>Voltar</g:link>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<g:javascript>
        $(document).ready(function(){
            TICKIT.selectMenu("setorOpt");
        });
</g:javascript>
</body>
</html>
