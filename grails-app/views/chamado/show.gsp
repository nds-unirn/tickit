
<%@ page import="br.edu.unirn.tickit.dominio.Chamado" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="administrativo">
	<g:set var="entityName" value="${message(code: 'chamado.label', default: 'Chamado')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>

 	<script type="text/javascript">

    	function display(){
		    
		        document.getElementById('areaAtual').style.display = 'none';
		        document.getElementById('areaNova').style.display = 'block';
		    
		}


		function displayBoxComentario(){
		    
		        document.getElementById('comentario').style.display = 'block';
		    
		}

		function closeBoxComentario(){
		    
		        document.getElementById('comentario').style.display = 'none';
		    
		}

    </script>

</head>
<body>
<section class="content-header">
	<ol class="breadcrumb">
		<li class="active">Listagem</li>
		<li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">

			<div class="row">

				<div class="col-md-8">
					<h3 class="box-title"><g:message code="default.show.label" args="[entityName]" /></h3>
				</div>
				<div class="col-md-2">
					<h3 class="box-title pull-right" >Status: </h3>
				</div>
				<div class="col-md-2">
						<g:if test="${status?.status[0]?.nome?.equals('Aberto')}">
							<button type="button" class="btn btn-success"><i class="fa fa-folder-open-o"></i>${status.status[0]}</button>
						</g:if>
						<g:if test="${status?.status[0]?.nome?.equals('Cancelado')}">
							<button type="button" class="btn btn-danger"><i class="fa fa-ban"></i>${status.status[0]}</button>
						</g:if>
						<g:if test="${status?.status[0]?.nome?.equals('Em espera')}">
							<button type="button" class="btn btn-warning"><i class="fa fa-pause"></i>${status.status[0]}</button>
						</g:if>
						<g:if test="${status?.status[0]?.nome?.equals('Em andamento')}">
							<button type="button" class="btn btn-warning"><i class="fa fa-play"></i>${status.status[0]}</button>
						</g:if>
						<g:if test="${status?.status[0]?.nome?.equals('Resolvido')}">
							<button type="button" class="btn btn-success"><i class="fa fa-check"></i>${status.status[0]}</button>
						</g:if>
						<g:if test="${status?.status[0]?.nome?.equals('Pendente')}">
							<button type="button" class="btn btn-warning"><i class="fa fa-exclamation-circle"></i>${status.status[0]}</button>
						</g:if>

						<br /><a href="#openModalStatus">Alterar</a> 
					</div>
				</div>

		</div>

		<div class="box-body">

			<g:if test="${flash.message}">
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					${flash.message}
				</div>
			</g:if>

			<g:if test="${mensagemErro}">
			  <div class="alert alert-error alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
					<div class="message" role="status">${mensagemErro}</div>
				</div>
			</g:if>
			<g:if test="${mensagemSucesso}">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
					<div class="message" role="status">${mensagemSucesso}</div>
					</div>
			</g:if>

			<div class="row">
				<div class="col-xs-12">
					<table class="table table-view table-condensed">
						<thead>
							<tr>
								<th colspan="2">
									Id do ${entityName}: ${chamadoInstance?.id}
								</th>
							</tr>
						</thead>
						<tbody>
							
							<g:if test="${chamadoInstance?.titulo}">
								<tr>
									<td><strong><g:message code="chamado.titulo.label" default="Título" />:</strong></td>
									
									<td><g:fieldValue bean="${chamadoInstance}" field="titulo"/></td>
									
								</tr>
							</g:if>

							<g:if test="${chamadoInstance?.descricao}">
								<tr>
									<td><strong><g:message code="chamado.descricao.label" default="Descriçao" />:</strong></td>
									
									<td><g:fieldValue bean="${chamadoInstance}" field="descricao"/></td>
									
								</tr>
							</g:if>

							<g:if test="${chamadoInstance?.emailSolicitante}">
								<tr>
									<td><strong><g:message code="chamado.emailSolicitante.label" default="E-mail do solicitante" />:</strong></td>
									
									<td><g:fieldValue bean="${chamadoInstance}" field="emailSolicitante"/></td>
									
								</tr>
							</g:if>

							<g:if test="${chamadoInstance?.setorSolicitante}">
								<tr>
									<td><strong><g:message code="chamado.setorSolicitante.label" default="Setor solicitante" />:</strong></td>
									
									<td><g:fieldValue bean="${chamadoInstance}" field="setorSolicitante"/></td>
									
								</tr>
							</g:if>


							<g:if test="${chamadoInstance?.areaAtendimento}">
								
								
									<tr>
										<td><strong><g:message code="chamado.areaAtendimento.label" default="Área de atendimento" />:</strong></td>
										
										<td>
											<div id="areaAtual" style="display:block;">
									
												<g:fieldValue bean="${chamadoInstance}" field="areaAtendimento"/>
												   - 
												 <button class="btn btn-danger" id="alterar" onclick="display();">Alterar</button>
									
								
											</div>

											<div id="areaNova" style="display:none;">
												<g:form useToken="true" class="form-horizontal" novalidate="novalidate"  name="chamadoInstanceForm" url="[resource:chamadoInstance, action:'update']" method="PUT" >
													<g:select id="areaAtendimento" name="areaAtendimento" from="${areasAtendimento}" value="${chamadoInstance?.areaAtendimento?.id}" optionKey="id" optionValue="nome" />

												   - 
													<button id="atualizar" class="btn btn-success">Atualizar</button>
											 	</g:form>
											</div>
										 </td>
										
									</tr>
								
							</g:if>

							
							<g:if test="${chamadoInstance?.dateCreated}">
								<tr>
									<td><strong><g:message code="chamado.dateCreated.label" default="Data de Cadastro" />:</strong></td>
									
									<td><g:formatDate date="${chamadoInstance?.dateCreated}" format="dd/MM/yyyy HH:mm"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${chamadoInstance?.usuarioCadastrante}">
								<tr>
									<td><strong><g:message code="chamado.usuarioCadastrante.label" default="Usuário Cadastrante" />:</strong></td>
									
									<td><g:link controller="usuario" action="show" id="${chamadoInstance?.usuarioCadastrante?.id}">${chamadoInstance?.usuarioCadastrante?.encodeAsHTML()}</g:link></td>
									
								</tr>
							</g:if>
							
						</tbody>
					</table>
					
							<div class="box-footer clearfix">
								
								<g:link action="index" controller="dashboard" class="btn btn-primary pull-right"><i class="fa fa-reply"></i>Voltar</g:link>
							
							</div>
						
				</div>
			</div>
		</div>
	</div>
</section>


<!-- Upload de anexo-->

<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<div class="row">
				<div class="col-md-2">
					<h3 class="box-title">Anexos</h3>
				</div>
				<div class="col-md-2">
					<img src="${resource(dir:"images", file: "anexo.png") }" />
				<div>
				<div class="col-md-8">
				</div>
			</div>
		</div>


		<div class="box-body">
			
			<div class="row">
				
				<div class="col-md-12">
					<g:form enctype="multipart/form-data" useToken="true"
					novalidate="novalidate" name="chamadoInstanceForm" url="[resource:chamadoInstance, action:'upload']" method="PUT">

						<span class="button">                   
			                    <input type="file" name="anexo"/>
			                    <span style="font-style:italic; ">*(Formatos permitidos: pdf, jpg, jpeg, png, doc, docx, xls, xlsx, ppt, pptx.)</span><br /><br />
			                    <input type="submit" class="btn btn-primary" value="upload"/>
			            </span>

					</g:form>
				</div>
			</div>

			<br />

			<div class="row">
			<div class="col-xs-12">

				<g:if test="${!anexosDoChamado.isEmpty()}">
				<table class="table table-view table-condensed">
					<thead>
						<tr>
							<th>
								Data
							</th>
							<th>
								Arquivo
							</th>
							<th>
								Anexado por
							</th>
							<th>
								Ações
							</th>
						</tr>
					</thead>
					<tbody>
						
						<g:each in="${anexosDoChamado}">
							<tr>
								<td>

								<g:formatDate date="${it.dateCreated}" type="datetime"
								 style="MEDIUM"/>

								</td>

								<td><a href="${createLinkTo( dir:"images" , file:"${it.nomeArquivo}")}" target="_new">${it.nomeArquivo}</a></td>
								
								
								<td>${it.usuarioCadastrante.nome}</td>
								
								<td>
								<g:form useToken="true" url="[resource:chamadoInstance, action:'show']" method="PUT" style="display: inline-block;">
									<g:hiddenField name="anexoId" value="${it?.id}" />
									<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="bottom" title="Remover" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i class="glyphicon glyphicon-remove"></i></button>
								</g:form>

							

								</td>
								
							</tr>
					
						</g:each>
						
					</tbody>
				</table>
				</g:if>
				<g:else>
					Este chamado nao possui anexo
				</g:else>
				<div class="box-footer clearfix">
					
				</div>
			</div>
		</div>

		</div>

	</div>

</section>


<!-- Tabela de Responsaveis-->

<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Responsáveis pelo chamado</h3>

			<div class="box-tools pull-right">
				
				<a href="#openModal">Adicionar responsável</a> 

			</div>

		</div>


		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">

					<g:if test="${!responsaveisChamado.isEmpty()}">
					<table class="table table-view table-condensed">
						<thead>
							<tr>
								<th>
									Data
								</th>
								<th>
									Responsável
								</th>
								<th>
									Ações
								</th>
							</tr>
						</thead>
						<tbody>
							
							<g:each in="${responsaveisChamado}">
								<tr>
									<td>

									<g:formatDate date="${it.dateCreated}" type="datetime"
									 style="MEDIUM"/>

									</td>
									
									<td>${it.usuario.nome}</td>
									<td>
										


									<g:form useToken="true" url="[resource:chamadoInstance, action:'show']" method="PUT" style="display: inline-block;">
										<g:hiddenField name="usuarioId" value="${it?.id}" />
										<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="bottom" title="Remover" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i class="glyphicon glyphicon-remove"></i></button>
									</g:form>

								

									</td>
									
								</tr>
						
							</g:each>
							
						</tbody>
					</table>
					</g:if>
					<g:else>
						Este chamado nao possui responsável vinculado
					</g:else>
					<div class="box-footer clearfix">
						
					</div>
				</div>
			</div>



		</div>


	</div>



</section>






<!-- Tabela de equipamento-->

<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Equipamentos do chamado</h3>

			<div class="box-tools pull-right">
				
				<a href="#openModalEquipamento">Adicionar equipamento</a> 

			</div>

		</div>


		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">

					<g:if test="${!equipamentosChamado.isEmpty()}">
					<table class="table table-view table-condensed">
						<thead>
							<tr>
								<th>
									Data
								</th>
								<th>
									Equipamento
								</th>
								<th>
									Ações
								</th>
							</tr>
						</thead>
						<tbody>
							
							<g:each in="${equipamentosChamado}">
								<tr>
									<td>

									<g:formatDate date="${it.dateCreated}" type="datetime"
									 style="MEDIUM"/>

									</td>
									
									<td>${it.equipamento.nome}</td>
									<td>
										


									<g:form useToken="true" url="[resource:chamadoInstance, action:'show']" method="PUT" style="display: inline-block;">
										<g:hiddenField name="equipamentoId" value="${it?.id}" />
										<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="bottom" title="Remover" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i class="glyphicon glyphicon-remove"></i></button>
									</g:form>

								

									</td>
									
								</tr>
						
							</g:each>
							
						</tbody>
					</table>
					</g:if>
					<g:else>
						Este chamado nao possui equipamento vinculado
					</g:else>
					<div class="box-footer clearfix">
						
					</div>
				</div>
			</div>



		</div>


	</div>



</section>

<!-- Tabela de Status-->

<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Histórico de status</h3>


		</div>


		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">

					<g:if test="${!historicoStatus.isEmpty()}">
					<table class="table table-view table-condensed">
						<thead>
							<tr>
								<th>
									Data
								</th>
								<th>
									Alterado Por
								</th>
								<th>
									Status
								</th>
							</tr>
						</thead>
						<tbody>
							
							<g:each in="${historicoStatus}">
								<tr>
									<td>

									<g:formatDate date="${it.dateCreated}" type="datetime"
									 style="MEDIUM"/>

									</td>
									
									<td>

									<g:if test="${it.usuarioCadastrante != null}">${it.usuarioCadastrante.nome}
									</g:if>
									<g:else>Usuário nao logado</g:else>
									</td>
									<td>
										
									${it.status}

									</td>
									
								</tr>
						
							</g:each>
							
						</tbody>
					</table>
					</g:if>
					<g:else>
						Este chamado nao possui histórico de status
					</g:else>
					<div class="box-footer clearfix">
						
					</div>
				</div>
			</div>



		</div>


	</div>



</section>


<!-- Tabela de Comentarios-->

<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Comentários</h3>

			<div class="box-tools pull-right">
				
				<button id="adicionar" class="btn btn-default" onclick="displayBoxComentario();">Adicionar comentário</button> 

			</div>

		</div>


		<div id="comentario" style="display:none;">
			<g:form useToken="true" class="form-horizontal" novalidate="novalidate"  url="[resource:chamadoInstance, action:'show']" method="PUT" >

			<div class="container">

				<div class="row">
				
					<g:textArea name="comentario" value="" rows="5" cols="100" />
				
				</div>
				<div class="row">
					<button id="atualizar" class="btn btn-success">Adicionar</button>
					<button id="cancelar" type="button" onclick="closeBoxComentario();" class="btn btn-default">Cancelar</button>
				</div>
			</div>
		 </g:form>
		</div>

		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">

					<g:if test="${!comentariosChamado.isEmpty()}">
				
							
							<g:each in="${comentariosChamado}">
								<div class="row">
									<div class="col-md-2">
										<div>
											<img src="${resource(dir:"images", file: "profile.png") }"
											/>
											<br />
											<g:formatDate date="${it.dateCreated}" type="date"
											 style="MEDIUM"/><br/>
											 <g:formatDate date="${it.dateCreated}" type="time"
											 style="MEDIUM"/>
										 </div>
									</div>
									<div class="col-md-10">

										<div style="margin-top: 20px;">
											<span style="font-weight:bold; ; ">Usuário: </span>
											<span style="font-style:italic; ">${it.usuarioCadastrante.nome}</span>
											<br />
											<span style="font-weight:bold; ; ">Comentário:</span>  <span style="font-style:italic; ">${it.comentario}</span>
										</div>
									</div>
								</div>	<br/>
						
							</g:each>
							
					
					</g:if>
					<g:else>
						Este chamado nao possui comentários
					</g:else>
				
				</div>
			</div>



		</div>


	</div>



</section>


<!-- Modal Status -->

<div id="openModalStatus" class="modalDialog">
    <div>	

    	<a href="#close" title="Close" class="close">X</a>

        	<h2>Escolha o Status</h2>

        	<div class="content">

	        	<g:form url="[resource:chamadoInstance, action:'show']" name="modalStatus">
	        		<g:hiddenField name="chamadoId" value="${chamadoInstance?.id}" />

	        		<g:select class="form-control" name="status" from="${statusChamado}" noSelection="${['null':'Selecione']}" required="true"
	        					optionKey="id" optionValue="nome" />

	        		<button type="submit" form="modalStatus" class="btn btn-success pull-right" style="margin-top:100px; "><i class="fa fa-check"></i>Alterar</button>


	        	</g:form>
        	</div>
    </div>
</div>


<!-- Modal responsavel -->

<div id="openModal" class="modalDialog">
    <div>	

    	<a href="#close" title="Close" class="close">X</a>

        	<h2>Escolha o Responsável</h2>

        	<div class="content">

	        	<g:form url="[resource:chamadoInstance, action:'show']" name="modalResponsavel">
	        		<g:hiddenField name="chamadoId" value="${chamadoInstance?.id}" />

	        		<g:select class="form-control" name="addResponsavel" from="${responsaveis}" noSelection="${['null':'Selecione']}" required="true"
	        					optionKey="id" optionValue="nome" />

	        		<button type="submit" form="modalResponsavel" class="btn btn-success pull-right" style="margin-top:100px; "><i class="fa fa-check"></i>Adicionar</button>


	        	</g:form>
        	</div>
    </div>
</div>

<!-- Modal equipamento -->

<div id="openModalEquipamento" class="modalDialog">
    <div>	

    	<a href="#close" title="Close" class="close">X</a>

        	<h2>Escolha o Equipamento</h2>

        	<div class="content">

	        	<g:form url="[resource:chamadoInstance, action:'show']" name="modalEquipamento">
	        		<g:hiddenField name="chamadoId" value="${chamadoInstance?.id}" />

	        		<g:select class="form-control" name="addEquipamento" from="${equipamentos}" noSelection="${['null':'Selecione']}" required="true"
	        					optionKey="id" optionValue="nome" />

	        		<button type="submit" form="modalEquipamento" class="btn btn-success pull-right" style="margin-top:100px; "><i class="fa fa-check"></i>Adicionar</button>


	        	</g:form>
        	</div>
    </div>
</div>


<g:javascript>
        $(document).ready(function(){
            TICKIT.selectMenu("chamadoOpt");
        });
</g:javascript>
</body>
</html>

