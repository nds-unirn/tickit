<%@ page import="br.edu.unirn.tickit.dominio.Chamado" %>



<div class="fieldcontain ${hasErrors(bean: chamadoInstance, field: 'titulo', 'error')} required">
	<label for="titulo">
		<g:message code="chamado.titulo.label" default="Titulo" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="titulo" required="" value="${chamadoInstance?.titulo}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: chamadoInstance, field: 'descricao', 'error')} required">
	<label for="descricao">
		<g:message code="chamado.descricao.label" default="Descricao" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="descricao" required="" value="${chamadoInstance?.descricao}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: chamadoInstance, field: 'emailSolicitante', 'error')} required">
	<label for="emailSolicitante">
		<g:message code="chamado.emailSolicitante.label" default="Email Solicitante" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="emailSolicitante" required="" value="${chamadoInstance?.emailSolicitante}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: chamadoInstance, field: 'setorSolicitante', 'error')} required">
	<label for="setorSolicitante">
		<g:message code="chamado.setorSolicitante.label" default="Setor Solicitante" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="setorSolicitante" name="setorSolicitante.id" from="${br.edu.unirn.tickit.dominio.Setor.list()}" optionKey="id" required="" value="${chamadoInstance?.setorSolicitante?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: chamadoInstance, field: 'areaAtendimento', 'error')} required">
	<label for="areaAtendimento">
		<g:message code="chamado.areaAtendimento.label" default="Area Atendimento" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="areaAtendimento" name="areaAtendimento.id" from="${br.edu.unirn.tickit.dominio.AreaAtendimento.list()}" optionKey="id" required="" value="${chamadoInstance?.areaAtendimento?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: chamadoInstance, field: 'usuarioCadastrante', 'error')} ">
	<label for="usuarioCadastrante">
		<g:message code="chamado.usuarioCadastrante.label" default="Usuario Cadastrante" />
		
	</label>
	<g:select id="usuarioCadastrante" name="usuarioCadastrante.id" from="${br.edu.unirn.tickit.acesso.Usuario.list()}" optionKey="id" value="${chamadoInstance?.usuarioCadastrante?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: chamadoInstance, field: 'ativo', 'error')} ">
	<label for="ativo">
		<g:message code="chamado.ativo.label" default="Ativo" />
		
	</label>
	<g:checkBox name="ativo" showInForm="false" value="${chamadoInstance?.ativo}" />

</div>

