<li class="header">MENU PRINCIPAL</li>
<li id="indexOpt">
    <a href="${createLink(controller: 'dashboard')}">
        <i class="fa fa-dashboard"></i> <span>Vis&atilde;o Geral</span>
    </a>
</li>
<li id="createChamdoOpt">
    <a href="${createLink(controller: 'publico')}">
        <i class="fa fa-edit"></i> <span>Novo Chamado</span>
    </a>
</li>

<g:if test="${session?.usuario?.hasPerfilWithNome("Administrador") || session?.usuario?.hasPerfilWithNome("Supervisor")}">
<li class="treeview">
    <a href="#">
        <i class="fa fa-desktop"></i> <span>Equipamentos</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li id="equipamentoOpt"><a href="${createLink(controller: 'equipamento', action: 'index')}">Equipamentos</a></li>
        <li id="tipoEquipamentoOpt"><a href="${createLink(controller: 'tipoEquipamento', action: 'index')}">Tipos de Equipamento</a></li>
         <li id="caracteristicaOpt"><a href="${createLink(controller: 'caracteristica', action: 'index')}">Características</a></li>
    </ul>
</li>
</g:if>
<g:if test="${session?.usuario?.hasPerfilWithNome("Administrador")}">
<li class="treeview">
    <a href="#">
        <i class="fa fa-leaf"></i> <span>Relatórios</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li id="relatorioEquipamentosOpt"><a href="${createLink(controller: 'relatorios', action: 'equipamentos')}"> Equipamentos </a></li>
        <!--<li id="relatorioChamadosOpt"><a href="${createLink(controller: 'relatorios', action: 'chamados')}"> Chamados </a></li> -->
    </ul>
</li>
</g:if>

<li class="treeview">
    <a href="#">
        <i class="fa fa-exclamation-triangle"></i> <span>Ocorrências</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li id="ocorrenciaOpt"><a href="${createLink(controller: 'ocorrencia', action: 'index')}">Ocorrências</a></li>
        <g:if test="${session?.usuario?.hasPerfilWithNome("Administrador")}">
            <li id="tipoOcorrenciaOpt"><a href="${createLink(controller: 'tipoOcorrencia', action: 'index')}">Tipos de Ocorrência</a></li>
        </g:if>
    </ul>
</li>
<g:if test="${session?.usuario?.hasPerfilWithNome("Administrador")}">
<li class="treeview">
    <a href="#">
        <i class="fa fa-cogs"></i> <span>Configurações</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">

        <li id="areaAtendimentoOpt"><a href="${createLink(controller: 'areaAtendimento', action: 'index')}">Áreas atendimento</a></li>
        <li id="usuarioOpt"><a href="${createLink(controller: 'usuario', action: 'index')}">Usuário</a></li>
        <li id="instituicaoOpt"><a href="${createLink(controller: 'instituicao', action: 'index')}">Instituição</a></li>
        <li id="setorOpt"><a href="${createLink(controller: 'setor', action: 'index')}">Setor</a></li>
        <li id="statusOpt"><a href="${createLink(controller: 'status', action: 'index')}">Status</a></li>

    </ul>
</li>
</g:if>