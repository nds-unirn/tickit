<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>TickIT</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">

	<g:javascript>
        window.baseUrl = "${createLink(uri: '/', absolute: true)}"
	</g:javascript>
	<asset:stylesheet src="application.css"/>
	<asset:javascript src="application.js"/>
	<asset:stylesheet src="jquery-ui.css"/>
	<asset:javascript src="jquery-ui.js"/>
	<asset:stylesheet src="jquery-ui-timepicker-addon.css"/>
	<asset:javascript src="jquery-ui-timepicker-addon.js"/>
	<g:layoutHead/>
</head>
<body class="skin-blue">
<div class="wrapper">

	<header class="main-header">
		<g:link controller="dashboard" class="logo" > Tick<b>IT</b> </g:link>
	<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top" role="navigation">
			<!-- Sidebar toggle button-->
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<!-- User Account: style can be found in dropdown.less -->
					<li class="dropdown options">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="glyphicon glyphicon-user"></i>
							<span class="hidden-xs"> &nbsp; ${session?.usuario?.nome} </span>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu animated bounceIn" role="menu">
							<li>
								<span class="arrow top"></span>
								<g:link controller="usuario" action="edit" id="${session?.usuario?.id}">
									<span class="icon"><i class="fa fa-user"></i>
									</span>Editar Perfil</g:link>
								<g:link controller="autenticacao" action="logout">
									<span class="icon"><i class="fa fa-sign-out"></i>
									</span>Sair</g:link>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
	</header>

	<!-- =============================================== -->

	<!-- Left side column. contains the sidebar -->
	<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
			<!-- sidebar menu: : style can be found in sidebar.less -->
			<ul class="sidebar-menu">
				<g:render template="/layouts/sideMenu"/>
			</ul>
		</section>
		<!-- /.sidebar -->
	</aside>

	<!-- =============================================== -->
	<div class="content-wrapper">
		<g:layoutBody/>
	</div>

	<footer class="main-footer">
		<div class="pull-right hidden-xs">
			<b>Version</b> <g:meta name="app.version"/>
		</div>
		<strong>&copy; 2015 <a href="javascript:void(0);">NDS-UNIRN</a>.</strong>
	</footer>
</div>
</body>
</html>
