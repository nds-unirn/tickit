<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>TickIT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    %{--<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
    <link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">--}%
    <g:javascript>
        window.baseUrl = "${createLink(uri: '/', absolute: true)}"
    </g:javascript>
    <asset:stylesheet src="application.css"/>
    <asset:javascript src="application.js"/>
    <g:layoutHead/>
</head>
<body class="skin-blue sidebar-collapse">
<div class="wrapper">

    <header class="main-header">
        <g:if test="${session?.usuario == null}">
            <g:link controller="publico" class="logo" > Tick<b>IT</b> </g:link>
        </g:if>
        <g:else>
             <g:link controller="dashboard" action="index" class="logo" > Tick<b>IT</b> </g:link>
        </g:else>
    <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li> 
                    <g:if test="${session?.usuario == null}">
                        <g:link controller="autenticacao" action="login" > <b>Área administrativa</b> </g:link> </li>
                    </g:if>
                    <!-- User Account: style can be found in dropdown.less -->
                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- =============================================== -->
    <div class="content-wrapper">
        <g:layoutBody/>
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> <g:meta name="app.version"/>
        </div>
        <strong>&copy; 2015 <a href="javascript:void(0);">NDS-UNIRN</a>.</strong>
    </footer>
</div>
</body>
</html>
