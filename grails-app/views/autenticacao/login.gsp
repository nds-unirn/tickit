<%@ page import="grails.util.Environment" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="administrativoAutenticar"/>
    <title>TickIT</title>
</head>
<body>
    <div class="login-logo">
        Tick<b>IT</b>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Faça login para começar a sua sessão</p>

        <g:if test="${flash.message}">
            <div class="alert alert-${tipoMensagem} alert-dismissable">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                <div class="message" role="status">${flash.message}</div>
            </div>
        </g:if>

        <g:form controller="autenticacao" action="autenticar">
            <g:set var="isProducao" value="${Environment.current == Environment.PRODUCTION}" />
            <div class="form-group has-feedback">
                <input type="text" name="login" class="form-control" placeholder="Login" value="${!isProducao ? "admin" : ""}"/>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="senha" class="form-control" placeholder="Senha" value="${!isProducao ? "admin" : ""}"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="form-group">
                        <input type="checkbox" name="lembrarMe"/> Lembrar-me
                    </div>
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
                </div>
            </div>
        </g:form>
    </div>
</body>
</html>