<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="publico"/>
	<title>TickIT</title>
</head>
<body>
<section class="container">
	<div style="width:100%;text-align: center;">
		<h2>Seja bem-vindo ao TickIT</h2>
		<p>O TickIT é o sistema de chamados utilizado pela Liga de ensino do Rio Grande do Norte.
		<br>Para abrir um chamado basta preencher os campos abaixo: </p>
	</div>

</section>
<!-- Main content -->
<section class="content" style="width:60%; ">
	<!-- Default box -->


	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Abrir chamado</h3>
		</div>



		<div class="box-body box-form">
			<g:if test="${flash.message}">
				<div class="alert alert-${tipoMensagem} alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					${flash.message}
				</div>
			</g:if>

			<g:hasErrors bean="${chamadoInstance}">
				<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Alguns campos precisam ser corrigidos, por favor confira o preenchimento do formulário.
				</div>
			</g:hasErrors>
			<g:form controller="publico" action="abrirChamado">

			<div class="row">
			<div class="col-md-12">
				<div class="col-md-6">
					
						<div class="form-group required">
						<label for="titulo">
							<g:message code="chamado.titulo.label" default="Título" />:
							<span class="required-indicator">*</span>
						</label>
						<div class="">
	                    	<input id="titulo" class="form-control input-lg" type="text" name="titulo" placeholder="Insira o título do chamado" value="${chamadoInstance?.titulo}">

							<g:hasErrors bean="${chamadoInstance}" field="titulo">
								<span class="help-block error"><g:renderErrors bean="${chamadoInstance}" field="titulo" as="list" /></span>
							</g:hasErrors>
						</div>
					</div>

					</div>
					<div class="col-md-6">
						
					<div class="form-group required">
						<label for="emailSolicitante">
							<g:message code="chamado.emailSolicitante.label" default="Email solicitante" />:
							<span class="required-indicator">*</span>
						</label>
						<div class="">
	                    	<input id="emailSolicitante" class="form-control input-lg" type="text" name="emailSolicitante" placeholder="Insira o seu email" value="${chamadoInstance?.emailSolicitante}">

							<g:hasErrors bean="${chamadoInstance}" field="emailSolicitante">
								<span class="help-block error"><g:renderErrors bean="${chamadoInstance}" field="emailSolicitante" as="list" /></span>
							</g:hasErrors>
						</div>
					</div>
					
					</div>
					</div>
				</div>
				<div class="row">

				<div class="col-md-12">
					<div class="col-md-6">

					<div class="form-group required">
						<label for="areaAtendimento">
							<g:message code="chamado.areaAtendimento.label" default="Área de atendimento" />:
							<span class="required-indicator">*</span>
						</label>
						<div class="">
	                    	<g:select id="areaAtendimento" name="areaAtendimento" from="${areasAtendimento}" value="${chamadoInstance?.areaAtendimento?.id}" optionKey="id" optionValue="nome" class="form-control input-lg"/>

							<g:hasErrors bean="${chamadoInstance}" field="areaAtendimento">
								<span class="help-block error"><g:renderErrors bean="${chamadoInstance}" field="areaAtendimento" as="list" /></span>
							</g:hasErrors>
						</div>
					</div>

					</div>
					<div class="col-md-6">

					<div class="form-group required">
						<label for="setorSolicitante">
							<g:message code="chamado.setorSolicitante.label" default="Setor Solicitante" />:
							<span class="required-indicator">*</span>
						</label>
						<div class="">
	                    	<g:select id="setorSolicitante" name="setorSolicitante" from="${setores}" value="${chamadoInstance?.setorSolicitante?.id}" class="form-control input-lg" optionKey="id" optionValue="nome"/>

							<g:hasErrors bean="${chamadoInstance}" field="setorSolicitante">
								<span class="help-block error"><g:renderErrors bean="${chamadoInstance}" field="setorSolicitante" as="list" /></span>
							</g:hasErrors>
						</div>
						</div>
					</div>
					</div>
					</div>

				<div class="row">

				<div class="col-md-12">
					<div class="col-md-12">

					<div class="form-group">
	                    <label for="descricao">Descrição</label>
	                    <textarea id="descricao" class="form-control" name="descricao" rows="6" placeholder="Descreva o problema enfrentado...">${chamadoInstance?.descricao}</textarea>
						
						<g:hasErrors bean="${chamadoInstance}" field="descricao">
							<span class="help-block error"><g:renderErrors bean="${chamadoInstance}" field="descricao" as="list" /></span>
						</g:hasErrors>
	                </div>

					<div class="box-footer clearfix">
						<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> <g:message code="default.button.abrirChamado.label"  default="Abrir chamado"/></button>
						<button type="reset" class="btn btn-danger pull-left"><i class="fa fa-close"></i>Limpar formulário</button>
					</div>
					</div>
					</div>
				</div>

			</g:form>

		</div>
		<!--  Fim do Box-body --> 
		
		</div>
	</div>
</section>
<g:javascript>
        $(document).ready(function(){
            TICKIT.selectMenu("equipamentoOpt");
        });
</g:javascript>
</body>
</html>
