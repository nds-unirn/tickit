<%@ page import="br.edu.unirn.tickit.acesso.Usuario; br.edu.unirn.tickit.dominio.AreaAtendimento; br.edu.unirn.tickit.dominio.Status; br.edu.unirn.tickit.dominio.Setor" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="administrativo"/>
	<title>TickIT</title>
</head>
<body>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Listagem de Chamados</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-xs-12">
							<g:form action="index">
								<a href="#pesquisa" data-toggle="collapse" class="pull-right help-block collapse-btn">Filtros</a>
								<div class="col-xs-12 margin-bottom-10 collapse" id="pesquisa">

									<div class="col-sm-4">
										<div class="form-group">
											<label class="control-label" for="setor">
												Setor
											</label>
											<g:select id="setor" name="setor.id" from="${Setor?.findAllByAtivo(true, [sort:'nome', order:"asc"])}" optionKey="id" value="${params?.setor?.id}" class="form-control" noSelection="['':'- Selecione -']"/>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group">
											<label class="control-label" for="status">
												Status
											</label>
											<g:select id="status" name="status.id" from="${Status?.findAllByAtivo(true, [sort:'nome', order:"asc"])}" optionKey="id" value="${params?.status?.id}" class="form-control" noSelection="['':'- Selecione -']"/>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label class="control-label" for="areaAtendimento">
												Área de Atendimento
											</label>
											<g:select id="areaAtendimento" name="areaAtendimento.id" from="${AreaAtendimento?.findAllByAtivo(true, [sort:'nome', order:"asc"])}" optionKey="id" value="${params?.areaAtendimento?.id}" class="form-control" noSelection="['':'- Selecione -']"/>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label class="control-label" for="usuario">
												Responsável
											</label>
											<g:select id="usuario" name="usuario.id" from="${Usuario.findAllByAtivo(true, [sort:'nome', order:"asc"])}" optionKey="id" value="${params?.areaAtendimento?.id}" class="form-control" noSelection="['':'- Selecione -']"/>
										</div>
									</div>

									<div class="col-sm-2 padding-top-20">
										<button type="submit" class="btn btn-block btn-primary"><i class="fa fa-search"></i>Pesquisar</button>
									</div>
								</div>
							</g:form>
						</div>
					</div>

					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<div class="row">
							<div class="col-sm-12">
								<g:if test="${flash.message}">
									<div class="alert alert-error alert-dismissable">
										<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
										<div class="message" role="status">${flash.message}</div>
									</div>
								</g:if>
								<table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="Dashboard">
									<thead>
										<tr role="row">
											<th>Data</th>
											<th>Título</th>
											<th>Área de Atendimento</th>
											<th>Setor</th>
											<th>Status</th>
											<th width="3%" colspan="1">&nbsp;</th>
										</tr>
									</thead>
									<tbody>
										<g:each in="${result}" status="i" var="it">
											<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

												<td><g:formatDate format="dd/MM/yyyy HH:mm" date="${it?.data}"/> </td>

												<td>${it?.titulo}</td>

												<td>${it?.area_de_atendimento}</td>

												<td>${it?.setor}</td>

												<td>${it?.status}</td>

												<td><g:link class="btn btn-info btn-sm" id="${it?.id}" controller="chamado" action="show" data-toggle="tooltip" data-placement="bottom" title="Visualizar"><i class="fa fa-eye"></i></g:link> </td>

											</tr>
										</g:each>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<asset:javascript src="visaogeral.js"/>
<g:javascript>
	$(document).ready(function(){
		TICKIT.selectMenu("indexOpt");
	});
</g:javascript>
</body>
</html>
