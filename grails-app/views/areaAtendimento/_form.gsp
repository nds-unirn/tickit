<%@ page import="br.edu.unirn.tickit.dominio.AreaAtendimento" %>


<div class="form-group ${hasErrors(bean: areaAtendimentoInstance, field: 'nome', 'error')} required">
	<label for="nome" class="col-sm-2 control-label">
		<g:message code="areaAtendimento.nome.label" default="Nome" />:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5">
		<g:textField name="nome" required="" maxlength="255" value="${areaAtendimentoInstance?.nome}"/>

		<g:hasErrors bean="${areaAtendimentoInstance}" field="nome">
			<span class="help-block error"><g:renderErrors bean="${areaAtendimentoInstance}" field="nome" as="list" /></span>
		</g:hasErrors>
	</div>
</div>

<div class="form-group ${hasErrors(bean: areaAtendimentoInstance, field: 'email', 'error')} required">
	<label for="email" class="col-sm-2 control-label">
		<g:message code="areaAtendimento.email.label" default="Email" />:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5">
		<g:textField name="email" required="" maxlength="255" value="${areaAtendimentoInstance?.email}"/>

		<g:hasErrors bean="${areaAtendimentoInstance}" field="email">
			<span class="help-block error"><g:renderErrors bean="${areaAtendimentoInstance}" field="email" as="list" /></span>
		</g:hasErrors>
	</div>
</div>


