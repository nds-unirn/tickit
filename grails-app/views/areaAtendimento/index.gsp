
<%@ page import="br.edu.unirn.tickit.dominio.AreaAtendimento" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="administrativo">
		<g:set var="entityName" value="${message(code: 'areaAtendimento.label', default: 'AreaAtendimento')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>


	<!-- Content Header (Page header) -->
	<section class="content-header">
		<ol class="breadcrumb">
			<li class="active">Listagem</li>
		</ol>
	</section>





	<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Lista de Áreas de atendimento (${areaAtendimentoInstanceCount})</h3>
			<div class="box-tools pull-right">
				<g:link class="btn btn-default btn-box-tool" action="create"><i class="fa fa-plus"></i> Nova área de atendimento</g:link>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<g:form action="index">
						<a href="#pesquisa" data-toggle="collapse" class="pull-right help-block collapse-btn">Pesquisar</a>
						<div class="col-xs-12 margin-bottom-10 collapse" id="pesquisa">
							<div class="row">
								<div class="col-sm-5">
									<div class="form-group">
										<label class="control-label" for="nome">Nome</label>
										<g:textField name="nome" class="form-control input-sm" value="${params?.nome}"/>
									</div>
								</div>


								<div class="col-sm-2 padding-top-20">
									<button type="submit" class="btn btn-block btn-primary"><i class="fa fa-search"></i>Pesquisar</button>
								</div>
							</div>
						</div>
					</g:form>
				</div>
				<div class="col-xs-12">
					<g:if test="${flash.message}">
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
							<div class="message" role="status">${flash.message}</div>
						</div>
					</g:if>
					<table class="table table-bordered table-condensed">
						<thead>
						<tr>
							
							<g:sortableColumn params="[q: "${query?:''}"]" property="nome" title="${message(code: 'areaAtendimento.nome.label', default: 'Nome')}" />
							
							


							<th width="9%" colspan="3">&nbsp;</th>
						</tr>
						</thead>
						<tbody>
						<g:each in="${areaAtendimentoInstanceList}" status="i" var="areaAtendimentoInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
								
								
								<td>${fieldValue(bean: areaAtendimentoInstance, field: "nome")}</td>
								
								
								

								<td class="text-center">
									<g:link class="btn btn-xs btn-default" action="show" id="${areaAtendimentoInstance.id}" data-toggle="tooltip" data-placement="bottom" title="Visualizar"><i class="fa fa-eye"></i></g:link>
									<g:link class="btn btn-xs btn-default" action="edit" id="${areaAtendimentoInstance.id}" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="fa fa-pencil"></i></g:link>
									<g:form useToken="true" url="[resource:areaAtendimentoInstance, action:'delete']" method="DELETE" style="display: inline-block;">
										<button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="bottom" title="Remover" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i class="glyphicon glyphicon-remove"></i></button>
									</g:form>
								</td>
							</tr>
						</g:each>
						</tbody>
					</table>
					<div class="pagination">
						<g:paginate params="[q: "${query?:''}"]" class="pagination-sm" total="${areaAtendimentoInstanceCount ?: 0}" />
					</div>
				</div>
			</div>
		</div><!-- /.box-body -->
	</div><!-- /.box -->

</section>
<g:javascript>
        $(document).ready(function(){
            TICKIT.selectMenu("areaAtendimentoOpt");
        });
</g:javascript>
	
	</body>
</html>
