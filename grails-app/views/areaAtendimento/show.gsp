
<%@ page import="br.edu.unirn.tickit.dominio.AreaAtendimento" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="administrativo">
	<g:set var="entityName" value="${message(code: 'areaAtendimento.label', default: 'Área de Atendimento')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<section class="content-header">
	<ol class="breadcrumb">
		<li class="active">Listagem</li>
		<li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><g:message code="default.show.label" args="[entityName]" /></h3>
		</div>

		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<table class="table table-view table-condensed">
						<thead>
							<tr>
								<th colspan="2">
									Dados da ${entityName}
								</th>
							</tr>
						</thead>
						<tbody>
							
							<g:if test="${areaAtendimentoInstance?.nome}">
								<tr>
									<td><strong><g:message code="areaAtendimento.nome.label" default="Nome" />:</strong></td>
									
									<td><g:fieldValue bean="${areaAtendimentoInstance}" field="nome"/></td>
									
								</tr>
							</g:if>

							<g:if test="${areaAtendimentoInstance?.email}">
								<tr>
									<td><strong><g:message code="areaAtendimento.email.label" default="Email" />:</strong></td>
									
									<td><g:fieldValue bean="${areaAtendimentoInstance}" field="email"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${areaAtendimentoInstance?.ativo}">
								<tr>
									<td><strong><g:message code="areaAtendimento.ativo.label" default="Ativo" />:</strong></td>
									
									<td><g:formatBoolean boolean="${areaAtendimentoInstance?.ativo}" /></td>
									
								</tr>
							</g:if>
							
							<g:if test="${areaAtendimentoInstance?.dateCreated}">
								<tr>
									<td><strong><g:message code="areaAtendimento.dateCreated.label" default="Data de Cadastro" />:</strong></td>
									
									<td><g:formatDate date="${areaAtendimentoInstance?.dateCreated}" format="dd/MM/yyyy HH:mm"/></td>
									
								</tr>
							</g:if>
							
							<g:if test="${areaAtendimentoInstance?.usuarioCadastrante}">
								<tr>
									<td><strong><g:message code="areaAtendimento.usuarioCadastrante.label" default="Usuário Cadastrante" />:</strong></td>
									
									<td><g:link controller="usuario" action="show" id="${areaAtendimentoInstance?.usuarioCadastrante?.id}">${areaAtendimentoInstance?.usuarioCadastrante?.encodeAsHTML()}</g:link></td>
									
								</tr>
							</g:if>
							
						</tbody>
					</table>
					<div class="box-footer clearfix">
						<g:link action="index" class="btn btn-primary pull-right"><i class="fa fa-reply"></i>Voltar</g:link>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<g:javascript>
        $(document).ready(function(){
            TICKIT.selectMenu("areaAtendimentoOpt");
        });
</g:javascript>
</body>
</html>
