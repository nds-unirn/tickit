<%@ page import="br.edu.unirn.tickit.dominio.Status" %>



<div class="form-group ${hasErrors(bean: statusInstance, field: 'nome', 'error')} required">
	<label for="nome" class="col-sm-2 control-label">
		<g:message code="status.nome.label" default="Nome" />:
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-5">
		<g:textField name="nome" required="" value="${statusInstance?.nome}"/>

		<g:hasErrors bean="${statusInstance}" field="nome">
			<span class="help-block error"><g:renderErrors bean="${statusInstance}" field="nome" as="list" /></span>
		</g:hasErrors>
	</div>
</div>

