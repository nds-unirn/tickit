package br.edu.unirn.tickit.acesso
/**
 * Classe responsável por Armazenar informações sobre os Perfis de Usuário.
 * @author: brunoomatheuss
 */
class Perfil {

	public static final String ADMINISTRADOR = "Administrador", SUPERVISOR = "Supervisor", PADRAO = "Padrão"

	String nome;
	Boolean ativo = true;
	Date dateCreated;
	Usuario usuarioCadastrante;

	@Override
	String toString() {
		nome
	}

	static mapping = {
        dateCreated column: "data_de_cadastro"
    }

    static constraints = {
    	nome(nullable:false, unique:true, blank:false, inList: [ADMINISTRADOR, SUPERVISOR, PADRAO])
    	ativo()
    	dateCreated()
    	usuarioCadastrante()
    }

}
