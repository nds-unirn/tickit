package br.edu.unirn.tickit.acesso
/**
 * Classe responsável por fazer o relacionamento entre Usuário e seu Perfil.
 * @author: brunoomatheuss
 */
class UsuarioPerfil {

	Usuario usuario
	Perfil perfil
	Boolean ativo = true
	Date dateCreated
	Usuario usuarioCadastrante

	static mapping = {
        dateCreated column: "data_de_cadastro"
        autoTimestamp true
    }

    public UsuarioPerfil(Usuario usuario, Perfil perfil, Usuario usuarioCadastrante){
    	this?.usuario = usuario
    	this?.perfil = perfil
    	this?.usuarioCadastrante = usuarioCadastrante
    }

    static constraints = {
    	usuario()
		perfil()
		ativo()
		dateCreated()
		usuarioCadastrante()
    }
}
