package br.edu.unirn.tickit.acesso
/**
 * Classe responsável por Armazenar informações sobre o usuário que acessa o sistema.
 * @author: Jefferson Silva
 */
class Usuario {

    String login
    String senha
    String nome
    String email
    boolean ativo = Boolean.TRUE
    Usuario usuarioCadastrante
    Date dateCreated

    @Override
    String toString() {
        nome
    }

    static constraints = {
        login blank:false
        nome blank:false
        email nullable:false,blank:false,email:true
        senha(attributes:[showInList:false])
        ativo(attributes:[showInForm:false])
        dateCreated(attributes:[fieldName: "Data de Cadastro"])
        usuarioCadastrante validator: {val,obj ->
            def totalUsuarios = Usuario.executeQuery("select count(u) from Usuario u where u.ativo = true")
            if(totalUsuarios[0] > 0){
                if(totalUsuarios[0] == 1 && obj?.id != null){
                    return true
                }
                if(val != null){
                    return true
                }else{
                    return false
                }
            }else{
                return true
            }
        }
    }

    static mapping = {
        dateCreated column: "data_de_cadastro"
    }

    static searchFields = ["nome","login"]

    /**
     * Método responsável pela criptografia da senha, como observação
     * depois ver a possibilidade de adicionar também na alteração da
     * senha
     * @return
     */
    def beforeInsert(){
        if(senha)
            this.senha = senha.encodeAsSHA256()
    }


    boolean hasPerfil(Perfil perfil){
        def usuarioPerfil = UsuarioPerfil.executeQuery("select count(*) from UsuarioPerfil as up where up.ativo = true and up.usuario = :usuario and up.perfil = :perfil",['usuario':this,'perfil':perfil])
        if(usuarioPerfil[0] > 0) return true
        return false
    }

    boolean hasPerfilWithNome(String nome){
        def usuarioPerfil = UsuarioPerfil.executeQuery("select count(*) from UsuarioPerfil as up where up.ativo = true and up.usuario = :usuario and up.perfil.nome = :nomePerfil",['usuario':this,'nomePerfil':nome])
        if(usuarioPerfil[0] > 0) return true
        return false
    }

    boolean desativarPerfis(){
        boolean deuCerto = true
        def usuarioPerfis = UsuarioPerfil.findAllWhere(ativo:true,usuario:this)
        usuarioPerfis.each{
            it?.ativo = false
            if(!it?.save(flush:true)){
                deuCerto = false
            }
        }
        return deuCerto
    }

    Perfil getPerfil(){
        return UsuarioPerfil.findByAtivoAndUsuario(true,this)?.perfil
    }

    boolean desativar(){
        this.ativo = false
        return this.desativarPerfis()
    }
}
