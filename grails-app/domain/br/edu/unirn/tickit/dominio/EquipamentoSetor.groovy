package br.edu.unirn.tickit.dominio

import br.edu.unirn.tickit.acesso.Usuario
/**
 * Classe responsável por Armazenar o vínculo de um equipamento a um Setor
 * @author: Jefferson Silva
 */
class EquipamentoSetor {

    Equipamento equipamento
    Setor setor
    boolean ativo = Boolean.TRUE
    Usuario usuarioCadastrante
    Date dateCreated

    static constraints = {
        equipamento()
        setor()
        dateCreated(attributes:[fieldName: "Data de Cadastro"])
        ativo(attributes:[showInForm:false])
    }

    static mapping = {
        dateCreated column: "data_de_cadastro"
    }
}
