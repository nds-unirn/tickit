package br.edu.unirn.tickit.dominio

import br.edu.unirn.tickit.acesso.Usuario
/**
 * Classe responsável por Armazenar informações sobre o Equipamento
 * @author: Jefferson Silva
 */
class Equipamento {

    String nome
    Date dataCompra
    boolean locado = Boolean.FALSE
    String observacoes
    String tombamento
    boolean ativo = Boolean.TRUE
    TipoEquipamento tipoEquipamento
    Usuario usuarioCadastrante
    Date dateCreated

    @Override
    String toString() {
        nome
    }

    static constraints = {
        nome blank:false
        tipoEquipamento()
        dataCompra(attributes:[fieldName: "Data de Compra"])
        locado()
        observacoes nullable: true
        ativo(attributes:[showInForm:false])
        tombamento nullable: true
        tombamento validator: {val,obj ->
            if(obj?.locado && val == null)
                return["br.edu.unirn.tickit.dominio.Equipamento.tombamento.default.invalid.validator.message"]
        }
        dateCreated(attributes:[fieldName: "Data de Cadastro"])

    }

    static mapping = {
        dateCreated column: "data_de_cadastro"
        observacoes type: "text"
    }

    static searchFields = ["nome,tombamento"]
}
