package br.edu.unirn.tickit.dominio

import br.edu.unirn.tickit.acesso.Usuario
/**
 * Classe responsável por Armazenar informações sobre anexos de um Chamado.
 * @author: Jefferson Silva
 */
class AnexoChamado {

    String nomeArquivo
    boolean ativo = Boolean.TRUE
    Chamado chamado
    Usuario usuarioCadastrante
    Date dateCreated

    @Override
    String toString() {
        nomeArquivo
    }
    static constraints = {
        nomeArquivo blank:false
        chamado()
        ativo(attributes:[showInForm:false])
        dateCreated(attributes:[fieldName: "Data de Cadastro"])
    }

    static mapping = {
        dateCreated column: "data_de_cadastro"
    }

    static searchFields = ["nomeArquivo"]
}
