package br.edu.unirn.tickit.dominio

import br.edu.unirn.tickit.acesso.Usuario
/**
 * Por ser uma aplicação genérica possa ser que em outros locais um setor tenham mais
 * de uma Área de Atendimento.
 * Por exemplo: Setor manutenção que tem a Área eletríca, Área Hidráulica.
 * @author: Jefferson Silva
 */
class AreaAtendimento {

    String nome
    String email
    boolean ativo = Boolean.TRUE
    Usuario usuarioCadastrante
    Date dateCreated

    @Override
    String toString() {
        nome
    }

    static constraints = {
        nome blank:false,unique:true
        email blank:false,email:true,nullable:false
        ativo(attributes:[showInForm:false])
        dateCreated(attributes:[fieldName: "Data de Cadastro"])
    }

    static mapping = {
        dateCreated column: "data_de_cadastro"
    }

    static searchFields = ["nome"]
}
