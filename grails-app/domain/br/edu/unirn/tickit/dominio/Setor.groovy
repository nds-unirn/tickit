package br.edu.unirn.tickit.dominio

import br.edu.unirn.tickit.acesso.Usuario
/**
 * Classe responsável por Armazenar informações sobre os setores de uma instituição.
 * @author: Jefferson Silva
 */
class Setor {

    String nome
    boolean ativo = Boolean.TRUE
    Usuario usuarioCadastrante
    Instituicao instituicao
    Date dateCreated

    @Override
    String toString() {
        nome
    }

    static constraints = {
        nome blank:false
        instituicao(unique: 'nome')
        ativo(attributes:[showInForm:false])
        dateCreated(attributes:[fieldName: "Data de Cadastro"])
    }

    static mapping = {
        dateCreated column: "data_de_cadastro"
    }

    static searchFields = ["nome"]
}
