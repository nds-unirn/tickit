package br.edu.unirn.tickit.dominio

import br.edu.unirn.tickit.acesso.Usuario

class TipoOcorrencia {

	String nome
	boolean ativo = true
	Date dateCreated
	Usuario usuarioCadastrante

	@Override
	String toString(){
		nome
	}

    static constraints = {

    	nome blank:false, unique:true
    	ativo(attributes:[showInForm:false])
    	dateCreated(attributes:[fieldName:"Data de cadatro"])
    }

    static mapping = {
    	dateCreated column:"data_de_cadastro"
    }

    static searchFields = ["nome"]
}
