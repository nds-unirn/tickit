package br.edu.unirn.tickit.dominio

import br.edu.unirn.tickit.acesso.Usuario
/**
 * Classe responsável por fazer o Relacionamento entre o Chamado e seu Status.
 * @author: Jefferson Silva
 */
class StatusChamado {

    Status status
    Chamado chamado
    boolean ativo = Boolean.TRUE
    Usuario usuarioCadastrante
    Date dateCreated

    static constraints = {
        status()
        chamado()
        usuarioCadastrante nullable:true
        usuarioCadastrante validator: {val,obj ->
            if(obj?.status?.nome?.equals("Aberto")){
                return true
            }else{
                if(val != null){
                    return true
                }else{
                    return ['usuarioCadastranteNulo']
                }
            }
        }
        dateCreated(attributes:[fieldName: "Data de Cadastro"])
    }

    static mapping = {
        dateCreated column: "data_de_cadastro"
    }
}
