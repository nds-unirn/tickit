package br.edu.unirn.tickit.dominio
import br.edu.unirn.sisuni.utils.CalendarUtils
import br.edu.unirn.tickit.acesso.Usuario
/**
 * Classe responsável por Armazenar informações sobre o Chamado.
 * @author: Jefferson Silva
 */
class Chamado {

    String titulo
    String descricao
    String emailSolicitante
    boolean ativo = Boolean.TRUE
    Usuario usuarioCadastrante
    Setor setorSolicitante
    AreaAtendimento areaAtendimento
    Date dateCreated

    @Override
    String toString() {
        titulo
    }

    static constraints = {
        titulo blank:false
        descricao blank:false
        emailSolicitante blank:false, nullable:false,email:true
        setorSolicitante nullable:false
        areaAtendimento nullable: false
        usuarioCadastrante nullable:true
        ativo(attributes:[showInForm:false])
        dateCreated(attributes:[fieldName: "Data de Cadastro"])
    }

    static mapping = {
        dateCreated column: "data_de_cadastro"
        descricao type: "text"
    }

    static searchFields = ["titulo","emailSolicitante"]

    public String getDateCreatedAsString(){
        return CalendarUtils.getStriWithTimengByDate(this.dateCreated)
    }

}
