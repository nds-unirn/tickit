package br.edu.unirn.tickit.dominio

import br.edu.unirn.tickit.acesso.Usuario
/**
 * Classe responsável por Armazenar informações sobre Instituição.
 * @author: Jefferson Silva
 */
class Instituicao {

    String nome
    boolean ativo = Boolean.TRUE
    Usuario usuarioCadastrante
    Date dateCreated

    @Override
    String toString() {
        nome
    }

    static constraints = {
        nome unique: true, blank:false
        ativo(attributes:[showInForm:false])
        dateCreated(attributes:[fieldName: "Data de Cadastro"])
    }

    static mapping = {
        dateCreated column: "data_de_cadastro"
    }

    static searchFields = ["nome"]
}
