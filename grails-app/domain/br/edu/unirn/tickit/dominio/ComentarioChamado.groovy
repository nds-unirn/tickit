package br.edu.unirn.tickit.dominio

import br.edu.unirn.tickit.acesso.Usuario
/**
 * Classe responsável por Armazenar Comentários de um chamado.
 * @author: Jefferson Silva
 */
class ComentarioChamado {

    String comentario
    boolean ativo = Boolean.TRUE
    Chamado chamado
    Usuario usuarioCadastrante
    Date dateCreated

    static constraints = {
        comentario()
        chamado()
        ativo(attributes:[showInForm:false])
        dateCreated(attributes:[fieldName: "Data de Cadastro"])
    }

    static mapping = {
        dateCreated column: "data_de_cadastro"
        comentario type: "text"
    }
}
