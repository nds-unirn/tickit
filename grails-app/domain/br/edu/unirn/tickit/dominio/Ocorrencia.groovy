package br.edu.unirn.tickit.dominio

import br.edu.unirn.tickit.acesso.Usuario

class Ocorrencia {

	String descricao
	TipoOcorrencia tipoOcorrencia
	Usuario usuarioCadastrante
	boolean ativo = true
	Date dateCreated
    Date dataOcorrencia

    static constraints = {

    	descricao nullable:false, blank:false
    	tipoOcorrencia()
    	ativo(attributes:[showInForm:false])
    	dateCreated(attributes:[fieldName:"data_de_cadastro"])
        dataOcorrencia(attributes:[fieldName:"data_da_ocorrencia"], nullable:false)

    }

    static mapping = {
        dateCreated column: "data_de_cadastro"
        dataOcorrencia column: "data_da_ocorrencia"
        descricao type:"text"
    }
}
