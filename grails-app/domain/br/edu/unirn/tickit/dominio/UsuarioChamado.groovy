package br.edu.unirn.tickit.dominio

import br.edu.unirn.tickit.acesso.Usuario
/**
 * Classe responsável por Armazenar informacoes sobre o responsavel de um chamado.
 * @author: Jefferson Silva
 */
class UsuarioChamado {

    Usuario usuario
    Chamado chamado
    boolean ativo = Boolean.TRUE
    Usuario usuarioCadastrante
    Date dateCreated

    static constraints = {
        usuario()
        chamado()
        ativo(attributes:[showInForm:false])
        dateCreated(attributes:[fieldName: "Data de Cadastro"])
    }

    static mapping = {
        dateCreated column: "data_de_cadastro"
    }

    boolean isUsuarioChamado(Chamado chamado, Usuario usuario){

        def listaDeUsuariosDochamado = UsuarioChamado.executeQuery("select uc from UsuarioChamado uc where uc.ativo = true and uc.chamado = :chamado and uc.usuario = :usuario ", [chamado:chamado, usuario:usuario])

        if(listaDeUsuariosDochamado.isEmpty())
            return false

        return true
    }
}
