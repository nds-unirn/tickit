package br.edu.unirn.tickit.dominio
import br.edu.unirn.tickit.acesso.Usuario
/**
 * Classe responsável por fazer o relacionamento entre Equipamento e suas Características.
 * @author: Felipe Vianna
 */
class EquipamentoCaracteristica {

	Equipamento equipamento
	Caracteristica caracteristica
    String descricaoCaracteristica
	Boolean ativo = true
	Date dateCreated
	Usuario usuarioCadastrante

    static mapping = {
        dateCreated column: "data_de_cadastro"
        autoTimestamp true
    }

    public UsuarioPerfil(Equipamento equipamento, Caracteristica caracteristica, Usuario usuarioCadastrante){
    	this?.equipamento = equipamento
    	this?.caracteristica = caracteristica
    	this?.usuarioCadastrante = usuarioCadastrante
    }

    static constraints = {
    	equipamento()
		caracteristica()
        descricaoCaracteristica()
		ativo()
		dateCreated()
		usuarioCadastrante()
    }
}
