package br.edu.unirn.tickit.dominio

import br.edu.unirn.tickit.acesso.Usuario

/**
 * Classe responsável por fazer o Relacionamento entre o Chamado e Equipamento.
 * @author: Jefferson Silva
 */
class EquipamentoChamado {

    Chamado chamado
    Equipamento equipamento
    boolean ativo = Boolean.TRUE
    Usuario usuarioCadastrante
    Date dateCreated

    static constraints = {
        chamado()
        equipamento()
        dateCreated(attributes:[fieldName: "Data de Cadastro"])
    }

    static mapping = {
        dateCreated column: "data_de_cadastro"
    }
}
