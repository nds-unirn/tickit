package br.edu.unirn.tickit.dominio
import br.edu.unirn.tickit.acesso.Usuario
/**
*Classe responsável por guardar características de um equipamento
*@author Felipe Vianna
*/

class Caracteristica {

	String nome
	String descricao
	boolean ativo = Boolean.TRUE
	Usuario usuarioCadastrante
    Date dateCreated



    static constraints = {
    	nome blank:false
    	descricao blank:false
    	ativo(attributes:[showInForm:false])
    	dateCreated(attributes:[fieldName: "Data de Cadastro"])
    }

    static mapping = {
        dateCreated column: "data_de_cadastro"
        descricao type: "text"
    }
}
