package br.edu.unirn.tickit.dominio



import grails.test.mixin.*
import spock.lang.*

@TestFor(TipoOcorrenciaController)
@Mock(TipoOcorrencia)
class TipoOcorrenciaControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.tipoOcorrenciaInstanceList
            model.tipoOcorrenciaInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.tipoOcorrenciaInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            def tipoOcorrencia = new TipoOcorrencia()
            tipoOcorrencia.validate()
            controller.save(tipoOcorrencia)

        then:"The create view is rendered again with the correct model"
            model.tipoOcorrenciaInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            tipoOcorrencia = new TipoOcorrencia(params)

            controller.save(tipoOcorrencia)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/tipoOcorrencia/show/1'
            controller.flash.message != null
            TipoOcorrencia.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def tipoOcorrencia = new TipoOcorrencia(params)
            controller.show(tipoOcorrencia)

        then:"A model is populated containing the domain instance"
            model.tipoOcorrenciaInstance == tipoOcorrencia
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def tipoOcorrencia = new TipoOcorrencia(params)
            controller.edit(tipoOcorrencia)

        then:"A model is populated containing the domain instance"
            model.tipoOcorrenciaInstance == tipoOcorrencia
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/tipoOcorrencia/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def tipoOcorrencia = new TipoOcorrencia()
            tipoOcorrencia.validate()
            controller.update(tipoOcorrencia)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.tipoOcorrenciaInstance == tipoOcorrencia

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            tipoOcorrencia = new TipoOcorrencia(params).save(flush: true)
            controller.update(tipoOcorrencia)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/tipoOcorrencia/show/$tipoOcorrencia.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/tipoOcorrencia/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def tipoOcorrencia = new TipoOcorrencia(params).save(flush: true)

        then:"It exists"
            TipoOcorrencia.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(tipoOcorrencia)

        then:"The instance is deleted"
            TipoOcorrencia.count() == 0
            response.redirectedUrl == '/tipoOcorrencia/index'
            flash.message != null
    }
}
