package br.edu.unirn.sisuni.utils
import java.text.SimpleDateFormat

class CalendarUtils {

	static definirHoraInicio(data){
		Calendar hoje = Calendar.instance
		hoje.time = data
		hoje.set(Calendar.HOUR_OF_DAY,0)
		hoje.set(Calendar.MINUTE,0)
		hoje.set(Calendar.SECOND,0)
		hoje.set(Calendar.MILLISECOND,0)
		hoje.time
	}
	
	static definirHoraFim(data){
		Calendar hojeFim = Calendar.instance
		hojeFim.time = data
		hojeFim.set(Calendar.HOUR_OF_DAY,23)
		hojeFim.set(Calendar.MINUTE,59)
		hojeFim.set(Calendar.SECOND,59)

		hojeFim.time
	}
	
	//Metodo para pegar a hora final do turno matutino 
	static horaFimMatutino(data){
		Calendar hoje = Calendar.instance
		hoje.time = data
		hoje.set(Calendar.HOUR_OF_DAY,11)
		hoje.set(Calendar.MINUTE,59)
		hoje.set(Calendar.SECOND,59)
		hoje.set(Calendar.MILLISECOND,59)
		hoje.time
	}
	
	//Metodo para pegar a hora final do turno vespertino
	static horaFimVespertino(data){
		Calendar hoje = Calendar.instance
		hoje.time = data
		hoje.set(Calendar.HOUR_OF_DAY,17)
		hoje.set(Calendar.MINUTE,59)
		hoje.set(Calendar.SECOND,59)
		hoje.set(Calendar.MILLISECOND,59)
		hoje.time
	}
	
	static Date getDateByString(String data){
		if(data != null && !data?.trim()?.equals("")){
			String [] partes = data.split("/")
			int dia = partes[0].toInteger()
			int mes = partes[1].toInteger()
			int ano = partes[2].toInteger()
			if( (mes>=01 && mes<=12) && (ano>=0) &&
				( ( (mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes==10 || mes==12) && (dia>=0 && dia<=31) )
				 || ( (mes==4 || mes==6 || mes==9 || mes==11) && (dia>=0 && dia<=30) )
				 || ( (mes==2) && (dia>=0 && dia<=29) ) ) ){
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy")
				Calendar dataCalendar = Calendar.getInstance()
				dataCalendar.setTime(simpleDateFormat.parse(data))
				dataCalendar.set(Calendar.HOUR_OF_DAY,0)
				dataCalendar.set(Calendar.MINUTE,0)
				dataCalendar.set(Calendar.SECOND,0)
				dataCalendar.set(Calendar.MILLISECOND,0)
				return dataCalendar.getTime()
			}
		}else{
			null
		}
	}

	static String getStringByDate(Date data){
		if(data != null){
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy")
			return simpleDateFormat.format(data)
		}else{
			null
		}
	}

	static String getStriWithTimengByDate(Date data){
		if(data != null){
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss")
			return simpleDateFormat.format(data)
		}else{
			null
		}
	}

	static Date getTodayWithoutTime(){
		Calendar hoje = Calendar.getInstance()
		hoje.setTime(new Date())
		hoje.set(Calendar.HOUR_OF_DAY,0)
		hoje.set(Calendar.MINUTE,0)
		hoje.set(Calendar.SECOND,0)
		hoje.set(Calendar.MILLISECOND,0)
		return hoje.getTime()
	}

	static String getDiaMes(Date data){
		Calendar diaMes = Calendar.getInstance()
		diaMes.setTime(data)
		String dia = diaMes.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + diaMes.get(Calendar.DAY_OF_MONTH) : diaMes.get(Calendar.DAY_OF_MONTH)
		String mes = diaMes.get(Calendar.MONTH) <10 ? "0" + diaMes.get(Calendar.MONTH) : diaMes.get(Calendar.MONTH)
		return  dia + "/"  + mes
	}

	static String getDiaStringDoisDigitos(int dia){
		if(dia < 10){
			return "0"+dia
		}else{
			return ""+dia
		}
	}

}
 