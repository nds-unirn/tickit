package br.edu.unirn.tickit.dominio.command

import br.edu.unirn.tickit.dominio.Instituicao
import br.edu.unirn.tickit.dominio.Setor
import br.edu.unirn.tickit.dominio.TipoEquipamento
import grails.validation.Validateable

/**
 * Created by MarcioDavi on 16/04/2016.
 */
@Validateable
class RelatorioEquipamentoCommand {
    Instituicao instituicao
    Setor setor
    TipoEquipamento tipoEquipamento
    Boolean locado
    Date dataCompraInicio
    Date dataCompraFim

}
