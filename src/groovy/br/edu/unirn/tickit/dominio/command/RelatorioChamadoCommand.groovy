package br.edu.unirn.tickit.dominio.command

import br.edu.unirn.tickit.acesso.Usuario
import br.edu.unirn.tickit.dominio.AreaAtendimento
import br.edu.unirn.tickit.dominio.Instituicao
import br.edu.unirn.tickit.dominio.Setor
import br.edu.unirn.tickit.dominio.TipoEquipamento
import grails.validation.Validateable

/**
 * Created by MarcioDavi on 16/04/2016.
 */
@Validateable
class RelatorioChamadoCommand {

    String titulo
    String descricao
    String emailSolicitante
    boolean ativo = Boolean.TRUE
    Usuario usuarioCadastrante
    Setor setorSolicitante
    AreaAtendimento areaAtendimento
    Date dateCreated
    Date dateCreatedInicio
    Date dateCreatedFim

}
